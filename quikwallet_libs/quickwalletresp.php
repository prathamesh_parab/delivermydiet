<?php
include "../dynamicVAR.php";

session_start();

$status = $_GET['status'];
$Redirect_URL = QUIKWALLET_CLOSE_REDIRECT_URL;
$txn_id = $_SESSION['billnumbers'];
$order_id = $_SESSION['orderid'];


$data = array();
$data['order_id'] = $_SESSION['orderid'];
$data['txn_status'] = $_GET['status'];
$data['txn_id'] = $_SESSION['billnumbers'];


date_default_timezone_set("Asia/Calcutta");


if (!isset($_SESSION['client_platform'])) {
    ?>
    <?php if (strtolower($data['txn_status']) === "success" || strtolower($data['txn_status']) === "paid") { ?>
        <input type="hidden" id="success" value="1"/>
        <input type="hidden" id="txnid" value='<?php echo $data['txn_id']; ?>'/>
        <input type="hidden" id="msg" value="Your Order was successful placed"/>
    <?php } else { ?>
        <input type="hidden" id="success" value="0"/>
        <input type="hidden" id="txnid" value='<?php echo $data['txn_id']; ?>'/>
        <input type="hidden" id="msg" value="Uh! oh!. We could not process the payment"/>
    <?php } ?>
    <script>
    <?php if (strtolower($data['txn_status']) === "success" || strtolower($data['txn_status']) === "paid") { ?>
            var txnid = '////<?php echo $data['txn_id']; ?>';
            var type = 1;
            var msg = "Your Order was successful placed";



    <?php } else { ?>
            var txnid = '////<?php echo $data['txn_id']; ?>';
            var type = 0;
            var msg = "Uh! oh!. We could not process the payment";


    <?php } ?>
        function handleResponse() {
            Android.handleResponse(txnid, type, msg);
        }
        handleResponse();
    </script>

    <?php
    exit;
} else {
    ?>

    <div id="myModal1" class='modal fade' role='dialog'>

    </div>
    <div style="height: 100%;width: 100%; background: url('../img/logo.png');opacity: 0.2;">

    </div>


    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet/less" type="text/css" href="../css/main.less" />
    <link rel="stylesheet" href="../css3-animate-it-master/css/animations.css" type="text/css" />
    <!--[if lte IE 9]>
        <link href='css3-animate-it-master/css/animations-ie-fix.css' rel='stylesheet'/>
    <![endif]-->
    <link rel="stylesheet" href="../css/jquery-ui.css">
    <link rel="stylesheet" href="../font-awesome-4.1.0/css/font-awesome.min.css">

    <script src="../js/less-1.7.5.min.js" type="text/javascript"></script>
    <script src="../js/dynamicCSS.js" type="text/javascript"></script>
    <!-- jquery -->
    <script src="../js/jquery-1.11.3.min.js"></script>
    <script src="../js/jquery-1.11.4-ui.js"></script>
    <!-- Bootstrap -->
    <script src="../js/bootstrap.min.js"></script>
    <!-- custom js> -->
    <script src="../js/angular.min.js"></script>
    <script src="../js/angular-animate.min.js"></script>
    <script src="../js/angular-cookies.min.js"></script>
    <script src="../js/angular-route.min.js"></script>
    <script src="../js/angular-touch.js"></script> 
    <script src="../js/jquery.backstretch.min.js"></script>
    <script src="../js/moment.min.js"></script>

    <script type="text/javascript" src="../fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
    <script src='../css3-animate-it-master/js/css3-animate-it.js'></script>



    <script>
    <?php if (strtolower($data['txn_status']) === "success" || strtolower($data['txn_status']) === "paid") { ?>

            $("#myModal1").html("<div><div class='modal-dialog'><div class='modal-content'><div class='modal-header' style='background-color: #CCC;'><button type='button' class='close' data-dismiss='modal'>&times;</button><h4 class='modal-title text-center'>Payment Status</h4></div><div class='modal-body text-center'><p>Congratulations ! , Your Order was successful.<br> Please Note your transaction id <strong><?php echo $txn_id; ?></strong>, <br>May help you for future reference<br/>Order Id is : <?php echo $order_id; ?> <br>You May also call on our respective outlet numbers</p></div><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");
            $("#myModal1").modal('show');
            $('#myModal1').on('hidden.bs.modal', function () {
                localStorage.removeItem("cart");
                window.open("<?php echo $Redirect_URL; ?>", "_self");
            });

    <?php } else { ?>

            $("#myModal1").html("<div ><div class='modal-dialog'><div class='modal-content'><div class='modal-header' style='background-color: #CCC;'><button type='button' class='close' data-dismiss='modal'>&times;</button><h4 class='modal-title text-center'>Payment Status</h4></div><div class='modal-body text-center'><p>Sorry ! We could not process the payment.<br> Please Note your transaction id <strong><?php echo $txn_id; ?></strong>, <br>May help you for future reference<br/>Order Id is : <?php echo $order_id; ?> <br>You May also call on our respective outlet numbers</p></div><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");
            $('#myModal1').modal('show');
            $('#myModal1').on('hidden.bs.modal', function () {
                localStorage.removeItem("cart");
                window.open("<?php echo $Redirect_URL; ?>", "_self");
            });
    <?php } ?>
    </script>
    <?php
}
/* And we are done. */
?>