<?php

include "../../dynamicVAR.php";

$postData = file_get_contents('php://input');
$jsonData = json_decode($postData, true);
$jsonData["outletid"] = getOutletId($jsonData["outletid"],$mapped_outlets);
//print_r($jsonData);exit;
$url = $jsonData["url"];
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
curl_close($ch);
print_r($response);


function getOutletId($phone_outlet_id,$mapped_outlets) {

    $done_outlet_id = $phone_outlet_id;
    if (isset($mapped_outlets[$done_outlet_id])) {
        $flip_array = array_flip($mapped_outlets);
        $outlet_id = array_search($done_outlet_id, $flip_array);
        return $outlet_id;
    } else {
        return "0";
    }
}

?>
