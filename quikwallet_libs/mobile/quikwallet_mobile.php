<?php

include "../../dynamicVAR.php";

$postData = file_get_contents('php://input');
$jsonData = json_decode($postData, true);

function callWebService($url, $methodType, $data) {
    
    if ($data != null)
        $data = json_encode($data, JSON_NUMERIC_CHECK);
    $headers = array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data)
    );
    $ch = curl_init($url);
    if ($methodType) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}

$jsonData["outletid"] = getOutletId($jsonData["outletid"],$mapped_outlets);
//print_r($jsonData);exit;
$url = $jsonData["url"];
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);

curl_close($ch);
$res = json_decode($response);
$req = array();
    $req['order_id'] = $jsonData['orderid'];
    $req['txn_id'] = $jsonData['billnumbers'];
    $req['gateway_txn_id'] = $res->data->id;
    $resp = callWebService("http://api.done.to/done-update-gateway-id",true, $req);

    $file2 = 'text2.txt';
    $current2 = file_get_contents($file2);
    $current2 .= json_encode($jsonData);
    file_put_contents($file2, $current2);
    print_r($response);

function getOutletId($phone_outlet_id,$mapped_outlets) {

    $done_outlet_id = $phone_outlet_id;
    if (isset($mapped_outlets[$done_outlet_id])) {
        $flip_array = array_flip($mapped_outlets);
        $outlet_id = array_search($done_outlet_id, $flip_array);
	if(isset($outlet_id)){
        return $outlet_id;
	}else{
	return QUIKWALLET_DEFAULT_OUTLET_ID;
	}
    } else {
        return "0";
    }
}

?>
