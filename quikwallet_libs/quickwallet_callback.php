<?php

include "../dynamicVAR.php";

date_default_timezone_set("Asia/Calcutta");

/* * ******************************************************* */
$received = json_decode(file_get_contents("php://input"));

$timestamp = $received->data->timestamp;
$compute_timestamp = date('Y-m-d H:i:s', $timestamp);

$server = "http://10.0.1.201:5000";//QUIKWALLET_SERVER;
/* * ******************************************************* */


//$status = $_GET['status'];
//if($status == 'paid') {
//    $status='success';
//}
//$xmlstr = <<<EOF
//<xmlrequest>
// <userauthenticate>
//   <username>done</username>
//   <xmltoken>a1768a2ff3fbb23c8ae78e6de892a95f73be7e6d</xmltoken>
// </userauthenticate>
// <headerdetails>
//   <fromemail>getit@done.to</fromemail>
//   <body><![CDATA[ $received1 ]]></body>
//   <bounceserver />
//   <bouncedemailaddress>bounce@v.email360api.com</bouncedemailaddress>
//   <bouncepassword />
//   <bounceaccounttype>IMAP</bounceaccounttype>
//   <contactlist>abc</contactlist>
//   <campaignname>Test Campaign</campaignname>
// </headerdetails>
// <recipientdata>
//   <r1>
//     <to>nishant.nirmal@done.to</to>
//   </r1>                       
// </recipientdata>
//</xmlrequest>
//EOF;
//       $time = '';
//       $url = "http://v.email360api.com/emailAPI/sendemail.php";
//       $postfields = "xmlData=" . urlencode($xmlstr);



$status = $received->data->state;

$payment_mode = 'NA';
if (strtolower($status) == 'paid' || strtolower($status) == 'success') {
    $status = 'success';
    $payment_mode = $received->data->mode;
} else if(strtolower($status) == 'failed'){
    $status = 'failed';
    $payment_mode = 'NA';
}else{
    $status = $received->data->state;
    $payment_mode = 'NA';

}

function payment_success() {
    /* Payment success logic goes here. */
}

function payment_failure() {
    /* Payment failure logic goes here. */
}

function callWebService($url, $methodType, $data) {
    if ($data != null)
        $data = json_encode($data, JSON_NUMERIC_CHECK);
    $headers = array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data)
    );
    $ch = curl_init($url);
    if ($methodType) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}

$data = array();
$data['order_id'] = $received->data->udf2;
$data['txn_status'] = $status;
$data['txn_id'] = $received->data->billnumbers;
$data['txn_message'] = $status;
$data['txn_payment_mode'] = $payment_mode;
$data['txn_time'] = $compute_timestamp;
$data['client-platform'] = $received->data->udf3;
$data['new_user'] = $received->data->udf1;

//$url = "http://api.done.to/Maroosh/webservices/sendmail.php";
$ch = curl_init($url);


//$datas = array();
//$data = json_encode($data);
//$datas['datas'] = $data;

//curl_setopt($ch, CURLOPT_POST, TRUE);
//curl_setopt($ch, CURLOPT_POSTFIELDS, $datas);
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
//$out_put = curl_exec($ch);

//
//$json_data = '{
//    "username":"done",
//    "password":"done@123",
//    "from": "transaction@done.to",
//    "fromName":"Done",
//    "to": ["nishant.nirmal@done.to"],
//    "subject": "Testing Quick Wallet ",
//    "replyTo": "nishant.nirmal@done.to",
//    "campaignName": "sd Test Campaign",
//    "htmlBody": "' . base64_encode(file_get_contents("php://input")) . '",
//    "textBody": "",
//    "listName":"Simply Test"
//   
//}';
//
//$URL = "http://api.emsender.in/campaign_api/campaign/format/json";
//$ch = curl_init($URL);
//curl_setopt($ch, CURLOPT_POST, 1);
//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
//curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//echo $response = curl_exec($ch);
//curl_close($ch);
$res = callWebService($server.'/done-save-transaction', true, $data);

$file2 = 'test2.txt';
$current2 = file_get_contents($file2);
$current2 .= json_encode($received) . "testtest";
echo file_put_contents($file2, $current2);
?>
