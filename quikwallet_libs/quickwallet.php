<?php

/*
 * @version 1.0
 */
session_start();
include "../dynamicVAR.php";
include "../css/main.less";

$key = QUIKWALLET_MERCHANT_KEY;
$done_outlet_id = $_POST['outlet_id'];
$partner_id = QUIKWALLET_PARTNER_ID;
$staging_type = QUIKWALLET_ENVIRONMENT;


$post_action = '';
if ($staging_type == "TEST") {
    $post_action = "https://uat.quikpay.in/api/partner/" . $partner_id . "/requestpayment";
    $outlet_id = QUIKWALLET_DEFAULT_OUTLET_ID;
    
} else {

    if (isset($mapped_outlets[$done_outlet_id])) {
        $flip_array = array_flip($mapped_outlets);
        $outlet_id = array_search($done_outlet_id, $flip_array);
    } else {
        $outlet_id = QUIKWALLET_DEFAULT_OUTLET_ID;
    }
    $post_action = "https://server.livquik.com/api/partner/" . $partner_id . "/requestpayment";
}

date_default_timezone_set("Asia/Calcutta");
$current_timestamp = date("Y-m-d H:i:s");

function callWebService($url, $methodType, $data) {
    
    if ($data != null)
        $data = json_encode($data, JSON_NUMERIC_CHECK);
    $headers = array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data)
    );
    $ch = curl_init($url);
    if ($methodType) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}

$data = array();
$data['partnerid'] = $partner_id;
$data['outletid'] = $outlet_id;
$data['mobile'] = $_POST['mobile'];
$data['secret'] = $key;
$data['amount'] = $_POST['total_amount'];
$data['orderid'] = $_POST['order_id'];
$data['billnumbers'] = $_POST['txn_id'];
$data['udf1'] = $_POST['is_new_user'];          // Is New User
$data['udf2'] = $_POST['order_id'];             // Order ID
$data['udf3'] = $_POST['client_platform'];      // Client Platform


$_SESSION['orderid'] = $_POST['order_id'];
$_SESSION['billnumbers'] = $_POST['txn_id'];
$_SESSION['client_platform'] = $_POST['client_platform'];

$res = callWebService($post_action, "POST", $data);
$res = json_decode($res);


$link_status = $res->status;
print_r($res);

if (strtolower($link_status) == "success") {
    $req = array();
    $req['order_id'] = $_POST['order_id'];
    $req['txn_id'] = $_POST['txn_id'];
    $req['gateway_txn_id'] = $res->data->id;  
    
//    $req['order_id'] = "849760";//$_POST['order_id'];
//    $req['txn_id'] = "TXN-1456136661152";//$_POST['txn_id'];
//    $req['gateway_txn_id'] = "M-180619";//$res->data->id;  
    
    $response = callWebService("http://api.done.to/done-update-gateway-id",true, $req);
    
//    done-update-gateway-id
    $link = $res->data->link;
} else {

    $data['order_id'] = $_POST['order_id'];
    $data['txn_status'] = "failed";
    $data['txn_id'] = $_POST['txn_id'];
    $data['txn_message'] = "failed";
    $data['txn_payment_mode'] = "NA";
    $data['txn_time'] = $current_timestamp;
    $data['client-platform'] = $_POST['client_platform'];
    $data['new_user'] = $_POST['is_new_user'];
    $res = callWebService('http://api.done.to/done-save-transaction', true, $data);
}

//    exit;
?>




<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Quick Wallet Payment</title>
    </head>

    <body>

        <form action="<?php echo $link ?>" id='linkpost'>

        </form>
        <h2> Quick Wallet Payment Example </h2> <hr/>
        <form method='POST' id="transactionForm" action='<?php echo $post_action; ?>'>
            <table border='0'>

<!--<tr> <td> Key : </td> <td> <input name='key' type='text' value='tradus'> </td>-->
                <tr> 
                    <td> Partner ID : </td> 
                    <td> <input name='partnerid' type='text' value='<?php echo $partner_id; ?>'> </td>	
                <tr> 
                    <td> Outlet Id : </td> 
                    <td> <input name='outletid' type='text' value='<?php echo $outlet_id; ?>'> </td>
                <tr> 
                    <td> Mobile : </td> 
                    <td> <input name='mobile' type='text' value='<?php echo $_POST['mobile']; ?>'> </td>
                <tr> 
                    <td> Secret : </td> 
                    <td> <input name='secret' type='text' value='<?php echo $key; ?>'> </td>			
                <tr> 
                    <td> Amount : </td> 
                    <td> <input name='amount' type='text' value='<?php echo $_POST['total_amount']; ?>'> </td>
                <tr> 
                    <td> Order ID : </td> 
                    <td> <input name='orderid' type='text' value='<?php echo $_POST['order_id']; ?>'> </td>
            </table>
            <input type="submit" value="Submit">
        </form>
        <script type="text/javascript">
            document.getElementById("linkpost").submit();
        </script>
    </body>
</html>
