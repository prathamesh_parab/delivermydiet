<?php
/*
 * @version : 1.0 
 * 
 * @date-modified : 12/04/2016
 * 
 * @gateway-integrated-final : PayUmoney, PayUbiz, Quikwallet, Citrus, Paytm, CCavenue
 * 
 * @last-3-modified-lib : ccavenue_libs, PaytmKit, citrus_libs
 * 
 */

/*--------------------- PAY U MONEY CREDENTIALS ----------------------------*/
define('PAYUMONEY_MERCHANT_KEY', 'fyMbpOW0'); // gtKFFx MERCHANT KEY
define('PAYUMONEY_SALT', 'CyX2cIUzEa'); //eCwWELxi SALT
define('PAYUMONEY_REDIRECT_URL', 'http://localhost/vegex');  
define('PAYUMONEY_COMPANY_NAME', 'Veg Ex Product'); 
define('PAYUMONEY_ENVIRONMENT', 'PROD'); // PROD = Production or TEST = Testing
define('PAYUMONEY_SERVER', 'http://api.done.to'); // http://10.0.1.201:5000 or http://apiserver.done.to or http://api.done.to


/*--------------------- PAY U BIZ CREDENTIALS ----------------------------*/
define('PAYUBIZ_MERCHANT_KEY', 'gtKFFx'); // gtKFFx MERCHANT KEY
define('PAYUBIZ_SALT', 'eCwWELxi'); //eCwWELxi SALT
define('PAYUBIZ_REDIRECT_URL', 'http://order.maroosh.co.in');
define('PAYUBIZ_COMPANY_NAME', 'Maroosh Product');
define('PAYUBIZ_ENVIRONMENT', 'PROD');
define('PAYUBIZ_SERVER', 'http://api.done.to'); // http://10.0.1.201:5000 or http://apiserver.done.to or http://api.done.to


/*--------------------- QUIK WALLET CREDENTIALS ----------------------------*/
define('QUIKWALLET_MERCHANT_KEY', '3R8MRHIrTUNZk8c1f23P14X8NCzjYlwI');
define('QUIKWALLET_DEFAULT_OUTLET_ID', '1124');
define('QUIKWALLET_PARTNER_ID', '110');
define('QUIKWALLET_ENVIRONMENT', 'PROD');
define('QUIKWALLET_CLOSE_REDIRECT_URL', 'http://order.maroosh.co.in'); 
define('QUIKWALLET_SERVER', 'http://api.done.to'); // http://10.0.1.201:5000 or http://apiserver.done.to or http://api.done.to
/*
 *  '<done outlets>' => '<quikwallet outlets>',
 */
$mapped_outlets = array(
    '8850' => '1124',
    '8873' => '1125',
    '8890' => '1126',
    '8898' => '1127',
    '8875' => '1128',
    '8856' => '1129',
    '8874' => '1130',
    '8853' => '1131',
    '8899' => '1132',
    '9011' => '1133',
    '8891' => '1134',
    '8892' => '1135',
    '8913' => '1136',
    '8872' => '1137',
    '8911' => '1138',
    '8878' => '1139',
    '8912' => '1140',
    '8891' => '1141',
    '8909' => '1142');


/*--------------------- PAYTM CREDENTIALS ----------------------------*/
define('PAYTM1_MERCHANT_KEY', 'R62ZFOFPDA17TET9');
define('PAYTM1_MERCHANT_MID', 'HEALTH29776461935958');
define('PAYTM1_MERCHANT_WEBSITE', 'WEB_STAGING');
define('PAYTM1_INDUSTRY_TYPE_ID', 'Retail');
define('PAYTM1_CHANNEL_ID', 'WEB');
define('PAYTM1_ENVIRONMENT', 'TEST'); //If Production = PROD or else Testing = TEST
define('PAYTM1_CLOSE_REDIRECT_URL', 'http://apiserver.done.to/delivermydiet');
define('PAYTM1_SERVER', 'http://apiserver.done.to'); // http://10.0.1.201:5000 or http://apiserver.done.to or http://api.done.to


/*--------------------- CCAVENUE CREDENTIALS ----------------------------*/
define('CCAVENUE_MERCHANT_ID', '90938'); //Merchant ID Provided by ccavenue
define('CCAVENUE_WORKING_KEY', '3A09A7186EE59FE41C39A450F334F23D'); // Working Key Provided by ccavenue
define('CCAVENUE_MOBILE_WORKING_KEY', '70A0E9E5AFB2A8690A01A9C7CDFAAC8A'); // Working Key Provided by ccavenue
define('CCAVENUE_ACCESS_CODE', 'AVKI64DB14BU57IKUB'); // Access Code Provided by ccavenue
define('CCAVENUE_REDIRECT_URL', 'http://api.done.to/yumpys'); //Redirect
define('CCAVENUE_ENVIRONMENT', 'PROD'); //If Production = PROD or else Testing = TEST
define('CCAVENUE_SERVER', 'http://api.done.to'); // http://10.0.1.201:5000 or http://apiserver.done.to or http://api.done.to


/*--------------------- CITRUS CREDENTIALS  ----------------------------*/
define('CITRUS_ACCESS_KEY', '61da981b3abaeff08b3c54b97a58ae69b2493ac1'); // Provide by Citrus
define('CITRUS_RESPONSE_URL', 'http://api.done.to/eatsome_new');  // Provided by us
define('CITRUS_VANITY_URL', 'Eatsome'); // Provided by Citrus
define('CITRUS_SERVER', 'http://api.done.to'); // http://10.0.1.201:5000 or http://apiserver.done.to or http://api.done.to
define('CITRUS_SERVER_TO_SERVER', 'https://api.done.to/eatsome_new'); // Use HTTPS provided by us



?>