<?php
require_once './payu_libs/payu.php';

function payment_success() {
    /* Payment success logic goes here. */
    echo "Congratulations !! The Payment is successful.";
}

function payment_failure() {
    /* Payment failure logic goes here. */
    echo "We are sorry. The Payment has failed";
}

if (isset($_POST['from_client'])) {
    $TxnId = $_POST['txnid'];
    $total_amount = $_POST['amount'];
    $first_name = $_POST['firstname'];
    $email = $_POST['email'];
    $mobile = $_POST['phone'];
    $product_info = $_POST['productinfo'];
    $is_new_user = $_POST['udf1'];
    $order_id = $_POST['udf2'];
    $user_id = $_POST['udf3'];
    $order_number = $_POST['udf4'];
    pay_page(array('key' => 'gtKFFx', 'txnid' => $TxnId, 'amount' => $total_amount,
        'firstname' => $first_name, 'email' => $email, 'phone' => $mobile,
        'productinfo' => $product_info, 'udf1' => $is_new_user, 'udf2' => $order_id, 'udf3' => $user_id, 'udf4' => $order_number, 'surl' => 'payment_success', 'furl' => 'payment_failure'), 'eCwWELxi');
} else if (isset($_POST['from_js'])) {
    ?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
        <head>
            <title>Payu Payments</title>
        </head>

        <body>
            <h2> Payu Payments Example </h2> <hr />
            <form method='POST' id="transactionForm" action="example_2.php">
                <table border='0'>
                    <tr> <td> from_client : </td> <td> <input name='from_client' type='hidden' value='1'> </td>
                    <!--<tr> <td> Key : </td> <td> <input name='key' type='text' value='tradus'> </td>-->
                    <tr> <td> Transaction Id : </td> <td> <input name='txnid' type='text' value='<?php echo $_POST['txn_id']; ?>'> </td>			
                    <tr> <td> Amount : </td> <td> <input name='amount' type='text' value='<?php echo $_POST['total_amount']; ?>'> </td>
                    <tr> <td> Firstname : </td> <td> <input name='firstname' type='text' value='<?php echo $_POST['first_name']; ?>'> </td>
                    <tr> <td> Email : </td> <td> <input name='email' type='text' value='<?php echo $_POST['email']; ?>'> </td>
                    <tr> <td> Phone : </td> <td> <input name='phone' type='text' value='<?php echo $_POST['mobile']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='productinfo' type='text' value='Just another test Product'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='udf1' type='text' value='<?php echo $_POST['is_new_user']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='udf2' type='text' value='<?php echo $_POST['order_id']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='udf3' type='text' value='<?php echo $_POST['user_id']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='udf4' type='text' value='<?php echo $_POST['order_number']; ?>'> </td>
                </table>
                <input type="submit" value="Submit">
            </form>
            <script type="text/javascript">
                document.getElementById("transactionForm").submit();
            </script>
        </body>
    </html>

    <?php
} else {

    function callWebService($url, $methodType, $data) {
        if ($data != null)
            $data = json_encode($data, JSON_NUMERIC_CHECK);
        $headers = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data)
        );
        $ch = curl_init($url);
        if ($methodType) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
    
        $data = array();
        $data['order_id'] = $_POST['udf2'];
        $data['txn_status'] = $_POST['status'];
        $data['txn_id'] = $_POST['txnid'];
        $data['txn_message'] = $_POST['unmappedstatus'];
        $data['txn_payment_mode'] = $_POST['mode'];
        $data['txn_time'] = $_POST['addedon'];
        $data['client-platform'] = $_POST['platform'];
        $data['new_user'] = $_POST['udf1'];
//$res = callWebService('http://10.0.0.26:5000/done-save-transaction', true, $data);
        $res = callWebService('http://api.done.to:5000/done-save-transaction', true, $data);
    

?>
    <!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" ><![endif]-->
<html class="no-js" lang="en" ng-app="done">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">

        <title>Reeti fashions</title>
        <!--<link rel="icon" href="img/.png">-->

        <!-- Other styles -->
        <link rel="stylesheet" href="css/animate.css">

        <!-- Fonts -->
        <link rel="stylesheet" href="font-awesome-4.1.0/css/font-awesome.min.css">
        <link href="http://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet" type="text/css">
        
        <!-- Foundation styles -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/foundation.min.css">
        <link rel="stylesheet" href="css/jquery.bxslider.css">
        <link rel="stylesheet/less" type="text/css" href="css/done.less" />
        <script>
            less = {
                env: "production"
            };
        </script>
        <script src="js/less-1.7.3.min.js" type="text/javascript"></script>

        <script src="js/vendor/modernizr.js"></script>
    </head>
    <body>

      
        <div id="slider" class="hide-for-small-only"></div>

        <div id="alert-container">
            <div data-alert class="alert-box alert radius" style="display: none;">
                <span class="alert-message">Alert!</span>
                <a class="close">&times;</a>
            </div>
            <div data-alert class="alert-box warning radius" style="display: none;">
                <span class="alert-message">Alert!</span>
                <a class="close">&times;</a>
            </div>
            <div data-alert class="alert-box success radius" style="display: none;">
                <span class="alert-message">Alert!</span>
                <a class="close">&times;</a>
            </div>
        </div>



        <div class="fixed hide-for-small-only">
            <nav class="top-bar" data-topbar>
                <div class="small-2 columns">


                    <ul class="title-area left">
                        
                        <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                        <li class="toggle-topbar menu-icon"><a href="#" ng-click="$event.preventDefault();"><span>Site Menu</span></a></li>
                    </ul>
                </div>
                <div class="small-10 columns">
                    <section class="top-bar-section">

                        <ul class="left">
                            <li ><a ng-href="#">Outlets</a></li>      
                            <li ><a ng-href="#">Menu</a></li>      
                            <li ><a ng-href="#">Checkout</a></li>      
                            <li ><a ng-href="#">Order History</a></li>      
                            <li ><a ng-href="#">About Us</a></li>      
                            <li ><a ng-href="#">Contact Us</a></li>      
                        </ul>

                    </section>

                    <ul class="left">
                        <li>
                            <div id="social-links-container" class="small-centered hide-for-small-only">
                                <div id="fb-icon" ng-hide="verified_facebook_link === ''" onclick="show_social_link('facebook_link')"><i class="fa fa-facebook"></i></div>

                                <div id="tweet-icon" ng-hide="verified_twitter_link === ''" onclick="show_social_link('twitter_link')"><i class="fa fa-twitter"></i></div>

                                <!--<div id="android-icon" ng-hide="verified_android_link === ''" onclick="show_social_link('android_link')"><i class="fa fa-android"></i></div>-->

                                <!--<div id="ios-icon" ng-hide="verified_ios_link === ''" onclick="show_social_link('ios_link')"><i class="fa fa-apple"></i></div>-->

                            </div>
                        </li>
                    </ul>
                </div>
            </nav>


        </div>

       <div id="save-order-modal" class="reveal-modal small" data-reveal style="font-family: arial;">

        </div>


        <div class="footer footer-right copyrigthText hide-for-small-only"> &copy; 2015 all rights reserved by. <span class="title-text company-title"> Juice Lounge</span></div>
        <div class="footer footer-left copyrigthText hide-for-small-only"> Powered by <span class="title-text" ><a href="http://done.to/" target="_blank"> Done Solutions PVT LTD</a></span></div>


        <script src="js/vendor/jquery.js"></script>
        <script src="js/moment.min.js"></script>

        <script src="js/angular.min.js"></script>
        <script src="js/angular-animate.min.js"></script>
        <script src="js/angular-cookies.min.js"></script>
        <script src="js/angular-route.min.js"></script>

        <script src="js/done.js"></script>
        <script src="js/controllers/outletsCtrl.js"></script>
        <script src="js/controllers/menuCtrl.js"></script>
        <script src="js/controllers/checkoutCtrl.js"></script>
        <script src="js/controllers/orderhistoryCtrl.js"></script>
        <script src="js/controllers/aboutusCtrl.js"></script>
        <script src="js/controllers/contactusCtrl.js"></script>

        <script src="js/foundation.min.js"></script>
        <script src="js/jquery.backstretch.min.js"></script>
        <script>
            $(document).foundation();
<?php if (strtolower($_POST['status']) === "success") { ?>

                $("#save-order-modal").html("<div class=\"row panel\">Congratulations ! , Your Order was successful </div><div class=\"row panel\">Please Note your transaction id <strong><?php echo $_POST['txnid']; ?></strong>, May help you for future reference<br/><div>Order Id is : <?php echo $_POST['udf4']; ?></div></div><div class=\"row panel\">You May also call on our respective outlet numbers</div><a class=\"close-reveal-modal\">&#215;</a>");
                $("#save-order-modal").foundation("reveal", "open");
      $(document).on('closed.fndtn.reveal', '[data-reveal]', function () {
                    localStorage.removeItem("cart");
                    window.open("http://www.reetifashions.com/", "_self");
                });

<?php } else { ?>

                $("#save-order-modal").html("<div class=\"row panel\">Uh! oh!. We could not process the payment.</div><div class=\"row panel\">Please Note your transaction id <strong><?php echo $_POST['txnid']; ?></strong>, May help you for future reference</div><div class=\"row panel\">You May also call on our respective outlet numbers</div><a class=\"close-reveal-modal\">&#215;</a>");
                $("#save-order-modal").foundation("reveal", "open");
                $(document).on('closed.fndtn.reveal', '[data-reveal]', function () {
                     window.open("http://www.reetifashions.com/", "_self");
                });
<?php } ?>

        </script>
    <?php
   
}

/* And we are done. */

?>