<?php

function callWebService($url, $methodType, $data) {
    if ($data != null)
        $data = json_encode($data, JSON_NUMERIC_CHECK);
    $headers = array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data)
    );
    $ch = curl_init($url);
    if ($methodType) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}
//print_r($_POST);
$response = explode("|",$_POST['responseparams']);
$otherdata = explode(",",$response[4]); 
//print_r($otherdata);
//exit;
$data = array();
$data['order_id'] = $otherdata[0];
$data['txn_status'] = "Success";
$data['txn_id'] = $otherdata[1];
$data['txn_message'] = "Transaction Success";
$data['txn_payment_mode'] = "";
$data['txn_time'] = date();
$data['client-platform'] = $otherdata[2];
$data['new_user'] = $otherdata[3];
$res = callWebService('http://apiserver.done.to/done-save-transaction', true, $data);
?>


<html class="no-js" lang="en" ng-app="done">
    <head>
    </head>
    <body>
        <script type="text/javascript">

                var txnid = '<?php echo $data['txn_id']; ?>';
                var type = 0;
                var msg = "Your Order was successful placed";
            function handleResponse() {
                Android.handleResponse(txnid, type, msg);
            }
            handleResponse();
        </script>
        <?php
        ?>
    </body>
</html>
