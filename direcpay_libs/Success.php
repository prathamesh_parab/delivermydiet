<?php

function callWebService($url, $methodType, $data) {
    if ($data != null)
        $data = json_encode($data, JSON_NUMERIC_CHECK);
    $headers = array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data)
    );
    $ch = curl_init($url);
    if ($methodType) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}

date_default_timezone_set("Asia/Calcutta");
$current_timestamp = date("Y-m-d H:i:s");
$server = DIRECPAY_SERVER;

$response = explode("|",$_POST['responseparams']);
$otherdata = explode(",",$response[4]); 
//print_r($otherdata);
//exit;
$data = array();
$data['order_id'] = $otherdata[0];
$data['txn_status'] = "Success";
$data['txn_id'] = $otherdata[1];
$data['txn_message'] = "Transaction Success";
$data['txn_payment_mode'] = "direcpay";
$data['txn_time'] = $current_timestamp;
$data['client-platform'] = $otherdata[2];
$data['new_user'] = $otherdata[3];
$res = callWebService($server.'/done-save-transaction', true, $data);
?>

    <div id="myModal1" class='modal fade' role='dialog'>

    </div>
    <div style="height: 100%;width: 100%; background: url('../img/logo.png');opacity: 0.2;">

    </div>


    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet/less" type="text/css" href="../css/main.less" />
    <link rel="stylesheet" href="../css3-animate-it-master/css/animations.css" type="text/css" />
    <!--[if lte IE 9]>
        <link href='css3-animate-it-master/css/animations-ie-fix.css' rel='stylesheet'/>
    <![endif]-->
    <link rel="stylesheet" href="../css/jquery-ui.css">
    <link rel="stylesheet" href="../font-awesome-4.1.0/css/font-awesome.min.css">

    <script src="../js/less-1.7.5.min.js" type="text/javascript"></script>
    <script src="../js/dynamicCSS.js" type="text/javascript"></script>
    <!-- jquery -->
    <script src="../js/jquery-1.11.3.min.js"></script>
    <script src="../js/jquery-1.11.4-ui.js"></script>
    <!-- Bootstrap -->
    <script src="../js/bootstrap.min.js"></script>
    <!-- custom js> -->
    <script src="../js/angular.min.js"></script>
    <script src="../js/angular-animate.min.js"></script>
    <script src="../js/angular-cookies.min.js"></script>
    <script src="../js/angular-route.min.js"></script>
    <script src="../js/angular-touch.js"></script> 
    <script src="../js/jquery.backstretch.min.js"></script>
    <script src="../js/moment.min.js"></script>

    <script type="text/javascript" src="../fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
    <script src='../css3-animate-it-master/js/css3-animate-it.js'></script>



    <script>
    <?php if (strtolower($data['txn_status']) === "success" || strtolower($data['txn_status']) === "paid") { ?>

                    $("#myModal1").html("<div><div class='modal-dialog'><div class='modal-content'><div class='modal-header' style='background-color: #CCC;'><button type='button' class='close' data-dismiss='modal'>&times;</button><h4 class='modal-title text-center'>Payment Status</h4></div><div class='modal-body text-center'><p>Congratulations ! , Your Order was successful.<br> Please Note your transaction id <strong><?php echo $data['txn_id']; ?></strong>, <br>May help you for future reference<br/>Order Id is : <?php echo $data['order_id']; ?> <br>You May also call on our respective outlet numbers</p></div><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");
                    $("#myModal1").modal('show');
                    $('#myModal1').on('hidden.bs.modal', function () {
                        localStorage.removeItem("cart");
                        window.open("<?php echo $Redirect_URL; ?>", "_self");
                    });

    <?php } else { ?>
    
                    $("#myModal1").html("<div ><div class='modal-dialog'><div class='modal-content'><div class='modal-header' style='background-color: #CCC;'><button type='button' class='close' data-dismiss='modal'>&times;</button><h4 class='modal-title text-center'>Payment Status</h4></div><div class='modal-body text-center'><p>Sorry ! We could not process the payment.<br> Please Note your transaction id <strong><?php echo $data['txn_id']; ?></strong>, <br>May help you for future reference<br/>Order Id is : <?php echo $data['order_id']; ?> <br>You May also call on our respective outlet numbers</p></div><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");
                    $('#myModal1').modal('show');
                    $('#myModal1').on('hidden.bs.modal', function () {
                        localStorage.removeItem("cart");
                        window.open("<?php echo $Redirect_URL; ?>", "_self");
                    });
    <?php } ?>
    </script>
    <?php

/* And we are done. */
?>


