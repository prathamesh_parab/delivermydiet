﻿<?php
$data = array();
$data['order_id'] = $_POST['orderid'];
$data['txn_status'] = $_POST['TxStatus'];
$data['txn_id'] = $_POST['TxId'];
$data['txn_message'] = $_POST['TxMsg'];
$data['txn_payment_mode'] = $_POST['paymentMode'];
$data['txn_time'] = $_POST['txnDateTime'];
$data['new_user'] = $_POST['newuser'];
//$res = callWebService('http://10.0.1.197:5000/done-save-transaction', true, $data);
$res = callWebService('http://api.done.to:5000/done-save-transaction', true, $data);

function callWebService($url, $methodType, $data) {
    if ($data != null)
        $data = json_encode($data,JSON_NUMERIC_CHECK);
    $headers = array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data)
    );
    $ch = curl_init($url);
    if ($methodType) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}

?>
