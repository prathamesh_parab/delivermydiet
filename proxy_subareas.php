<?php

$latitude = $_GET["latitude"];
$longitude = $_GET["longitude"];
echo callWebService('http://api.done.to:8983/solr/subareas/select?wt=json&indent=true&fl=name,store&q=*:*&sfield=latlng&pt=' . $latitude . ',' . $longitude . '&sort=geodist()%20asc&fl=dist:geodist(),subarea_name,id,area_id,area_name,city_id,city_name&start=0&rows=1');

function callWebService($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}