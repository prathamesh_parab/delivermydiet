<html>
    <head>
        <title> Non-Seamless-kit</title>
    </head>
    <body>
    <center>

        <?php include('Crypto.php') ?>
        <?php
        include "../dynamicVAR.php";
        error_reporting(0);

        $merchant_data = '';
        $working_key = CCAVENUE_WORKING_KEY; //Shared by CCAVENUES // //258119F2F46777BBBA991CCDC720B5C7
        $access_code = CCAVENUE_ACCESS_CODE; //Shared by CCAVENUES // //AVAF00CH23BP80FAPB
        $enviroment = CCAVENUE_ENVIRONMENT;

        if ($enviroment == "TEST") {
            $post_action = 'http://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
        } else {
            $post_action = 'https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction'; // Production URL
        }

        foreach ($_POST as $key => $value) {
            $merchant_data.= $key . '=' . $value . '&';
        }

        $encrypted_data = encrypt($merchant_data, $working_key); // Method for encrypting the data.
        ?>
        <form method="post" id="redirect" name="redirect" action='<?php echo $post_action; ?>' style="display: none;"> 
            <?php
            echo "<input type=text name=encRequest value=$encrypted_data>";
            echo "<input type=text name=access_code value=$access_code>";
            ?>
        </form>
    </center>
    <script language='javascript'>document.getElementById('redirect').submit();</script>
</body>
</html>


<!-- http://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction -->
<!-- https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction -->