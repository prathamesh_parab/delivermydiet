<?php
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");

session_start();
// following files need to be included

require_once ('./lib/config_paytm.php');
require_once ('./lib/encdec_paytm.php');
include "../dynamicVAR.php";

date_default_timezone_set("Asia/Calcutta");
$Redirect_URL = PAYTM1_RESPONSE_URL;
$current_timestamp = date("Y-m-d H:i:s");
$payment_mode = "NA";
$payment_status = "Failed";

function callWebService($url, $methodType, $data) {
//    echo "in callwebservice function";
    if ($data != null)
        $data = json_encode($data, JSON_NUMERIC_CHECK);
    $headers = array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data)
    );
    $ch = curl_init($url);
    if ($methodType) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}
/* * ******************** For TESTING PURPOSE with Text **************** */

$file = 'test.txt';
$current = file_get_contents($file);
$current .= json_encode($_POST) . "input";
echo file_put_contents($file, $current);

/* * ************************************ */
//print_r($_POST);
$paytmChecksum = "";
$paramList = array();
$isValidChecksum = "FALSE";

$paramList = $_POST;
$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg
//echo $isValidChecksum;
$isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.

if ($isValidChecksum === TRUE) {


    if ($_POST['STATUS'] === "TXN_SUCCESS") {
        $payment_mode = "Paytm Wallet";
        $status = "Success";
    } else {
        $payment_mode = "NA";
        $status = "Failed";
    }
    $data_array = explode("|", $_POST['MERC_UNQ_REF']);
    $orderinfo = explode("|", $_POST['ORDERID']);
    $txn_id = $data_array[0];
    $new_user = $data_array[1];
    
    $data = array();
    $data['order_id'] = $orderinfo[0];
    $data['txn_status'] = $status;
    $data['txn_id'] = $orderinfo[1];
    $data['txn_message'] = $status;
    $data['txn_payment_mode'] = $payment_mode;
    $data['txn_time'] = $current_timestamp;
    $data['client-platform'] = "web";
    $data['new_user'] = $new_user;

    echo "<pre>";print_r($data);
    $res = callWebService('http://10.0.1.197:5000/done-save-transaction', true, $data);
//    $res = callWebService($server.'/done-save-transaction', true, $data);
    ?>

    <div id="myModal1" class='modal fade' role='dialog'>

    </div>
    <div style="height: 100%;width: 100%; background: url('../img/logo.png');opacity: 0.2;">

    </div>


    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet/less" type="text/css" href="../css/main.less" />
    <link rel="stylesheet" href="../css3-animate-it-master/css/animations.css" type="text/css" />
    <!--[if lte IE 9]>
        <link href='css3-animate-it-master/css/animations-ie-fix.css' rel='stylesheet'/>
    <![endif]-->
    <link rel="stylesheet" href="../css/jquery-ui.css">
    <link rel="stylesheet" href="../font-awesome-4.1.0/css/font-awesome.min.css">

    <script src="../js/less-1.7.5.min.js" type="text/javascript"></script>
    <!--<script src="../js/dynamicCSS.js" type="text/javascript"></script>-->
    <!-- jquery -->
    <script src="../js/jquery-1.11.3.min.js"></script>
    <script src="../js/jquery-1.11.4-ui.js"></script>
    <!-- Bootstrap -->
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/bootstrap-select.js"></script>    
    <!-- custom js> -->
    <script src="../js/angular.min.js"></script>
    <script src="../js/angular-animate.min.js"></script>
    <script src="../js/angular-cookies.min.js"></script>
    <script src="../js/angular-route.min.js"></script>
    <script src="../js/angular-touch.js"></script> 
    <!--<script src="../js/jquery.backstretch.min.js"></script>-->
    <script src="../js/moment.min.js"></script>

            <!--<script type="text/javascript" src="../fancybox/source/jquery.fancybox.js?v=2.1.5"></script>-->
            <!--<script src='../css3-animate-it-master/js/css3-animate-it.js'></script>-->



    <script>
    <?php if (strtolower($status) === "success" || strtolower($status) === "paid") { ?>

            $("#myModal1").html("<div><div class='modal-dialog'><div class='modal-content'><div class='modal-header' style='background-color: #CCC;'><button type='button' class='close' data-dismiss='modal'>&times;</button><h4 class='modal-title text-center'>Payment Status</h4></div><div class='modal-body text-center'><p>Congratulations ! , Your Order was successful.<br> Please Note your transaction id <strong><?php echo $data['txn_id']; ?></strong>, <br>May help you for future reference<br/>Order Id is : <?php echo $_SESSION['order_number']; ?> <br>You May also call on our respective outlet numbers</p></div><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");
            $("#myModal1").modal('show');
            $('#myModal1').on('hidden.bs.modal', function () {
                localStorage.removeItem("cart");
                window.open("<?php echo $Redirect_URL; ?>", "_self");
            });

    <?php } else { ?>
            $("#myModal1").html("<div ><div class='modal-dialog'><div class='modal-content'><div class='modal-header' style='background-color: #CCC;'><button type='button' class='close' data-dismiss='modal'>&times;</button><h4 class='modal-title text-center'>Payment Status</h4></div><div class='modal-body text-center'><p>Sorry ! We could not process the payment.<br> Please Note your transaction id <strong><?php echo $data['txn_id']; ?></strong>, <br>May help you for future reference<br/>Order Id is : <?php echo $_SESSION['order_number']; ?> <br>You May also call on our respective outlet numbers</p></div><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");
            $('#myModal1').modal('show');
            $('#myModal1').on('hidden.bs.modal', function () {
                localStorage.removeItem("cart");
                window.open("<?php echo $Redirect_URL; ?>", "_self");
            });
    <?php } ?>
    </script>
    <?php
}
/* And we are done. */
?>
