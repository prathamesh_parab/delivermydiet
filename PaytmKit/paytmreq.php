<?php

/*
 * @version 1.0
 */

header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");

include "../dynamicVAR.php";
session_start();



$ORDER_ID =  $_POST["order_id"]."_".$_POST['txn_id']."_".$_POST['is_new_user'];
$CUST_ID = $_POST["user_id"];
$INDUSTRY_TYPE_ID = PAYTM1_INDUSTRY_TYPE_ID;
$CHANNEL_ID = PAYTM1_CHANNEL_ID;
$_SESSION["TXN_AMOUNT"]=$TXN_AMOUNT = $_POST["total_amount"];
$isnewuser = $_POST["is_new_user"];
$MOBILE_NO = $_POST["MOBILE_NO"];
$EMAIL = $_POST["EMAIL"];
$TXN_ID = $_POST["txn_id"];
$MERC_UNQ_REF = $_POST["order_id"]."_".$_POST['txn_id']."_".$_POST['is_new_user'];
$Callback_Url = "http://apiserver.done.to/delivermydiet/PaytmKit/paytm_callback.php";


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Merchant Check Out Page</title>
        <meta name="GENERATOR" content="Evrsoft First Page">
    </head>
    <body>
        <h1>Merchant Check Out Page</h1>

        <form method="post" action="PaytmKit/pgRedirect.php" name="f2">
            <table border="1">
                <tbody>
                    <tr>
                        <th>S.No</th>
                        <th>Label</th>
                        <th>Value</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><label>ORDER_ID::*</label></td>
                        <td><input id="ORDER_ID" tabindex="1" maxlength="20" size="20"
                                   name="ORDER_ID" autocomplete="off"
                                   value="<?= $ORDER_ID ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><label>CUSTID ::*</label></td>
                        <td><input id="CUST_ID" tabindex="2" maxlength="12" size="12" name="CUST_ID" autocomplete="off" value="<?= $CUST_ID ?>"></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><label>INDUSTRY_TYPE_ID ::*</label></td>
                        <td><input id="INDUSTRY_TYPE_ID" tabindex="4" maxlength="12" size="12" name="INDUSTRY_TYPE_ID" autocomplete="off" value="<?= $INDUSTRY_TYPE_ID ?>"></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><label>Channel ::*</label></td>
                        <td><input id="CHANNEL_ID" tabindex="4" maxlength="12"
                                   size="12" name="CHANNEL_ID" autocomplete="off" value="<?= $CHANNEL_ID ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><label>Callback_Url ::*</label></td>
                        <td><input id="Callback_Url" tabindex="4" 
                                    name="Callback_Url" autocomplete="off" value="<?= $Callback_Url ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td><label>txnAmount*</label></td>
                        <td><input title="TXN_AMOUNT" tabindex="10"
                                   type="text" name="TXN_AMOUNT"
                                   value="<?= $TXN_AMOUNT ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td><label>mobile No.*</label></td>
                        <td><input title="MOBILE_NO" tabindex="10"
                                   type="text" name="MOBILE_NO"
                                   value="<?= $MOBILE_NO ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td><label>emailid*</label></td>
                        <td><input title="EMAIL" tabindex="10"
                                   type="text" name="EMAIL"
                                   value="<?= $EMAIL ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td><label>IsnewUser*</label></td>
                        <td><input title="IS_NEW_USER" tabindex="10"
                                   type="text" name="IS_NEW_USER"
                                   value="<?= $isnewuser ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td><label>MERC_UNQ_REF*</label></td>
                        <td><input title="MERC_UNQ_REF" tabindex="10"
                                   type="text" name="MERC_UNQ_REF"
                                   value="<?= $MERC_UNQ_REF ?>">
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><input value="CheckOut" type="submit"	onclick=""></td>
                    </tr>
                </tbody>
            </table>
            <script type="text/javascript">
                document.f2.submit();
            </script>
            * - Mandatory Fields
        </form>
    </body>
</html>
