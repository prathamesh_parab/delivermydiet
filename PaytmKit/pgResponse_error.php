<?php
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");

// following files need to be included
require_once("./PaytmKit/lib/config_paytm.php");
require_once("./PaytmKit/lib/encdec_paytm.php");

function callWebService($url, $methodType, $data) {
//    echo "in callwebservice function";
    if ($data != null)
        $data = json_encode($data, JSON_NUMERIC_CHECK);
    $headers = array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data)
    );
    $ch = curl_init($url);
    if ($methodType) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}

$paytmChecksum = "";
$paramList = array();
$isValidChecksum = "FALSE";

$paramList = $_POST;
$return_array = $_POST;
$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg
//echo $isValidChecksum;
$isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.

if ($isValidChecksum === TRUE) {

    session_start();
//    print_r($_SESSION);

    $data = array();
    $data['order_id'] = $_POST['ORDERID'];
    if ($_POST['STATUS'] === "TXN_SUCCESS") {
        $data['txn_status'] = "SUCCESS";
    } else {
        $data['txn_status'] = $_POST['STATUS'];
    }

    $return_array["TXNTYPE"] = "";
    $return_array["REFUNDAMT"] = "";
    unset($return_array["CHECKSUMHASH"]);

    $encoded_json = htmlentities(json_encode($return_array));

    if(!isset($_SESSION['IS_NEW_USER'])){ ?>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-I">
        <title>Paytm</title>
        <script type="text/javascript">
            function response(){
                return document.getElementById('response').value;
            }
        </script>
    </head>
    <body>
        Redirect back to the app<br>

        <form name="frm" method="post">
            <input type="hidden" id="response" name="responseField" value='<?= $encoded_json?>'>
        </form>
    </body>
    </html>
    <?php    
    exit;   }

    $data['txn_message'] = $_POST['RESPMSG'];
    $data['txn_payment_mode'] = $_POST['PAYMENTMODE'];
    $data['txn_time'] = $_POST['TXNDATE'];
    $data['new_user'] = $_SESSION['IS_NEW_USER'];
    $data['txn_id'] = $_SESSION['TXN_ID'];
//    echo "hiii";
//    $res = callWebService('http://10.0.1.197:5000/done-save-transaction', true, $data);

    $res = callWebService('http://api.done.to/done-save-transaction', true, $data);
//    print_r($res);
    ?>

    <!DOCTYPE html>
    <!--[if IE 9]><html class="lt-ie10" lang="en" ><![endif]-->
    <html class="no-js" lang="en" ng-app="done">
        <head>
            <meta charset="utf-8">

            <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">

            <title>Eatsome.</title>
            <link rel="icon" href="img/Eatsome-logo-mini_1.png">

            <!-- Other styles -->
            <link rel="stylesheet" href="css/animate.css">

            <!-- Fonts -->
            <link rel="stylesheet" href="font-awesome-4.1.0/css/font-awesome.min.css">
            <link href="http://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet" type="text/css">

            <!-- Foundation styles -->
            <link rel="stylesheet" href="css/normalize.css">
            <link rel="stylesheet" href="css/foundation.min.css">

            <link rel="stylesheet/less" type="text/css" href="css/done.less" />
            <script>
                less = {
                    env: "production"
                };
            </script>
            <script src="js/less-1.7.3.min.js" type="text/javascript"></script>

            <script src="js/vendor/modernizr.js"></script>
        </head>
        <body>

            <div class="small-12 columns header">
                <div class="medium-2 columns">
                    <img class="logo-small" src="img/Eatsome-logo-mini.png" alt="Eatsome Mini logo">
                </div>

            </div>
            <div id="save-order-modal" class="reveal-modal small" data-reveal>

            </div>
            <div class="fixed">
                <nav class="top-bar" data-topbar>
                    <ul class="title-area">
                        <li class="name"></li>
                        <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                        <li class="toggle-topbar menu-icon"><a href="#" ng-click="$event.preventDefault();"><span>Site Menu</span></a></li>
                    </ul>
                    <section class="top-bar-section">
                        <ul class="left">
                            <li ng-repeat="page in pages" ng-class="{active: page.route === page_active}"><a ng-href="#{{page.route}}">{{page.label}}</a></li>      
                            <li ><a href="http://eatsome.in/about-us.html" target="_blank">About Us</a></li>      
                        </ul>
                        <div class="left terms-condition">
                            <a ng-href="#/terms">Terms & Conditions</a>
                            <a ng-href="#/faq">FAQ</a>
                            <a ng-href="#/privacypolicy">Privacypolicy</a>
                            <a ng-href="#/disclaimers">Disclaimers</a>
                        </div>
                        <ul id="social-links-container">
                            <li onclick="window.open('https://play.google.com/store/apps/details?id=done.mobile.eatsome')">
                                <img src="img/android.png" alt="android">
                            </li>
                            <li onclick="window.open('https://itunes.apple.com/us/app/eatsome/id929610729?mt=8')">
                                <img src="img/apple.png" alt="apple">
                            </li>
                            <li onclick="window.open('https://www.facebook.com/')">
                                <img src="img/facebook.png" alt="Facebook">
                            </li>
                            <li onclick="window.open('https://www.twitter.com/')">
                                <img src="img/twitter.png" alt="Twitter">
                            </li>
                        </ul>   

                    </section>
                </nav>
            </div>
            <script src="js/vendor/jquery.js"></script>
            <script src="js/moment.min.js"></script>

            <script src="js/angular.min.js"></script>
            <script src="js/angular-animate.min.js"></script>
            <script src="js/angular-cookies.min.js"></script>
            <script src="js/angular-route.min.js"></script>

            <script src="js/done.js"></script>
            <script src="js/controllers/outletsCtrl.js"></script>
            <script src="js/controllers/menuCtrl.js"></script>
            <script src="js/controllers/checkoutCtrl.js"></script>
            <script src="js/controllers/orderhistoryCtrl.js"></script>
            <script src="js/controllers/aboutusCtrl.js"></script>
            <script src="js/controllers/contactusCtrl.js"></script>

            <script src="js/foundation.min.js"></script>
            <script src="js/jquery.backstretch.min.js"></script>
            <script>
                                $(document).foundation();
            </script>
            <script>
                $(document).foundation();
    <?php if ($_POST['STATUS'] === 'TXN_SUCCESS') { ?>
                    localStorage.removeItem("cart");
                    $("#save-order-modal").html("<div class=\"row panel\">Congratulations ! , Your Order was successful </div><div class=\"row panel\">Please Note your transaction id <strong><?php echo $_SESSION['TXN_ID']; ?></strong>, May help you for future reference<br/><div>Order Id is : <?php echo $_POST['ordernumber']; ?></div></div><div class=\"row panel\">You May also call on our respective outlet numbers</div><a class=\"close-reveal-modal\">&#215;</a><img src=\"http://tracker.viralmint.com/sales/1426575350/<?php echo $_SESSION['total_amount']; ?>/<?php echo $_POST['ordernumber']; ?>?coupon_code=<?php echo $_SESSION['coupon_code']; ?>\" height=\"0\" width=\"0\" />\"");
                    $("#save-order-modal").foundation("reveal", "open");
                    $(document).on('close.fndtn.reveal', '[data-reveal]', function () {

                        //                    window.open("http://box8.in", "_self");
                        window.open("http://order.eatsome.in", "_self");
                    });
                    $(document).on('closed.fndtn.reveal', '[data-reveal]', function () {
                        //                    window.open("http://box8.in", "_self");
                        window.open("http://order.eatsome.in", "_self");
                    });

    <?php } else { ?>
                    console.log(JSON.stringify(<?php echo $res; ?>));
                    $("#save-order-modal").html("<div class=\"row panel\">Uh! oh!. We could not process the payment.</div><div class=\"row panel\">Please Note your transaction id <strong><?php echo $_SESSION['TXN_ID']; ?></strong>, May help you for future reference</div><div class=\"row panel\">You May also call on our respective outlet numbers</div><a class=\"close-reveal-modal\">&#215;</a>");
                    $("#save-order-modal").foundation("reveal", "open");


                    $(document).on('close.fndtn.reveal', '[data-reveal]', function () {

                        //                    window.open("http://box8.in", "_self");
                        window.open("http://order.eatsome.in/#/checkout", "_self");
                    });
                    $(document).on('closed.fndtn.reveal', '[data-reveal]', function () {
                        //                    window.open("http://box8.in", "_self");
                        window.open("http://order.eatsome.in/#/checkout", "_self");
                    });
    <?php } ?>
            </script>
        </body>
    </html>
<?php } ?>
