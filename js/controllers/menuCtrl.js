app.controller("menuCtrl", ["$http", "$location", "$scope", function ($http, $location, $scope) {
        if (localStorage.getItem("outlet") === null) {
            create_alert(1, "Please choose an outlet.", 3);
            $location.url("home");
        } else {


            //create_alert(2, "Please check outlet you want to order from..", 5);
            $scope.outlet = JSON.parse(localStorage.getItem("outlet"));

            $scope.outlet_timing = $scope.outlet.outlet_timing;
            $scope.outlet_id = $scope.outlet.id;
            $scope.all_categories = [];
            $scope.categories = [];
            $scope.sub_categories = [];
            $scope.all_products = [];
            $scope.products = [];
            $scope.item_count = 0;
            $scope.cart_total = 0;
            $scope.selected_sub_category = {};
            $scope.attributes = [];
            $scope.cart_items = [];
            $scope.ntok = "";
            $scope.customization_note = "";
            $scope.product_search_keyword = null;
            $scope.mobile_web_view = false;
            $scope.open_mobile_customization = false;
            $scope.with_image = false;
            $scope.menu_company_id = structure_config.company_id;
            $scope.outlet_minorder_validation = structure_config.outlet_minorder_validation;


            $scope.with_image_template = structure_config.with_image_template;
            $scope.side_category = structure_config.side_category;
            $scope.product_grid_size = structure_config.product_grid_size;
            $scope.done_host = structure_config.done_host;
            $scope.soldout = structure_config.soldout;

            $scope.subcategories = false;
            if (localStorage.getItem("cart") !== null) {
                $scope.cart_items = JSON.parse(localStorage.getItem("cart"));
            }

            $scope.choose_filter = function (attribute_name) {
//                if (attribute_name === "Veg") {
//                    $scope.product_search_keyword = attribute_name;
//                } else {
//                    $scope.product_search_keyword = attribute_name;
//                }

                  if (attribute_name === "All") {
                    $scope.product_search_keyword = null;
                } else {
                    $scope.product_search_keyword = attribute_name;
                    $scope.product_search_keyword_1 = attribute_name;
                }
            };

            if (structure_config.with_image_template === "true")
            {
                $scope.with_image = true;
            }


            $scope.check_date_time = function (outlet_timing)
            {
                var is_open = false;
                if (outlet_timing !== undefined)
                {
                    $.ajax({
                        url: "ws/validatestoretime.php",
                        method: "POST",
                        data: {
                            outlet_timing: outlet_timing
                        },
                        success: function (result) {
                            if (result == 1) {

                            } else {
                                create_alert(1, "Outlet Closed", 3);
                                $location.url("menu");
                            }
                        }});


                }
            }


            $scope.show_toast = function (item) {
                if (item.customization.length !== 0) {
//                    var feature_str="";
//                    angular.forEach(extra_feature,function(feature){
//                        
//                        feature_str=feature_str+ feature.name+", ";
//                    });
//                     $scope.customization_note=feature_str;
                    if (item.full_view_customization === true) {
                        item.full_view_customization = false;
                    } else {
                        item.full_view_customization = true;
                    }

                }
            };
            $(".category-container").height($(document).height());
            $scope.hide_toast = function () {

                $scope.customization_note = "";
            };

            $scope.get_current_stock = function () {
//                done-get-current-stock
                var request = {
                    "outlet_id": $scope.outlet_id

                };
                $http.post(done_host + "/done-get-current-stock", request, done_host_parameters).success(function (response) {
                    //create_alert(2, "Welcome!", 3);
                    if (development_mode) {
                        console.log(response);
                    }
                    $scope.show_splash_screen = false;
                    $scope.current_stock = response.dataArray;
                    $scope.get_product();


                }).error(function () {
                    create_alert(1, "It's taking longer than expected. Retry?", 3);
                    $scope.error_text = "It's taking longer than expected. Retry?";
                    $scope.show_splash_screen = true;
                });
            };
//            create_alert(2, "Loading...", 3);
            $scope.get_product = function () {
                $http.get(demongoon_host + "/menu/" + $scope.outlet_id, {
                    headers: {
                        client_platform: "web"
                    },
                    cache: true,
                    timeout: 5000
                }).success(function (response) {
                    if (development_mode) {
                        console.log(response);
                    }
                    var all_category = {
                        id: null,
                        parent_category_id: null,
                        name: "All",
                        description: "Showing all products across all categories served by this outlet.",
                        photo_url: null,
                        selection_min: 1,
                        selection_max: 1,
                        display: "0",
                        sr: -1
                    };

                    $scope.all_categories = response.categories;
                    $scope.all_products = response.products;
                    $scope.attribute_groups = response.attribute_groups;

//                    $scope.categories.push(all_category);

                    for (var i = 0; i < $scope.all_categories.length; i++) {
                        if ($scope.all_categories[i].display === "0" && $scope.all_categories[i].parent_category_id == null && $scope.all_categories[i].id !== 14871 || $scope.all_categories[i].parent_category_id == 0) {
                            $scope.categories.push($scope.all_categories[i]);
                        }

                        if ($scope.all_categories[i].display === "0" && $scope.all_categories[i].parent_category_id !== null && $scope.all_categories[i].id !== 14871) {
                            $scope.sub_categories.push($scope.all_categories[i]);

                        }
                    }
                    if ($scope.sub_categories.length > 0) {
//                    $scope.subcategories = true;
                    }
                    for (var i = 0; i < $scope.categories.length; i++) {
                        $scope.categories[i].sub_categories = [];
                        for (var j = 0; j < $scope.sub_categories.length; j++) {
                            if ($scope.sub_categories[j].parent_category_id === $scope.categories[i].id) {
                                $scope.categories[i].sub_categories.push($scope.sub_categories[j]);
                            }
                        }
                    }




                    $scope.selected_category = $scope.categories[0];
                    if (!$scope.selected_category.sub_categories) {
                        $scope.subcategories = false;
                    }else{
                        $scope.selected_sub_category = $scope.categories[0].sub_categories[0];
                    }
                    for (var i = 0; i < $scope.all_products.length; i++) {

                        $scope.all_products[i].in_cart_qty = 0;
                        $scope.all_products[i].attribute_images = [];
                        angular.forEach($scope.all_products[i].attributes, function (attribute) {

                            var photo_url = '';
                            if (attribute.photo_url !== null && attribute.photo_url !== undefined && attribute.photo_url !== '')
                            {
                                photo_url = 'http://api.done.to/images/' + $scope.menu_company_id + '/' + attribute.photo_url;
                            }
                            else if (attribute.name === 'Veg')
                            {
                                photo_url = "img/veg.png";
                            }
                            else if (attribute.name === 'Non-Veg')
                            {
                                photo_url = "img/non-veg.png";
                            }
                            if (photo_url !== '')
                            {
                                $scope.all_products[i].attribute_images.push(photo_url);
                            }
                            $scope.all_products[i].attribute_bg = null;
                            switch (attribute.name) {
                                case "Veg":
                                    $scope.all_products[i].attribute_bg = "img/veg.png";
                                    break;
                                case "Non-Veg":
                                    $scope.all_products[i].attribute_bg = "img/non-veg.png";
                                    break;
                                case "Exclusive":
                                    $scope.all_products[i].attribute_bg_Ex = "img/Exclusive.png";
                                    break;
                            }
                        });
                        var finalprice = parseInt($scope.all_products[i].price);

                        if ($scope.all_products[i].default_price !== null && $scope.all_products[i].default_price !== undefined)
                        {
   
                            var default_price_array = $scope.all_products[i].default_price.split(',');
                            //var default_price_array=default_price1.split(',');
                            for (var priceCount = 0; priceCount < default_price_array.length; priceCount++) {

                                finalprice = finalprice + parseInt(default_price_array[priceCount]);
                            }

                        }
                        $scope.all_products[i].final_product_price = finalprice;
                        $scope.all_products[i].quantity = 1;

                        angular.forEach($scope.cart_items, function (item) {
                            if ($scope.all_products[i].id === item.id)
                            {

                                $scope.all_products[i].in_cart_qty = $scope.all_products[i].in_cart_qty + item.quantity;
                            }

                        });
                        if ($scope.current_stock) {
                            for (var l = 0; l < $scope.current_stock.length; l++) {
                                if ($scope.current_stock[l].product_id === $scope.all_products[i].id) {
                                    $scope.all_products[i].current_stock = $scope.current_stock[l].current_stock;
                                }
                            }
                        }
                        
                         if($scope.cart_items){
                          
                          for(var e =0;e<$scope.cart_items.length;e++){
                              if($scope.cart_items[e].customization && $scope.cart_items[e].id === $scope.all_products[i].id){
                                  $scope.all_products[i].is_customization = true;
                              }
                          }
                      }
                      
                        //if ($scope.all_products[i].display === "0") {
                        if ($scope.all_products[i].parent_outlet_product_id === null && $scope.all_products[i].category_id !== 14871) {
                            $scope.products.push($scope.all_products[i]);
                        }

//                    create_alert(2, "Done", 1);

                    }
                    $(".dvContent").css("min-height", $(document).height() - 34);


                    setTimeout(function () {
                        scrolltotop();
                    }, 150);
                    $(".loader").css("display", "none");
                }).error(function () {
                    create_alert(1, "It's taking longer than expected. Retry?", 3);
                });
            };
//            $scope.change_category = function (category)
//            {
//                scrolltosection('dvMenus');
//                $scope.selected_category = category;
//                $scope.product_search_keyword = null;
//                if (category.sub_categories !== undefined && category.sub_categories !== null) {
//                    if (category.sub_categories.length > 0) {
//                        $scope.subcategories = true;
//                        $scope.selected_sub_category = $scope.sub_categories[0];
//                    } else {
//                        $("#dvCatList_Ver").css("left", "-300px");
//                        $scope.subcategories = false;
//                        $scope.selected_sub_category = {};
//                    }
//                }
//
//            };
//            $scope.change_subcategory = function (category) {
////                $("#dvCatList_Ver1").css("left", "-300px");
//                if (window.innerWidth < 1014) {
//                    $("#dvCatList_Ver").css("left", "-300px");
//                }else{
//                    $("#dvCatList_Ver").css("left", "0px");
//                }
//                $scope.selected_sub_category = category;
//                $scope.product_search_keyword = null;
//            };
//The following is the new code for parent-child categories in mobile view.
            $scope.change_category = function (category)
            {

//                scrolltosection('dvMenus');
                $scope.selected_category = category;

                $scope.product_search_keyword = null;

                if (category.sub_categories !== undefined && category.sub_categories !== null) {
                    if (category.sub_categories.length > 0) {

                        $scope.subcategories = true;
                        $scope.selected_sub_category = $scope.sub_categories[0];
                        $(".sub_category_list").css("display", "none");
                        $("#" + category.id).css("display", "block");
                    } else {
                        if ($(window).width() <= 990)
                        {
                            $("#dvCatList_Ver").css("left", "-300px");
                        }
                        $scope.subcategories = false;
                        $scope.selected_sub_category = {};
                    }
                } else {
                    if ($(window).width() <= 990)
                    {
                        $("#dvCatList_Ver").css("left", "-300px");
                    }
                }


            };
            $scope.change_subcategory = function (category) {

//                $("#dvCatList_Ver1").css("left", "-300px");
                if (window.innerWidth < 1014) {

                    $(".main_category").next().css("display", "none");
                    $("#dvCatList_Ver1").css("left", "-300px");
                } else {
                    
//                    $("#dvCatList_Ver1").css("left", "0px");
                }
                
                $scope.selected_sub_category = category;

                $scope.product_search_keyword = null;
            };


            $scope.toggleMenu = function (flag)
            {
                if (flag === true)
                {
                    $('.off-canvas-wrap').foundation('offcanvas', 'toggle', 'move-right');
                }
                $('html, body').animate({scrollTop: 0}, 500);
                return false;
            };

            $scope.customization_model_view = function (Open_model)
            {
//                customscroll();
                if ($scope.mobile_web_view === true)
                {
                    $scope.open_mobile_customization = Open_model;
                }
                else
                {
                    if (Open_model === true)
                    {
                        // $("#customization-modal").foundation("reveal", "open");
                        $('#customization-modal').bsModal('show');
                    }
                    else
                    {
                        $('#customization-modal').bsModal('hide');
                        //$("#customization-modal").foundation("reveal", "close");
                    }
                }
            };
            $scope.get_customizations = function (product, is_mobile_view) {

                $scope.mobile_web_view = is_mobile_view;
                $scope.selected_product = angular.copy(product);
                $scope.selected_product.total_price = $scope.selected_product.price;
                $scope.selected_product.customization = [];
                $scope.child_selected_product = undefined;
                if (product.parent_product) {
                    $http.get(demongoon_host + "/customizations/" + $scope.outlet_id + "/" + product.outlet_product_id).success(function (response) {
                        if (development_mode) {
                            console.log(response);
                        }
                        $scope.selected_product.all_customizations = response;
                        $scope.active_tab_id = response[0].id;
                        angular.forEach($scope.selected_product.all_customizations, function (extra_feature_product) {

                            angular.forEach(extra_feature_product.products, function (child_product_obj) {

                                if (child_product_obj.default_selected === 1) {
                                    if (child_product_obj.parent_product === true) {
                                        $scope.get_Child_customizations(child_product_obj);
                                    }
                                }

                            });

                        });
                        product.is_customization = true;

                        $scope.customization_model_view(true);
//                        $("#customization-modal").foundation("reveal", "open");
                    });
                } else {
                    $scope.add_to_cart($scope.selected_product);
                }
            };



            $scope.get_Child_customizations = function (product) {

                if (product.parent_product) {
                    $http.get(demongoon_host + "/customizations/" + $scope.outlet_id + "/" + product.outlet_product_id).success(function (response) {
//                        customscroll();
                        if (development_mode) {
                            console.log(response);
                        }
                        if ($scope.child_selected_product === undefined) {
                            $scope.child_selected_product = angular.copy(product);
                            $scope.child_selected_product.total_price = $scope.child_selected_product.price;
                            $scope.child_selected_product.all_customizations = response;
                            $scope.child_selected_product.all_customizations.product_id = product.id;
                        } else {

                            if ($scope.child_selected_product.all_customizations.product_id === product.id) {
                                $scope.child_selected_product = undefined;

                            } else {
                                $scope.child_selected_product = angular.copy(product);
                                $scope.child_selected_product.total_price = $scope.child_selected_product.price;
                                $scope.child_selected_product.all_customizations = response;
                                $scope.child_selected_product.all_customizations.product_id = product.id;

                            }
                        }


                    });

                }
            };
            $scope.process_customizations = function () {
                var customizations = [];
                var customization_price = 0;
                var customization_string = "";
                if ($scope.mobile_web_view === true)
                {
                    $("#mobile-customization-modal input:checked").each(function () {
                        customizations.push($(this).data("product"));
                        customization_price += $(this).data("product").price;
                        customization_string += $(this).data("product").name + ", ";
                    });
                }
                else {
                    $("#customization-modal input:checked").each(function () {
                        customizations.push($(this).data("product"));
                        customization_price += $(this).data("product").price;
                        customization_string += $(this).data("product").name + ", ";
                    });
                }

                var numberregex = new RegExp(/\d/);
                if (!numberregex.test($scope.selected_product.quantity)) {
                    $scope.selected_product.quantity = 1;
                }

                $scope.selected_product.customization = customizations;
                $scope.selected_product.selected_customization_string = customization_string;
                $scope.selected_product.full_view_customization = false;

                $scope.selected_product.price = $scope.selected_product.price + customization_price;
                $scope.selected_product.total_price = $scope.selected_product.price * $scope.selected_product.quantity;
                $scope.add_to_cart($scope.selected_product);
//                $scope.customization_model_view(false);
//                $("#customization-modal").foundation("reveal", "close");

            };

            $scope.add_to_cart = function (product) {
                var duplicate = false;

                var count_customization = 0;
                var selection_min = 0;
                var selection_max = 0;
                var isvalid_customizations = [];
                var isvalid = true;
                if (product.all_customizations !== undefined)
                {

                    for (var j = 0; j < product.all_customizations.length; j++)
                    {
                        if (product.all_customizations !== undefined)
                        {
                            selection_min = product.all_customizations[j].selection_min;
                            selection_max = product.all_customizations[j].selection_max;
                            count_customization = 0;
                            if (product.customization !== undefined) {
                                count_customization = 0;
                                for (l = 0; l < product.customization.length; l++) {

                                    if (product.customization[l].category_id === product.all_customizations[j].id) {
                                        count_customization = count_customization + 1;
                                    }
                                }
//                                  
                            } else {
                                count_customization = 0;
                            }

                            var error_selection = {
                                valid: false,
                                name: product.all_customizations[j].name,
                                selection_min: selection_min,
                                selection_max: selection_max

                            };
 
                            if (selection_min === 0 && selection_max === 0) {
                                error_selection.valid = true;

                                isvalid_customizations.push(error_selection);
                            }
                            else if (count_customization >= selection_min && count_customization <= selection_max) {
                                error_selection.valid = true;
                                //isvalid_customizations.push(true);
                                isvalid_customizations.push(error_selection);
                            } else {
                                //isvalid_customizations.push(false);
                                error_selection.valid = false;
                                isvalid_customizations.push(error_selection);
                            }
                        }
                    }
                }
                else
                {
                    var valid_selection = {
                        valid: true,
                        name: "",
                        selection_min: "",
                        selection_max: ""

                    };
                    isvalid_customizations.push(valid_selection);
                }
                var error_customization_selection = null;
                for (var k = 0; k < isvalid_customizations.length; k++)
                {
                    if (isvalid_customizations[k].valid === true)
                    {
                        isvalid = true;
                    }
                    else
                    {
                        error_customization_selection = isvalid_customizations[k];
                        isvalid = false;
                        break;
                    }

                }
//                    if((count_customization === 0) ||(count_customization >= selection_min && count_customization <= selection_max))
                if (isvalid)
                {

                    for (var i = 0; i < $scope.cart_items.length; i++) {
                        // Copy original quantity value
                        var qty = $scope.cart_items[i].quantity;
                        // Set values similar to the product to be added
                        $scope.cart_items[i].quantity = product.quantity;
                        $scope.cart_items[i].total_price = $scope.cart_items[i].quantity * $scope.cart_items[i].price;

                        // Compare
                        if (JSON.stringify(angular.copy(product)) === JSON.stringify(angular.copy($scope.cart_items[i]))) {
                            duplicate = true;

                            $scope.cart_items[i].quantity = qty + product.quantity;
                            $scope.cart_items[i].total_price = $scope.cart_items[i].quantity * $scope.cart_items[i].price;
                            break;
                        } else {
  
                            $scope.cart_items[i].quantity = qty;
                            $scope.cart_items[i].total_price = $scope.cart_items[i].quantity * $scope.cart_items[i].price;
                        }
                    }
                    if (!duplicate) {

                        $scope.cart_items.push(angular.copy(product));
                    }
//                    create_product_alert(2, product.name + " added!", 3);
                    create_alert(2, product.name + " added!", 3);
                    var productIndex = $scope.products.map(function (item) {
                        return item.id;
                    }).indexOf(product.id);
                    var cartIndex = $scope.cart_items.map(function (item) {
                        return item.id;
                    }).indexOf(product.id);

                    $scope.products[productIndex].in_cart_qty = $scope.products[productIndex].in_cart_qty + product.quantity;
                    $scope.cart_items[cartIndex].in_cart_qty = $scope.cart_items[cartIndex].in_cart_qty + product.quantity;

//                    $("#customization-modal").foundation("reveal", "close");
                    $scope.customization_model_view(false);
                }
                else
                {
                    if (error_customization_selection !== null) {
                        create_alert(1, "select minimum " + error_customization_selection.selection_min + " & maximum " + error_customization_selection.selection_max + " from '" + error_customization_selection.name + "' category", 3);
                    }
                    else {
                        create_alert(1, "Kindly select correct items", 3);
                    }
                }
            };

            $scope.change_outlet = function () {
                localStorage.setItem("showalloutlet", true);
                $location.url("outlets");
            };

            $scope.change_active_tab = function (tab_id) {
                if ($scope.active_tab_id === tab_id) {
                    $scope.active_tab_id = 0;
                } else {
                    $scope.active_tab_id = tab_id;
                }
            };

            $scope.edit_quantity_from_product = function (delta, product) {
                if (product.current_stock > product.in_cart_qty || delta == -1 || !product.current_stock) {
                    for (var i = 0; i < $scope.cart_items.length; i++) {

                        if ($scope.cart_items[i].id === product.id) {

                            if (delta === -1 && $scope.cart_items[i].quantity > 1) {
                                $scope.cart_items[i].quantity--;
                                product.in_cart_qty--;
                            } else if (delta === 1) {

                                $scope.cart_items[i].quantity++;
                                product.in_cart_qty++;
                            }
                            $scope.cart_items[i].total_price = $scope.cart_items[i].quantity * $scope.cart_items[i].price;
                        }
                    }
                } else {
                    create_alert(2, "Only " + product.current_stock + " " + product.name + "  available in stock", 3);
                }


            };

            $scope.edit_quantity = function (delta, product) {

                if (product.current_stock > product.quantity || delta == -1 || !product.current_stock) {
                    if (delta === -1 && product.quantity > 1) {
                        product.quantity--;
                    } else if (delta === 1) {

                        product.quantity++;
                    }
                    product.total_price = product.quantity * product.price;

                    for (var i = 0; i < $scope.products.length; i++) {
                        if (product.id === $scope.products[i].id) {
                            $scope.products[i].in_cart_qty = product.quantity;

                        }
                    }
                } else {
                    create_alert(2, "Only " + product.current_stock + " " + product.name + "  available in stock", 3);
                }
            };

            $scope.remove_cart_item = function (index) {
                for (var i = 0; i < $scope.products.length; i++) {
                    if ($scope.cart_items[index].id === $scope.products[i].id) {
                        $scope.products[i].in_cart_qty = 0;

                    }
                }
                $scope.cart_items.splice(index, 1);

            };

            $scope.$watch("cart_items", function () {
                $scope.item_count = 0;
                $scope.cart_total = 0;
                //alert("m here==>"+JSON.stringify($scope.cart_items));
                angular.forEach($scope.cart_items, function (item) {

                    $scope.cart_total += item.total_price;
                    $scope.item_count += item.quantity;
                });
                localStorage.setItem("cart", JSON.stringify($scope.cart_items));
            }, true);

            $scope.$watch("selected_category", function () {
                setTimeout(function () {
                    $("#category-image").height($("#category-hero").height() + "px");
                }, 100);
            });

            $scope.$watch("cart_total", function () {
                $('#item-show').removeClass('item_count');
                setTimeout(function () {
                    $('#item-show').addClass('item_count');
                }, 10);
            });

            $scope.checkout = function () {

                $location.url("checkout");
            };

            if ($scope.soldout) {
                $scope.get_current_stock();
            } else {
                $scope.get_product();
            }
        }


    }
]);
app.filter('filterByProductNameAndProductAttributeName', function () {
    return function (products, searchText) {
        if (searchText === undefined || searchText === '' || searchText === null) {
            return products;
        }
        var searchText = searchText.toLowerCase();
        var result = [];
        for (i = 0; i < products.length; i++) {

            if (products[i].attributes.length >= 1) {
                if (products[i].name.toLowerCase().search(searchText) !== -1
                        || products[i].description.toLowerCase().search(searchText) !== -1
                        || (products[i].attributes[0].name.toLowerCase() === searchText)) {
                    result.push(products[i]);
                }
            }
            else {
                if (products[i].name.toLowerCase().search(searchText) !== -1
                        || products[i].description.toLowerCase().search(searchText) !== -1
                        ) {
                    result.push(products[i]);
                }
            }
        }
        return result;
    };
});
