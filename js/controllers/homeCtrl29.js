app.controller("homeCtrl", ["$http", "$scope", "$rootScope", "$location", function ($http, $scope, $rootScope, $location) {
        //configuration variables.

        $scope.city_outlet_selection = structure_config.city_outlet_selection;
        $scope.only_outlet_selection = structure_config.only_outlet_selection;
        $scope.city_area_selection = structure_config.city_area_selection;


        $scope.verified_facebook_link = "";
        $scope.verified_twitter_link = "";
        $scope.verified_android_link = "";
        $scope.verified_ios_link = "";

        $scope.enable_login = structure_config.enable_login;

        //To show outletlist to users.
        $scope.outlet_list = [];

        //To store all the outlets.
        $scope.outlets = [];

        //To store selected outlet.
        $scope.outlet_selected = [];

        //Only when option is given for city and area selection.

        $scope.outlet_fetched = false;

        $scope.Showoutletpopup = function () {

            $('#myModal').modal('show');
        };
        /*
         $scope.ShowAboutUspopup = function(){ //added
         $('#aboutUsModal').modal('show');
         };*/


        //used for outlet listing.
        $scope.api_outlets_by_company = function () {

            // Unset selected subarea in scope
            $scope.selected_subarea = {
                subarea_id: null,
                subarea_name: null,
                area_id: null,
                area_name: null,
                city_id: null,
                city_name: null
            };
            // Unset selected subarea in localStorage
            if (localStorage.getItem("subarea") !== null) {
                localStorage.removeItem("subarea");
            }
            // Update contents of the search box
            $scope.search_keyword = "";
            // Request object
            var request = {
                "company_id": company_id,
                "outlet_last_updated_timestamp": 0,
                "ot_last_updated_timestamp": 0,
                "oa_last_updated_timestamp": 0,
                "oam_last_updated_timestamp": 0,
                "oag_last_updated_timestamp": 0,
                "offer_last_updated_timestamp": 0,
                "oom_last_updated_timestamp": 0
            };
            $http.post(done_host + "/done-outlets-by-company", request, done_host_parameters).success(function (response) {
                //create_alert(2, "Welcome!", 3);
                if (development_mode) {
                    ////console.log(response);
                }
                $scope.show_splash_screen = false;

                $scope.outlets = response.data.outlets;
                $scope.outlet_timing = response.data.outlet_timing;
                $scope.set_outlet_datetime();
                if ($scope.only_outlet_selection) {
                    $scope.set_outlet_list();
                }
                $("#loader_popup").css("display", "none");

                ////console.log($scope.outlets);
            }).error(function () {
                create_alert(1, "It's taking longer than expected. Retry?", 3);
                $scope.error_text = "It's taking longer than expected. Retry?";
                $scope.show_splash_screen = true;
            });
        };

        $scope.api_get_city = function () {
            $http.get("Get_Solar_Webservices.php?webservice_name=cities_by_company&company_id=" + company_id).success(function (response) {
                $("#loader_popup").css("display", "none");
                if (development_mode) {
                    //console.log(response);
                }
                else {

                    if (response.response.docs.length > 1)
                    {
                        $scope.cities = response.response.docs;
                        //console.log($scope.cities);
                        $scope.get_location();
                    }
                    else
                    {
                        $('#citySelect').css("display", "none");
                        $scope.city_select = response.response.docs[0];
                        $scope.get_location();
                    }

                }
            });
        };


        $scope.api_get_company_banner = function () {
            $http.post(done_host + "/done-banners", {outlet_id: 8952, company_id: company_id, type: "company_banner_web"}).success(function (response) {
                $("#loader_popup").css("display", "none");
                if (development_mode) {
                    //console.log(response);
                }
                else {
                    $scope.banners = response.data;
//                    $scope.cities = response.response.docs;
                }
            });
        };

        $scope.get_location = function () {
            var city_id = 0;
            $("#loader_popup").css("display", "block");
//            alert("dfd");
//            consle.log($scope.city_select);
            if ($scope.city_select !== undefined || $scope.city_select !== "") {
                city_id = $scope.city_select.city_id;
                localStorage.setItem("city", JSON.stringify($scope.city_select));
//                    localStorage.setItem("state_id", JSON.stringify($scope.select_city.state_id));
                $http.get("Get_Solar_Webservices.php?webservice_name=company_subareas&company_id=" + company_id + '&city_id=' + city_id).success(function (response) {
                    if (development_mode) {
                        //console.log(response);
                    }
                    else {
                        // ... make subarea object of first result (assuming that result is sorted by increasing distance) and get outlets serving this subarea
                        $('#loactions').removeAttr('disabled');
                        $scope.areas = response.response.docs;
                        $scope.filterAreas(response.response.docs);
                    }
                    $("#loader_popup").css("display", "none");

                });
            } else {
                create_alert(1, "Kindly select a city", 3);
                $("#loader_popup").css("display", "none");
            }
        };

        $scope.filterAreas = function (items) {


            var hashCheck = {};
            $scope.areas = [];

            var extractValueToCompare = function (item) {
                return item['area_name'];

            };

            angular.forEach(items, function (item) {
                var valueToCheck, isDuplicate = false;

                for (var i = 0; i < $scope.areas.length; i++) {
                    ////console.log(newItems[i]);
                    if (angular.equals(extractValueToCompare($scope.areas[i]), extractValueToCompare(item))) {
                        isDuplicate = true;
                        break;
                    }
                }
                if (!isDuplicate) {
                    $scope.areas.push(item);
                }

            });
            items = $scope.areas;

            return items;
        };

        $scope.api_nearest_outlets_by_subarea = function () {
            // Request object

            $scope.outlets = [];
            if ($scope.area_select ? $scope.area_select.subarea_id : false) {
                var request = {
                    "company_id": company_id,
                    "subarea_id": parseInt($scope.area_select.subarea_id),
                    "service_type": "food"
                };
                // Call web-service
                $http.post(done_host + "/done-nearest-outlets-by-subarea", request, done_host_parameters).success(function (response) {
                    if (development_mode) {

                    }
                    // ... if no outlet(s) found
                    if (response.responseCode !== 0) {
                        $scope.outlets = [];
                        $scope.outlets_timings = [];
                        create_alert(1, "No outlets serving " + $scope.area_select.subarea_name + ".", 3);
                        $(".dvGo").attr("disabled", "disabled");
                    }
                    // ... if outlet(s) found
                    else {
                        $(".dvGo").removeAttr("disabled");
                        $scope.outlet_fetched = true;
                        if (response.data.outlets.length > 1) {

                            angular.forEach(response.data.outlets, function (outlet) {
                                $scope.outlet_select = response.data.outlets[0];
                                $scope.outlets.push(outlet);
                                $scope.set_outlet_list();

                            });
                        } else {
                            $scope.outlets = response.data.outlets;
                            $scope.outlet_select = response.data.outlets[0];
                        }
                        $scope.outlet_timing = response.data.outlet_timing;
                        $scope.set_outlet_datetime();





                    }
                }).error(function () {
                    create_alert(1, "It's taking longer than expected. Retry?", 3);
                });
            } else {
                create_alert(1, "Kindly select Location", 3);
            }
        };

        $scope.set_outlet_datetime = function () {
//            if ($scope.outlet_select != null) {

            var today = moment();
//                alert(today);
            var day_of_week = today.day() + 1;
//                 alert($scope.outlets.length);

            for (var i = 0; i < $scope.outlets.length; i++)
            {

//                    alert("I am going in outlets");
                angular.forEach($scope.outlet_timing, function (outlet_time)
                {
                    if ($scope.outlets[i].id === outlet_time.outlet_id && outlet_time.day_of_week === day_of_week)
                    {
                        //alert("first if");
                        if ($scope.outlets[i].outlet_timing === undefined)
                        {
                            $scope.outlets[i].outlet_timing = [];
                        }

                        $scope.outlets[i].outlet_timing.push(outlet_time);
                        var start_time = moment(outlet_time.open_time, "HH:mm:ss");
                        var duration = moment(outlet_time.duration, "HH:mm:ss");
                        var end_time = moment(start_time._d).add(duration._d, 'hours');
                        var is_open = false;
                        if (today >= start_time && today <= end_time)
                        {
                            is_open = true;
                        }

                        var format_start_time = moment(start_time).format("hh:mm A");
                        var format_end_time = moment(end_time).format("hh:mm A");
                        if ($scope.outlets[i].is_open !== undefined)
                        {
                            if ($scope.outlets[i].is_open !== true)
                            {
                                $scope.outlets[i].is_open = is_open;
                                $scope.outlets[i].start_time = format_start_time;
                                $scope.outlets[i].end_time = format_end_time;
                            }
                        }
                        else
                        {
                            $scope.outlets[i].is_open = is_open;
                            $scope.outlets[i].start_time = format_start_time;
                            $scope.outlets[i].end_time = format_end_time;
                        }

                        if ($scope.outlets[i].timing_string === undefined)
                        {
                            $scope.outlets[i].timing_string = format_start_time + " to " + format_end_time;
                        }
                        else
                        {
                            $scope.outlets[i].timing_string = $scope.outlets[i].timing_string + " or " + format_start_time + " to " + format_end_time;
                        }
                    }

                });
            }
//                $scope.view_main_menu();
//            } else {
//
//                create_alert(1, "Kindly select outlet", 3);
//            }

        };
        $scope.view_menu = function () {
            $(".loader").css("display", "flex");
//            alert("SDfsd");

            if ($scope.area_select === undefined)
            {
                create_alert(1, "Kindly select area to get proceed", 3);
                $(".loader").css("display", "none");
            }
            else
            {
                var outlet = $scope.outlet_select;//ng-model of outlet.
                //console.log($scope.area_select.subarea_name);
                if (outlet) {
                    if (outlet.outlet_open === 1) {
                        if (localStorage.getItem("outlet") === null) {

                            // Save outlet
                            localStorage.setItem("outlet", JSON.stringify(angular.copy(outlet)));
                            localStorage.setItem("city_name", $scope.outlet_select.city_name);
                            localStorage.setItem("subarea_name", JSON.stringify($scope.area_select));
                            $scope.city_name = localStorage.getItem("city_name");
                            localStorage.removeItem("cart");
                            $('#myModal').modal('hide');
                            $('body').removeClass('modal-open');
                            $('.modal-backdrop').remove();
                            // Load menu
                            $location.url("menu");
                        }
                        // If outlet exists
                        else {

                            if (JSON.parse(localStorage.getItem("outlet")).id !== outlet.id) {

                                // If cart is not empty
                                if (localStorage.getItem("cart") !== null && JSON.parse(localStorage.getItem("cart")).length !== 0) {
                                    // If user consents

                                    if (confirm("Your cart will be emptied on changing the outlet. Continue?")) {
                                        localStorage.setItem("outlet", JSON.stringify(angular.copy(outlet)));
                                        localStorage.setItem("city_name", $scope.outlet_select.city_name);
                                        localStorage.setItem("subarea_name", JSON.stringify($scope.area_select));
                                        $scope.city_name = localStorage.getItem("city_name");
                                        localStorage.removeItem("cart");
                                        $('#myModal').modal('hide');
                                        $('body').removeClass('modal-open');
                                        $('.modal-backdrop').remove();
                                        $location.url("menu");
                                    } else {

                                    }
                                } else {

                                    localStorage.setItem("outlet", JSON.stringify(angular.copy(outlet)));
                                    localStorage.setItem("city_name", $scope.outlet_select.city_name);
                                    $scope.city_name = localStorage.getItem("city_name");
                                    localStorage.setItem("subarea_name", JSON.stringify($scope.area_select));
                                    localStorage.removeItem("cart");
                                    $('#myModal').modal('hide');
                                    $('body').removeClass('modal-open');
                                    $('.modal-backdrop').remove();
                                    $location.url("menu");
                                }
                            } else {

                                localStorage.setItem("outlet", JSON.stringify(angular.copy(outlet)));
                                localStorage.setItem("city_name", $scope.outlet_select.city_name);
                                localStorage.setItem("subarea_name", JSON.stringify($scope.area_select));
                                $scope.city_name = localStorage.getItem("city_name");
                                localStorage.removeItem("cart");
                                $('#myModal').modal('hide');
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $location.url("menu");
                            }
                        }
                    } else {
                        $scope.message = outlet.outlet_timing[0].outlet_message;
                        $("#messagepop").modal("show");
                    }
                } else {
                    create_alert(1, "Kindly select Outlet to get proceed", 3);
                    $(".loader").css("display", "none");
                }
            }
            fbq('track', 'ViewContent');
        };

        $scope.view_main_menu = function () {
            $route.reload();
            try {


                if ($scope.outlet_select !== undefined) {

                    if (localStorage.getItem("outlet") !== null && JSON.parse(localStorage.getItem("outlet")).id !== $scope.outlet_select.id) {
// If cart is not empty

                        if (localStorage.getItem("cart") !== null && JSON.parse(localStorage.getItem("cart")).length !== 0) {
// If user consents
                            if (confirm("Your cart will be emptied on changing the outlet. Continue?")) {
                                localStorage.setItem("outlet", JSON.stringify(angular.copy($scope.outlet_select)));

                                localStorage.setItem("city_name", $scope.city_select.city_name);
                                $scope.city_name = localStorage.getItem("city_name");
                                localStorage.removeItem("cart");
                                $rootScope.root_item_count = 0;
                                $('#myModal').modal('hide');
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $location.url("menu");
                            } else {
// Do nothing
                            }
                        } else {

                            localStorage.setItem("outlet", JSON.stringify(angular.copy($scope.outlet_select)));
                            localStorage.setItem("city_name", $scope.city_select.city_name);
                            $scope.city_name = localStorage.getItem("city_name");
                            $('#myModal').modal('hide');
                            $('body').removeClass('modal-open');
//                        $rootScope.root_item_count = 0;
                            $('.modal-backdrop').remove();
                            $location.url("menu");
                        }
                    } else {
                        $('#myModal').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
//                    $rootScope.root_item_count = 0;
                        localStorage.setItem("outlet", JSON.stringify($scope.outlet_select));
                        localStorage.setItem("city_name", $scope.city_select.city_name);
                        $scope.city_name = localStorage.getItem("city_name");
                        $location.url("menu");
                    }
                } else {
//                    alert("Kindly select outlet");
                }
            }
            catch (e)
            {
                $scope.error_text = 'Your web browser does not support storing settings locally. In Safari, the most common cause of this is using "Private Browsing Mode". Some settings may not save or some features may not work properly for you.';
                alert(e);
//                $scope.show_splash_screen = true;
            }

        };


        $scope.set_outlet_list = function () {
            $scope.outlet_list = [];
            if ($scope.city_outlet_selection) {
                for (var i = 0; i < $scope.outlets.length; i++) {
                    if (parseInt($scope.city_select.city_id) === parseInt($scope.outlets[i].city_id)) {
                        $scope.outlet_list.push($scope.outlets[i]);
                    }
                }
            } else if ($scope.only_outlet_selection) {
                $scope.outlet_list = $scope.outlets;

            } else if ($scope.city_area_selection && $scope.outlet_fetched == false) {
                $scope.get_location();
            } else if ($scope.city_area_selection && $scope.outlet_fetched) {
                for (var i = 0; i < $scope.outlets.length; i++) {
                    $scope.outlet_list.push($scope.outlets[i]);
                }
            }
        }


        $scope.api_outlets_by_zipcode = function () {

            // Unset selected subarea in scope
            $scope.selected_subarea = {
                subarea_id: null,
                subarea_name: null,
                area_id: null,
                area_name: null,
                city_id: null,
                city_name: null
            };
            // Unset selected subarea in localStorage
            if (localStorage.getItem("subarea") !== null) {
                localStorage.removeItem("subarea");
            }
            // Update contents of the search box
            $scope.search_keyword = "";
            // Request object
            var request = {
                "company_id": company_id,
                "zipcode": $scope.zipcode,
                "service_type": "Food",
            };
            $http.post(done_host + "/done-outlet-by-zip_code", request, done_host_parameters).success(function (response) {
                //create_alert(2, "Welcome!", 3);
                if (development_mode) {
                    ////console.log(response);
                }

                if (response.responseCode !== 0) {
                    $scope.outlets = [];
                    $scope.outlets_timings = [];
                    create_alert(1, "No outlets serving " + $scope.zipcode + ".", 3);
                }
                // ... if outlet(s) found
                else {
                    $scope.outlet_fetched = true;
                    if (response.data.outlets.length > 1) {

                        angular.forEach(response.data.outlets, function (outlet) {
                            $scope.outlets.push(outlet);
                            $scope.set_outlet_list();


                        });
                    } else {
                        $scope.outlets = response.data.outlets;
                        $scope.outlet_select = response.data.outlets[0];
                    }
                    $scope.outlet_timing = response.data.outlet_timing;
                    $scope.set_outlet_datetime();
                    $scope.view_menu();


                    ////console.log("Timings are: "+JSON.stringify($scope.outlets_timings));

                }

                $("#loader_popup").css("display", "none");

                ////console.log($scope.outlets);
            }).error(function () {
                create_alert(1, "It's taking longer than expected. Retry?", 3);
                $scope.error_text = "It's taking longer than expected. Retry?";
                $scope.show_splash_screen = true;
            });
        };



        if ($scope.city_outlet_selection) {
            $scope.api_get_city();
            $scope.api_outlets_by_company();
        } else if ($scope.only_outlet_selection) {
            $scope.api_outlets_by_company();
            //set an object to show outlets.
            $scope.set_outlet_list();
        } else if ($scope.city_area_selection) {
            $scope.api_get_city();
        }

        $scope.page_active = "/";
        $rootScope.$on("$routeChangeSuccess", function () {
            $scope.page_active = $location.path();
        });

        $scope.verify_social_link = function ()
        {
            try {
                if (structure_config.social_config.facebook_link !== undefined)
                {
                    $scope.verified_facebook_link = structure_config.social_config.facebook_link;
                }
            }
            catch (e) {
            }
            try {
                if (structure_config.social_config.twitter_link !== undefined)
                {
                    $scope.verified_twitter_link = structure_config.social_config.twitter_link;
                }
            }
            catch (e) {
            }
            try {
                if (structure_config.social_config.android_link !== undefined)
                {
                    $scope.verified_android_link = structure_config.social_config.android_link;
                }
            }
            catch (e) {
            }

            try {
                if (structure_config.social_config.ios_link !== undefined)
                {
                    $scope.verified_ios_link = structure_config.social_config.ios_link;
                }
            }
            catch (e) {
            }


        };
        $scope.verify_social_link();


        /* feed back form*/
        $scope.send_feedback = function () {
            var emailReg = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
            if (!$scope.fbname) {
                create_alert(2, "Kindly Enter your name", 3);
            } else if (!$scope.fbmobile) {
                create_alert(2, "Kindly Enter your mobile number", 3);
            } else if ($scope.fbmobile.length !== 10) {
                create_alert(2, "Kindly Enter proper mobile number", 3);
            } else if (!new RegExp(/[7-9]{1}\d{9}/).test($scope.fbmobile)) {
                create_alert(2, "Kindly Enter proper mobile number", 3);
            } else if (!emailReg.test($scope.fbemail)) {
                create_alert(2, "Kindly Enter proper EmailID", 3);
            } else if (!$scope.fboutlet) {
                create_alert(2, "Kindly select outlet", 3);
            } else if (!$scope.fbmessage) {
                create_alert(2, "Kindly enter your Message", 3);
            } else {
                $scope.api_feedback();
            }

        };

        $scope.api_feedback = function () {
            var request = {
                name: $scope.fbname,
                phone_number: $scope.fbmobile,
                feedback: $scope.fbmessage,
                email: $scope.fbemail,
                outlet_id: $scope.fboutlet.id
            };
            create_alert(1, "Instructing the pigeon...", false);
            $http.post(done_host + "/done-feedback", request, done_host_parameters).success(function (response) {
                if (development_mode) {
                    //console.log(response);
                }
                if (response.responseCode === 0) {
                    $scope.fbname = "";
                    $scope.fbmobile = "";
                    $scope.fbmessage = "";
                    $scope.fbemail = "";
                    $scope.fboutlet = "";
                    create_alert(2, "Thank you for your valueable feedback!", 3);
                } else {
                    create_alert(1, "Uh oh. We ran into some problem. Please try again later.", 3);
                }
            }).error(function () {
                create_alert(1, "It's taking longer than expected. Retry?", 3);
            });
        };


        $scope.api_outlets_for_feedback = function () {
            // Unset selected subarea in scope
            $scope.selected_subarea = {
                subarea_id: null,
                subarea_name: null,
                area_id: null,
                area_name: null,
                city_id: null,
                city_name: null
            };
            // Unset selected subarea in localStorage
            if (localStorage.getItem("subarea") !== null) {
                localStorage.removeItem("subarea");
            }
            // Update contents of the search box
            $scope.search_keyword = "";
            // Request object
            var request = {
                "company_id": company_id,
                "outlet_last_updated_timestamp": 0,
                "ot_last_updated_timestamp": 0,
                "oa_last_updated_timestamp": 0,
                "oam_last_updated_timestamp": 0,
                "oag_last_updated_timestamp": 0,
                "offer_last_updated_timestamp": 0,
                "oom_last_updated_timestamp": 0
            };
            $http.post(done_host + "/done-outlets-by-company", request, done_host_parameters).success(function (response) {
                //create_alert(2, "Welcome!", 3);
                if (development_mode) {

                }
                $scope.show_splash_screen = false;

                $scope.feedbackoutlets = response.data.outlets;
//                $scope.outlet_timing = response.data.outlet_timing;
//                $scope.set_outlet_datetime();
//                if ($scope.only_outlet_selection) {
//                    $scope.set_outlet_list();
//                }
                $("#loader_popup").css("display", "none");


            }).error(function () {
                create_alert(1, "It's taking longer than expected. Retry?", 3);
                $scope.error_text = "It's taking longer than expected. Retry?";
                $scope.show_splash_screen = true;
            });
        };

        try {
            localStorage.setItem("test", "1");
            localStorage.removeItem("test");

        }
        catch (e)
        {
            alert('Your web browser does not support storing settings locally. In Safari, the most common cause of this is using "Private Browsing Mode". Some settings may not save or some features may not work properly for you.');
        }
        /* Re-initialize Foundation */
        $scope.api_get_company_banner();
        $scope.api_outlets_for_feedback();

    }
]);