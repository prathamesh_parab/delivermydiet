/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    // Toggle Nav on Click
    $('.toggle-nav').click(function () {
        // Calling a function in case you want to expand upon this.
        toggleNav();
    });


    function toggleNav() {
        if ($('.dvWrapper').hasClass('show-nav')) {
            // Do things on Nav Close
            $('.dvWrapper').removeClass('show-nav');
        } else {
            // Do things on Nav Open
            $('.dvWrapper').addClass('show-nav');
        }

        //$('#site-wrapper').toggleClass('show-nav');
    }
    ;

    // jQuery DatePicker



});



// Global stuff
var animation_delay = structure_config.animation_delay;
var demongoon_host = structure_config.demongoon_host;
var done_host = structure_config.done_host;
var development_mode = structure_config.development_mode;
var company_id = structure_config.company_id;

var local_test_mode = structure_config.local_test_mode;
var german_server_test_mode = false;
var german_server_prods_mode = false;

if (local_test_mode === true) {
//local server
    done_host = "http://10.0.1.197:5000";
    demongoon_host = "http://10.0.1.197:5001";
}
if (german_server_test_mode === true)
{
    done_host = "http://apiger.done.to";
    demongoon_host = "http://apiger.done.to";
}
if (german_server_prods_mode === true)
{
    done_host = "http://prods.done.to";
    demongoon_host = "http://prods.done.to";
}

var done_host_parameters = {
    timeout: 5000
};

function assign_css()
{
    less.refresh();
    $(".company-title").html(structure_config.company_name);
    less.modifyVars({
        "@bg_color": structure_config.css_config.bg_color,
        "@header_bg_color": structure_config.css_config.header_bg_color,
        "@header_bottom_color": structure_config.css_config.header_bottom_color,
        "@header_item_bg_color": structure_config.css_config.header_item_bg_color,
        "@header_item_bg_hover_color": structure_config.css_config.header_item_bg_hover_color,
        "@header_item_text_color": structure_config.css_config.header_item_text_color,
        "@header_item_text_hover_color": structure_config.css_config.header_item_text_hover_color,
        "@button_bg_color": structure_config.css_config.button_bg_color,
        "@button_bg_hover_color": structure_config.css_config.button_bg_hover_color,
        "@button_text_color": structure_config.css_config.button_text_color,
        "@button_text_hover_color": structure_config.css_config.button_text_hover_color,
        "@category_bg_color": structure_config.css_config.category_bg_color,
        "@category_bg_hover_color": structure_config.css_config.category_bg_hover_color,
        "@category_text_color": structure_config.css_config.category_text_color,
        "@category_text_hover_color": structure_config.css_config.category_text_hover_color,
        "@product_bg_color": structure_config.css_config.product_bg_color,
        "@product_bg_hover_border_color": structure_config.css_config.product_bg_hover_border_color,
        "@product_text_color": structure_config.css_config.product_text_color,
        "@product_description_color": structure_config.css_config.product_description_color,
        "@outlet_bg_color": structure_config.css_config.outlet_bg_color,
        "@outlet_text_color": structure_config.css_config.outlet_text_color,
        "@mobile_header_bg_color": structure_config.css_config.mobile_header_bg_color,
        "@mobile_header_text_color": structure_config.css_config.mobile_header_text_color,
        "@mobile_footer_bg_color": structure_config.css_config.mobile_footer_bg_color,
        "@mobile_footer_text_color": structure_config.css_config.mobile_footer_text_color,
        "@mobile_button_bg_color": structure_config.css_config.mobile_button_bg_color,
        "@mobile_button_text_color": structure_config.css_config.mobile_button_text_color,
        "@intro_text_color": structure_config.css_config.intro_text_color,
    });

}

assign_css();


function setCategoryHeight()
{
    $("#category-container").height($("#test").height());
}



function check_date_time(outlet_timing)
{
    var is_open = false;
    if (outlet_timing !== undefined)
    {
        $.ajax({
            url: "ws/validatestoretime.php",
            method: "POST",
            data: {
                outlet_timing: outlet_timing
            },
            success: function (result) {
                if (result == 1) {
//                    alert(result);
                    return false;
                } else {
                    return false;
                }
//                //console.log(result);
//                location.reload();
            }});


    }
}

function open_signupform() {
    $("#dvLoginPop").modal("hide");
    $("#dvSignUpPop").modal("show");
}
function show_social_link(social_link) {
    switch (social_link)
    {
        case "facebook_link":
            window.open(structure_config.social_config.facebook_link, '_blank');
            break;
        case "twitter_link":
            window.open(structure_config.social_config.twitter_link, '_blank');
            break;
        case "android_link":
            window.open(structure_config.social_config.android_link, '_blank');
            break;
        case "ios_link":
            window.open(structure_config.social_config.ios_link, '_blank');
            break;
    }
}

function create_alert(severity, message, timeout) {
    $("div.alert-box.active").remove();
    timeout *= 1000;
    var alert_div;
    switch (severity) {
        case 1:
            alert_div = $("div.alert-box.alert").clone();
            break;
        case 2:
            alert_div = $("div.alert-box.success").clone();
            break;
    }
    $(alert_div).addClass("active");
    $(alert_div).find(".alert-message").text(message);
    $("#alert-container").append(alert_div);
    $(alert_div).slideDown(animation_delay);
    if (timeout) {
        setTimeout(function () {
            $(alert_div).slideUp(animation_delay, function () {
                $(alert_div).remove();
            });
        }, timeout);
    }
}

function create_product_alert(severity, message, timeout) {
    $("div.alert-box.active").remove();
    timeout *= 1000;
    var alert_div;
    alert_div = $(".dvProductName");

    $(alert_div).animate({
        'display': 'block',
    }, 500);
    $(alert_div).find(".dvName").text(message);
//    $("#alert-container").append(alert_div);
    $(alert_div).slideDown(animation_delay);
    if (timeout) {
        setTimeout(function () {
            $(alert_div).slideUp(animation_delay, function () {
                $(alert_div).css("display", "none");
            });
        }, timeout);
    }
}
// Module
var app = angular.module('done', ['ngAnimate', 'ngRoute', 'ngTouch']);


// Routes
app.config(["$httpProvider", "$routeProvider", function ($httpProvider, $routeProvider) {
        $routeProvider
                // Open the Menu page by default. If no outlet is found in localStorage (i.e. first-time user) user gets redirected to the Outlets page
                .when("/", {
                    redirectTo: "/home"
                })
                .when("/home", {
                    templateUrl: "templates/home.html",
                    controller: "homeCtrl"
                })
                .when("/outlets", {
                    templateUrl: "templates/outlets.html",
                    controller: "outletsCtrl"
                })
                .when("/menu", {
                    templateUrl: "templates/menu.html",
                    controller: "menuCtrl"
                })
                .when("/checkout", {
                    templateUrl: "templates/checkout.html",
                    controller: "checkoutCtrl"
                })
                .when("/orderhistory", {
                    templateUrl: "templates/orderhistory.html",
                    controller: "orderhistoryCtrl"
                })
                .when("/aboutus", {
                    templateUrl: "templates/aboutus.html",
                    controller: "aboutusCtrl"
                })
                .when("/contactus", {
                    templateUrl: "templates/contactus.html",
                    controller: "contactusCtrl"
                })

                .when("/login", {
                    templateUrl: "templates/login.html",
                    controller: "loginCtrl"
                })
                
                .when("/privacypolicy", {
                    templateUrl: "templates/privacypolicy.html",
                    controller: "privacyCtrl"
                })
                .when("/termsandconditions", {
                    templateUrl: "templates/termsandconditions.html",
                    controller: "termsandconditions"
                })

                .when("/disclaimers", {
                    templateUrl: "templates/disclaimers.html",
                    controller: "disclaimersCtrl"
                })

                // Any other URL redirects to the Menu page
                .otherwise({
                    redirectTo: "/home"
                });
    }]);
// Controllers
app.controller("mainCtrl", ["$location", "$rootScope", "$scope", "$http", "$route", function ($location, $rootScope, $scope, $http, $route) {


        //configuration variables.

        $scope.city_outlet_selection = structure_config.city_outlet_selection;
        $scope.only_outlet_selection = structure_config.only_outlet_selection;
        $scope.city_area_selection = structure_config.city_area_selection;


        $scope.verified_facebook_link = "";
        $scope.verified_twitter_link = "";
        $scope.verified_android_link = "";
        $scope.verified_ios_link = "";

        $scope.enable_login = structure_config.enable_login;
        $scope.pages = [
            {
                label: "Outlets",
                faclass: "fa-home",
                url_on: "img/home-on.png",
                url_off: "img/home-off.png",
                route: "/outlets"
            },
            {
                label: "Menu",
                faclass: "fa-bars",
                url_on: "img/menu-on.png",
                url_off: "img/menu-off.png",
                route: "/menu"
            },
            {
                label: "Checkout",
                faclass: "fa-shopping-cart",
                url_on: "img/checkout-on.png",
                url_off: "img/checkout-off.png",
                route: "/checkout"
            },
            {
                label: "Order History",
                faclass: "fa-history",
                url_on: "img/order-history-on.png",
                url_off: "img/order-history-off.png",
                route: "/orderhistory"
            },
            {
                label: "About Us",
                faclass: "fa-star",
                url_on: "img/about-on.png",
                url_off: "img/about-off.png",
                route: "/aboutus"
            },
            {
                label: "Contact Us",
                faclass: "fa-phone",
                url_on: "img/contact-on.png",
                url_off: "img/contact-off.png",
                route: "/contactus"
            }
        ];

        //To show outletlist to users.
        $scope.outlet_list = [];

        //To store all the outlets.
        $scope.outlets = [];

        //To store selected outlet.
        $scope.outlet_selected = [];

        //Only when option is given for city and area selection.

        $scope.outlet_fetched = false;

        $scope.Showoutletpopup = function () {

            $('#myModal').modal('show');
        };
        /*
         $scope.ShowAboutUspopup = function(){ //added
         $('#aboutUsModal').modal('show');
         };*/

        //used for outlet listing.
        if (localStorage.getItem("user") !== null) {
            $scope.user = JSON.parse(localStorage.getItem("user"));
        }
         $rootScope.$on("$routeChangeSuccess", function ()
        {
            
            $scope.page_active = $location.path();

            if ($scope.page_active === "/home")
            {
                $("head").append('<script src="js/dist/semantic.min.js" data-id="semantic"></script>');
                $("head").append('<script src="js/dist/components/transition.js" data-id="transition"></script>');

            }
        });
        
        $scope.api_outlets_by_company = function () {

            // Unset selected subarea in scope
            $scope.selected_subarea = {
                subarea_id: null,
                subarea_name: null,
                area_id: null,
                area_name: null,
                city_id: null,
                city_name: null
            };
            // Unset selected subarea in localStorage
            if (localStorage.getItem("subarea") !== null) {
                localStorage.removeItem("subarea");
            }
            // Update contents of the search box
            $scope.search_keyword = "";
            // Request object
            var request = {
                "company_id": company_id,
                "outlet_last_updated_timestamp": 0,
                "ot_last_updated_timestamp": 0,
                "oa_last_updated_timestamp": 0,
                "oam_last_updated_timestamp": 0,
                "oag_last_updated_timestamp": 0,
                "offer_last_updated_timestamp": 0,
                "oom_last_updated_timestamp": 0
            };
            $http.post(done_host + "/done-outlets-by-company", request, done_host_parameters).success(function (response) {
                //create_alert(2, "Welcome!", 3);
                if (development_mode) {
                    ////console.log(response);
                }
                $scope.show_splash_screen = false;

                $scope.outlets = response.data.outlets;
                $scope.outlet_timing = response.data.outlet_timing;
                $scope.set_outlet_datetime();
                if ($scope.only_outlet_selection) {
                    $scope.set_outlet_list();
                }
                $("#loader_popup").css("display", "none");

                ////console.log($scope.outlets);
            }).error(function () {
                create_alert(1, "It's taking longer than expected. Retry?", 3);
                $scope.error_text = "It's taking longer than expected. Retry?";
                $scope.show_splash_screen = true;
            });
        };

        $scope.api_get_city = function () {
            $http.get("Get_Solar_Webservices.php?webservice_name=cities_by_company&company_id=" + company_id).success(function (response) {
                $("#loader_popup").css("display", "none");
                if (development_mode) {
                    //console.log(response);
                }
                else {
                    $scope.cities = response.response.docs;
                }
            });
        };


        $scope.api_get_company_banner = function () {
            $http.post(done_host + "/done-banners", {company_id: company_id, type: "company_banner_web"}).success(function (response) {
                $("#loader_popup").css("display", "none");
                if (development_mode) {
                    //console.log(response);
                }
                else {
                    $scope.banners = response.data;
//                    $scope.cities = response.response.docs;
                    //console.log(response.responseCode);
                    if (response.responseCode === 0) {
                        setTimeout(function () {
                            openBanner();
                        }, 200);
                    }
                }
            });
        };

        $scope.get_location = function () {
            var city_id = 0;
            $("#loader_popup").css("display", "block");
//            alert("dfd");
            if ($scope.city_select !== undefined || $scope.city_select !== "") {
                city_id = $scope.city_select.city_id;
                localStorage.setItem("city", JSON.stringify($scope.city_select));
//                    localStorage.setItem("state_id", JSON.stringify($scope.select_city.state_id));
                $http.get("Get_Solar_Webservices.php?webservice_name=company_subareas&company_id=" + company_id + '&city_id=' + city_id).success(function (response) {
                    if (development_mode) {
                        //console.log(response);
                    }
                    else {
                        // ... make subarea object of first result (assuming that result is sorted by increasing distance) and get outlets serving this subarea
                        $('#loactions').removeAttr('disabled');
                        $scope.areas = response.response.docs;
                        $scope.filterAreas(response.response.docs);
                    }
                    $("#loader_popup").css("display", "none");

                });
            } else {
                create_alert(1, "Kindly select a city", 3);
                $("#loader_popup").css("display", "none");
            }
        };

        $scope.filterAreas = function (items) {


            var hashCheck = {};
            $scope.areas = [];

            var extractValueToCompare = function (item) {
                return item['area_name'];

            };

            angular.forEach(items, function (item) {
                var valueToCheck, isDuplicate = false;

                for (var i = 0; i < $scope.areas.length; i++) {
                    ////console.log(newItems[i]);
                    if (angular.equals(extractValueToCompare($scope.areas[i]), extractValueToCompare(item))) {
                        isDuplicate = true;
                        break;
                    }
                }
                if (!isDuplicate) {
                    $scope.areas.push(item);
                }

            });
            items = $scope.areas;

            return items;
        };

        $scope.api_nearest_outlets_by_subarea = function () {
            // Request object
//            //console.log($scope.select_location);
            $scope.outlets = [];
            if ($scope.area_select !== undefined) {
                var request = {
                    "company_id": company_id,
                    "subarea_id": $scope.area_select.subarea_id,
                    "service_type": "food"
                };
                // Call web-service
                $http.post(done_host + "/done-nearest-outlets-by-subarea", request, done_host_parameters).success(function (response) {
                    if (development_mode) {
                        ////console.log(response);
                    }
                    // ... if no outlet(s) found
                    if (response.responseCode !== 0) {
                        $scope.outlets = [];
                        $scope.outlets_timings = [];
                        create_alert(1, "No outlets serving " + $scope.selected_subarea.subarea_name + ".", 3);
                    }
                    // ... if outlet(s) found
                    else {
                        $scope.outlet_fetched = true;
                        if (response.data.outlets.length > 1) {

                            angular.forEach(response.data.outlets, function (outlet) {
                                $scope.outlets.push(outlet);
                                $scope.set_outlet_list();

                            });
                        } else {
                            $scope.outlets = response.data.outlets;
                            $scope.outlet_select = response.data.outlets[0];
                        }
                        $scope.outlet_timing = response.data.outlet_timing;
                        $scope.set_outlet_datetime();



                        ////console.log("Timings are: "+JSON.stringify($scope.outlets_timings));

                    }
                }).error(function () {
                    create_alert(1, "It's taking longer than expected. Retry?", 3);
                });
            } else {
                create_alert(1, "Kindly select Location", 3);
            }
        };

        $scope.set_outlet_datetime = function () {
//            if ($scope.outlet_select != null) {

            var today = moment();
//                alert(today);
            var day_of_week = today.day() + 1;
//                 alert($scope.outlets.length);

            for (var i = 0; i < $scope.outlets.length; i++)
            {

//                    alert("I am going in outlets");
                angular.forEach($scope.outlet_timing, function (outlet_time)
                {
                    if ($scope.outlets[i].id === outlet_time.outlet_id && outlet_time.day_of_week === day_of_week)
                    {
                        //alert("first if");
                        if ($scope.outlets[i].outlet_timing === undefined)
                        {
                            $scope.outlets[i].outlet_timing = [];
                        }

                        $scope.outlets[i].outlet_timing.push(outlet_time);
                        var start_time = moment(outlet_time.open_time, "HH:mm:ss");
                        var duration = moment(outlet_time.duration, "HH:mm:ss");
                        var end_time = moment(start_time._d).add(duration._d, 'hours');
                        var is_open = false;
                        if (today >= start_time && today <= end_time)
                        {
                            is_open = true;
                        }

                        var format_start_time = moment(start_time).format("hh:mm A");
                        var format_end_time = moment(end_time).format("hh:mm A");
                        if ($scope.outlets[i].is_open !== undefined)
                        {
                            if ($scope.outlets[i].is_open !== true)
                            {
                                $scope.outlets[i].is_open = is_open;
                                $scope.outlets[i].start_time = format_start_time;
                                $scope.outlets[i].end_time = format_end_time;
                            }
                        }
                        else
                        {
                            $scope.outlets[i].is_open = is_open;
                            $scope.outlets[i].start_time = format_start_time;
                            $scope.outlets[i].end_time = format_end_time;
                        }

                        if ($scope.outlets[i].timing_string === undefined)
                        {
                            $scope.outlets[i].timing_string = format_start_time + " to " + format_end_time;
                        }
                        else
                        {
                            $scope.outlets[i].timing_string = $scope.outlets[i].timing_string + " or " + format_start_time + " to " + format_end_time;
                        }
                    }

                });
            }
//                $scope.view_main_menu();
//            } else {
//
//                create_alert(1, "Kindly select outlet", 3);
//            }

        };
        $scope.view_menu = function () {

//            alert("SDfsd");
            var outlet = $scope.outlet_select;//ng-model of outlet.

            //console.log($scope.outlet_select);

            if (localStorage.getItem("outlet") === null) {
//                //console.log("1");
                // Save outlet
                localStorage.setItem("outlet", JSON.stringify(angular.copy(outlet)));
                localStorage.setItem("city_name", $scope.outlet_select.city_name);
                $scope.city_name = localStorage.getItem("city_name");
                localStorage.removeItem("cart");
                $('#myModal').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                // Load menu
                $location.url("menu");
            }
            // If outlet exists
            else {
                // If user selects a different outlet
//                //console.log("2");
                if (JSON.parse(localStorage.getItem("outlet")).id !== outlet.id) {

                    // If cart is not empty
                    if (localStorage.getItem("cart") !== null && JSON.parse(localStorage.getItem("cart")).length !== 0) {
                        // If user consents

                        if (confirm("Your cart will be emptied on changing the outlet. Continue?")) {
                            localStorage.setItem("outlet", JSON.stringify(angular.copy(outlet)));
                            localStorage.setItem("city_name", $scope.outlet_select.city_name);
                            $scope.city_name = localStorage.getItem("city_name");
                            localStorage.removeItem("cart");
                            $('#myModal').modal('hide');
                            $('body').removeClass('modal-open');
                            $('.modal-backdrop').remove();
                            $location.url("menu");
                        } else {
//                            //console.log("3");
                            // Do nothing
                        }
                    } else {
//                        //console.log("4");
                        localStorage.setItem("outlet", JSON.stringify(angular.copy(outlet)));
                        localStorage.setItem("city_name", $scope.outlet_select.city_name);
                        $scope.city_name = localStorage.getItem("city_name");
                        localStorage.removeItem("cart");
                        $('#myModal').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                        $location.url("menu");
                    }
                } else {
//                    //console.log("5");
                    localStorage.setItem("outlet", JSON.stringify(angular.copy(outlet)));
                    localStorage.setItem("city_name", $scope.outlet_select.city_name);
                    $scope.city_name = localStorage.getItem("city_name");
                    localStorage.removeItem("cart");
                    $('#myModal').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $location.url("menu");
                }
            }

        };

        $scope.view_main_menu = function () {
            $route.reload();
            try {


                if ($scope.outlet_select !== undefined) {

                    if (localStorage.getItem("outlet") !== null && JSON.parse(localStorage.getItem("outlet")).id !== $scope.outlet_select.id) {
// If cart is not empty

                        if (localStorage.getItem("cart") !== null && JSON.parse(localStorage.getItem("cart")).length !== 0) {
// If user consents
                            if (confirm("Your cart will be emptied on changing the outlet. Continue?")) {
                                localStorage.setItem("outlet", JSON.stringify(angular.copy($scope.outlet_select)));

                                localStorage.setItem("city_name", $scope.city_select.city_name);
                                $scope.city_name = localStorage.getItem("city_name");
                                localStorage.removeItem("cart");
                                $rootScope.root_item_count = 0;
                                $('#myModal').modal('hide');
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $location.url("menu");
                            } else {
// Do nothing
                            }
                        } else {

                            localStorage.setItem("outlet", JSON.stringify(angular.copy($scope.outlet_select)));
                            localStorage.setItem("city_name", $scope.city_select.city_name);
                            $scope.city_name = localStorage.getItem("city_name");
                            $('#myModal').modal('hide');
                            $('body').removeClass('modal-open');
//                        $rootScope.root_item_count = 0;
                            $('.modal-backdrop').remove();
                            $location.url("menu");
                        }
                    } else {
                        $('#myModal').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
//                    $rootScope.root_item_count = 0;
                        localStorage.setItem("outlet", JSON.stringify($scope.outlet_select));
                        localStorage.setItem("city_name", $scope.city_select.city_name);
                        $scope.city_name = localStorage.getItem("city_name");
                        $location.url("menu");
                    }
                } else {
//                    alert("Kindly select outlet");
                }
            }
            catch (e)
            {
                $scope.error_text = 'Your web browser does not support storing settings locally. In Safari, the most common cause of this is using "Private Browsing Mode". Some settings may not save or some features may not work properly for you.';
                alert(e);
//                $scope.show_splash_screen = true;
            }

        };
        $scope.set_outlet_list = function () {
            $scope.outlet_list = [];
            if ($scope.city_outlet_selection) {
                for (var i = 0; i < $scope.outlets.length; i++) {
                    if (parseInt($scope.city_select.city_id) === parseInt($scope.outlets[i].city_id)) {
                        $scope.outlet_list.push($scope.outlets[i]);
                    }
                }
            } else if ($scope.only_outlet_selection) {
                $scope.outlet_list = $scope.outlets;
            } else if ($scope.city_area_selection && $scope.outlet_fetched == false) {
                $scope.get_location();
            } else if ($scope.city_area_selection && $scope.outlet_fetched) {
                for (var i = 0; i < $scope.outlets.length; i++) {
                    $scope.outlet_list.push($scope.outlets[i]);
                }
            }
        }




        if ($scope.city_outlet_selection) {
            $scope.api_get_city();
            $scope.api_outlets_by_company();
        } else if ($scope.only_outlet_selection) {
            $scope.api_outlets_by_company();
            //set an object to show outlets.
            $scope.set_outlet_list();
        } else if ($scope.city_area_selection) {
            $scope.api_get_city();
        }

        $scope.page_active = "/";
        $rootScope.$on("$routeChangeSuccess", function () {
            $scope.page_active = $location.path();
        });

        $scope.verify_social_link = function ()
        {
            try {
                if (structure_config.social_config.facebook_link !== undefined)
                {
                    $scope.verified_facebook_link = structure_config.social_config.facebook_link;
                }
            }
            catch (e) {
            }
            try {
                if (structure_config.social_config.twitter_link !== undefined)
                {
                    $scope.verified_twitter_link = structure_config.social_config.twitter_link;
                }
            }
            catch (e) {
            }
            try {
                if (structure_config.social_config.android_link !== undefined)
                {
                    $scope.verified_android_link = structure_config.social_config.android_link;
                }
            }
            catch (e) {
            }

            try {
                if (structure_config.social_config.ios_link !== undefined)
                {
                    $scope.verified_ios_link = structure_config.social_config.ios_link;
                }
            }
            catch (e) {
            }


        };

        //Login functions 


        var emailReg = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        $scope.showty = false;
        $scope.validatelogin = function () {
            $("#lgemailerror").css("display", "none");
            $("#lgpassworderror").css("display", "none");
//            var emailReg = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
            if (!$scope.primary_email) {
//                alert("Kindly Enter Email ID");
                $("#lgemailerror").text("Kindly Enter Email ID");
                $("#lgemailerror").css("display", "block");
            } else if (!emailReg.test($scope.primary_email)) {
                $("#lgemailerror").text("Kindly Enter proper Email ID");
                $("#lgemailerror").css("display", "block");
            } else if (!$scope.password) {
                $("#lgpassworderror").text("Kindly Enter Password");
                $("#lgpassworderror").css("display", "block");
            } else {
                $scope.callloginapi();
            }
        };

        $scope.callloginapi = function () {
            var request = {
                "primary_email": $scope.primary_email,
                "password": $scope.password,
                "company_id": company_id
            }

            $http.post(done_host + "/done-user-login", request, done_host_parameters).success(function (response) {
                if (development_mode) {
                    //console.log(response);
                }
                if (response.responseCode === 0) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                    $scope.primary_email = "";
                    $scope.password = "";
                    $("#dvLoginPop").modal("hide");
                } else {
                    create_alert(1, response.responseMsg, 3);
                }
            }).error(function () {
                create_alert(1, "It's taking longer than expected. Retry?", 3);
            });
        };

        $scope.validateregistration = function () {
            $("#regnameerror").css("display", "none");
            $("#regemailerror").css("display", "none");
            $("#regphoneerror").css("display", "none");
            $("#regpinerror").css("display", "none");
            $("#regpassworderror").css("display", "none");



            if (!$scope.name) {
                $("#regnameerror").text("Kindly enter name");
                $("#regnameerror").css("display", "block");
            } else if (!$scope.reg_primary_email) {
                $("#regemailerror").text("Kindly enter Email");
                $("#regemailerror").css("display", "block");
            } else if (!emailReg.test($scope.reg_primary_email)) {
                $("#regemailerror").text("Kindly enter proper Email");
                $("#regemailerror").css("display", "block");
            } else if (!$scope.reg_primary_phone) {
                $("#regphoneerror").css("display", "block");
                $("#regphoneerror").text("Kindly enter Mobile Number");
            } else if (!new RegExp(/[7-9]{1}\d{9}/).test($scope.reg_primary_phone)) {
                $("#regphoneerror").css("display", "block");
                $("#regphoneerror").text("Kindly enter proper Mobile Number");
            } else if (!$scope.pincode) {
                $("#regpinerror").css("display", "block");
                $("#regpinerror").text("Kindly enter Pincode");
            } else if ($scope.pincode.length !== 5) {
                $("#regpinerror").css("display", "block");
                $("#regpinerror").text("Kindly enter proper Pincode");
            } else if (!$scope.reg_password) {
                $("#regpassworderror").css("display", "block");
                $("#regpassworderror").text("Kindly enter password");
            } else {
                $scope.callregistrationapi();

            }
        }

        $scope.callregistrationapi = function () {
            var request = {
                "name": $scope.name,
                "primary_phone": $scope.reg_primary_phone,
                "primary_email": $scope.reg_primary_email,
                "password": $scope.reg_password,
                "facebook_id": "",
                "company_id": company_id,
                "pincode": $scope.pincode,
                "outlet_id": "",
                "redirect_url": "http://localhost/srfood"
            }

            $http.post(done_host + "/done-user-register", request, done_host_parameters).success(function (response) {
                if (development_mode) {
                    //console.log(response);
                }
                if (response.responseCode === 0) {
                    $("#dvOtpPop").modal("show");
                    $("#dvSignUpPop").modal("hide");
                    $scope.sendOtp();
                }
            }).error(function () {
                create_alert(1, "It's taking longer than expected. Retry?", 3);
            });
        };


        $scope.sendOtp = function (type) {

            $scope.otp = {
                "primary_phone": $scope.reg_primary_phone,
                "company_id": company_id
            }
            $http.post(done_host + "/done-registration-otp", $scope.otp, {
                headers: {
                    client_platform: "web"
                },
                timeout: 5000
            }).success(function (response) {
                if (development_mode) {
                    //console.log(response);
                    create_alert(2, "OTP sent to your Mobile No.");
                }

            }).error(function (response) {

            });
        };
        $scope.verify_otp = function (type) {

            $scope.req = {
                "primary_phone": $scope.reg_primary_phone,
                "company_id": company_id,
                "otp": parseInt($scope.reg_otp)
            }
//                //console.log($scope.user_otp); 	 	
            $http.post(done_host + "/done-verify-mobile-by-otp", $scope.req, {
                headers: {
                    client_platform: "web"
                },
                timeout: 5000
            }).success(function (response) {
                if (development_mode) {
                    //console.log(response);
                }
//                        //console.log(response); 	 	
                if (response.responseCode === 0) {
//                            create_alert(2,"OTP Verified",5);
                    $scope.isverified = true;
                    $("#dvLoginPop").modal("show");
                    $("#dvOtpPop").modal("hide");
//                            return 1;
                } else if (response.responseCode === 252) {
                    $scope.isverified = false;
                    create_alert(1, "Please enter 6 digit valid code", 5);
                } else {
                    $scope.isverified = false;
                    create_alert(1, response.responseMsg, 5);
//                            return 0;
                }
            }).error(function (response) {
//                        return 0;
                $scope.isverified = false;
            });
            if ($scope.isverified === true) {
                return 1;
            } else {

                return 0;
            }
        };

        $scope.verify_social_link();



        try {
            localStorage.setItem("test", "1");
            localStorage.removeItem("test");

        }
        catch (e)
        {
            alert('Your web browser does not support storing settings locally. In Safari, the most common cause of this is using "Private Browsing Mode". Some settings may not save or some features may not work properly for you.');
        }
        /* Re-initialize Foundation */
        $scope.api_get_company_banner();

    }]);

function scrolltotop() {
    $('html,body').animate({
        scrollTop: 0
    }, 500);

}

function scrolltosection(classid) {
    $('html,body').animate({
        scrollTop: $("." + classid).offset().top - $(".dvHeader2").height() - 200
    }, 500);
    $(".dvMenuMob").css("left", "-300px");
    $("html").css("overflow", "unset");
    mob_menu_open = "false";
}

//minify version.
//function assign_css(){less.refresh(),$(".company-title").html(structure_config.company_name),less.modifyVars({"@bg_color":structure_config.css_config.bg_color,"@header_bg_color":structure_config.css_config.header_bg_color,"@header_bottom_color":structure_config.css_config.header_bottom_color,"@header_item_bg_color":structure_config.css_config.header_item_bg_color,"@header_item_bg_hover_color":structure_config.css_config.header_item_bg_hover_color,"@header_item_text_color":structure_config.css_config.header_item_text_color,"@header_item_text_hover_color":structure_config.css_config.header_item_text_hover_color,"@button_bg_color":structure_config.css_config.button_bg_color,"@button_bg_hover_color":structure_config.css_config.button_bg_hover_color,"@button_text_color":structure_config.css_config.button_text_color,"@button_text_hover_color":structure_config.css_config.button_text_hover_color,"@category_bg_color":structure_config.css_config.category_bg_color,"@category_bg_hover_color":structure_config.css_config.category_bg_hover_color,"@category_text_color":structure_config.css_config.category_text_color,"@category_text_hover_color":structure_config.css_config.category_text_hover_color,"@product_bg_color":structure_config.css_config.product_bg_color,"@product_bg_hover_border_color":structure_config.css_config.product_bg_hover_border_color,"@product_text_color":structure_config.css_config.product_text_color,"@product_description_color":structure_config.css_config.product_description_color,"@outlet_bg_color":structure_config.css_config.outlet_bg_color,"@outlet_text_color":structure_config.css_config.outlet_text_color,"@mobile_header_bg_color":structure_config.css_config.mobile_header_bg_color,"@mobile_header_text_color":structure_config.css_config.mobile_header_text_color,"@mobile_footer_bg_color":structure_config.css_config.mobile_footer_bg_color,"@mobile_footer_text_color":structure_config.css_config.mobile_footer_text_color,"@mobile_button_bg_color":structure_config.css_config.mobile_button_bg_color,"@mobile_button_text_color":structure_config.css_config.mobile_button_text_color,"@intro_text_color":structure_config.css_config.intro_text_color})}function setCategoryHeight(){$("#category-container").height($("#test").height())}function check_date_time(a){void 0!==a&&$.ajax({url:"ws/validatestoretime.php",method:"POST",data:{outlet_timing:a},success:function(a){return 1==a?!1:!1}})}function show_social_link(a){switch(a){case"facebook_link":window.open(structure_config.social_config.facebook_link,"_blank");break;case"twitter_link":window.open(structure_config.social_config.twitter_link,"_blank");break;case"android_link":window.open(structure_config.social_config.android_link,"_blank");break;case"ios_link":window.open(structure_config.social_config.ios_link,"_blank")}}function create_alert(a,b,c){$("div.alert-box.active").remove(),c*=1e3;var d;switch(a){case 1:d=$("div.alert-box.alert").clone();break;case 2:d=$("div.alert-box.success").clone()}$(d).addClass("active"),$(d).find(".alert-message").text(b),$("#alert-container").append(d),$(d).slideDown(animation_delay),c&&setTimeout(function(){$(d).slideUp(animation_delay,function(){$(d).remove()})},c)}$(document).ready(function(){function a(){$(".dvWrapper").hasClass("show-nav")?$(".dvWrapper").removeClass("show-nav"):$(".dvWrapper").addClass("show-nav")}$("#owl-demo").owlCarousel({autoPlay:3e3,items:3,itemsDesktop:[1199,3],itemsDesktopSmall:[979,3],navigation:!0,navigationText:["<i class='icon-chevron-left icon-white'><</i>","<i class='icon-chevron-right icon-white'>></i>"]}),$(".toggle-nav").click(function(){a()})});var animation_delay=structure_config.animation_delay,demongoon_host=structure_config.demongoon_host,done_host=structure_config.done_host,development_mode=structure_config.development_mode,company_id=structure_config.company_id,local_test_mode=structure_config.local_test_mode,german_server_test_mode=!1,german_server_prods_mode=!1;local_test_mode===!0&&(done_host="http://10.0.1.197:5000",demongoon_host="http://10.0.1.197:5001"),german_server_test_mode===!0&&(done_host="http://apiger.done.to",demongoon_host="http://apiger.done.to"),german_server_prods_mode===!0&&(done_host="http://prods.done.to",demongoon_host="http://prods.done.to");var done_host_parameters={timeout:5e3};assign_css();var app=angular.module("done",["ngAnimate","ngRoute","ngTouch"]);app.config(["$httpProvider","$routeProvider",function(a,b){b.when("/",{redirectTo:"/home"}).when("/home",{templateUrl:"templates/home.html",controller:"homeCtrl"}).when("/outlets",{templateUrl:"templates/outlets.html",controller:"outletsCtrl"}).when("/menu",{templateUrl:"templates/menu.html",controller:"menuCtrl"}).when("/checkout",{templateUrl:"templates/checkout.html",controller:"checkoutCtrl"}).when("/orderhistory",{templateUrl:"templates/orderhistory.html",controller:"orderhistoryCtrl"}).when("/aboutus",{templateUrl:"templates/aboutus.html",controller:"aboutusCtrl"}).when("/contactus",{templateUrl:"templates/contactus.html",controller:"contactusCtrl"}).when("/login",{templateUrl:"templates/login.html",controller:"loginCtrl"}).otherwise({redirectTo:"/outlets"})}]),app.controller("mainCtrl",["$location","$rootScope","$scope","$http","$route",function(a,b,c,d,e){c.city_outlet_selection=structure_config.city_outlet_selection,c.only_outlet_selection=structure_config.only_outlet_selection,c.city_area_selection=structure_config.city_area_selection,c.verified_facebook_link="",c.verified_twitter_link="",c.verified_android_link="",c.verified_ios_link="",c.enable_login=structure_config.enable_login,c.pages=[{label:"Outlets",faclass:"fa-home",url_on:"img/home-on.png",url_off:"img/home-off.png",route:"/outlets"},{label:"Menu",faclass:"fa-bars",url_on:"img/menu-on.png",url_off:"img/menu-off.png",route:"/menu"},{label:"Checkout",faclass:"fa-shopping-cart",url_on:"img/checkout-on.png",url_off:"img/checkout-off.png",route:"/checkout"},{label:"Order History",faclass:"fa-history",url_on:"img/order-history-on.png",url_off:"img/order-history-off.png",route:"/orderhistory"},{label:"About Us",faclass:"fa-star",url_on:"img/about-on.png",url_off:"img/about-off.png",route:"/aboutus"},{label:"Contact Us",faclass:"fa-phone",url_on:"img/contact-on.png",url_off:"img/contact-off.png",route:"/contactus"}],c.outlet_list=[],c.outlets=[],c.outlet_selected=[],c.outlet_fetched=!1,c.Showoutletpopup=function(){$("#myModal").modal("show")},c.api_outlets_by_company=function(){c.selected_subarea={subarea_id:null,subarea_name:null,area_id:null,area_name:null,city_id:null,city_name:null},null!==localStorage.getItem("subarea")&&localStorage.removeItem("subarea"),c.search_keyword="";var a={company_id:company_id,outlet_last_updated_timestamp:0,ot_last_updated_timestamp:0,oa_last_updated_timestamp:0,oam_last_updated_timestamp:0,oag_last_updated_timestamp:0,offer_last_updated_timestamp:0,oom_last_updated_timestamp:0};d.post(done_host+"/done-outlets-by-company",a,done_host_parameters).success(function(a){c.show_splash_screen=!1,c.outlets=a.data.outlets,c.outlet_timing=a.data.outlet_timing,c.set_outlet_datetime(),c.only_outlet_selection&&c.set_outlet_list(),$("#loader_popup").css("display","none")}).error(function(){create_alert(1,"It's taking longer than expected. Retry?",3),c.error_text="It's taking longer than expected. Retry?",c.show_splash_screen=!0})},c.api_get_city=function(){d.get("Get_Solar_Webservices.php?webservice_name=cities_by_company&company_id="+company_id).success(function(a){$("#loader_popup").css("display","none"),development_mode?//console.log(a):c.cities=a.response.docs})},c.get_location=function(){var a=0;$("#loader_popup").css("display","block"),void 0!==c.city_select||""!==c.city_select?(a=c.city_select.city_id,localStorage.setItem("city",JSON.stringify(c.city_select)),d.get("Get_Solar_Webservices.php?webservice_name=company_subareas&company_id="+company_id+"&city_id="+a).success(function(a){development_mode?//console.log(a):($("#loactions").removeAttr("disabled"),c.areas=a.response.docs,c.filterAreas(a.response.docs)),$("#loader_popup").css("display","none")})):(create_alert(1,"Kindly select a city",3),$("#loader_popup").css("display","none"))},c.filterAreas=function(a){c.areas=[];var d=function(a){return a.area_name};return angular.forEach(a,function(a){for(var e=!1,f=0;f<c.areas.length;f++)if(angular.equals(d(c.areas[f]),d(a))){e=!0;break}e||c.areas.push(a)}),a=c.areas},c.api_nearest_outlets_by_subarea=function(){if(c.outlets=[],void 0!==c.area_select){var a={company_id:company_id,subarea_id:c.area_select.subarea_id,service_type:"food"};d.post(done_host+"/done-nearest-outlets-by-subarea",a,done_host_parameters).success(function(a){0!==a.responseCode?(c.outlets=[],c.outlets_timings=[],create_alert(1,"No outlets serving "+c.selected_subarea.subarea_name+".",3)):(c.outlet_fetched=!0,a.data.outlets.length>1?angular.forEach(a.data.outlets,function(a){c.outlets.push(a),c.set_outlet_list()}):(c.outlets=a.data.outlets,c.outlet_select=a.data.outlets[0]),c.outlet_timing=a.data.outlet_timing)}).error(function(){create_alert(1,"It's taking longer than expected. Retry?",3)})}else create_alert(1,"Kindly select Location",3)},c.set_outlet_datetime=function(){for(var a=moment(),b=a.day()+1,d=0;d<c.outlets.length;d++)angular.forEach(c.outlet_timing,function(e){if(c.outlets[d].id===e.outlet_id&&e.day_of_week===b){void 0===c.outlets[d].outlet_timing&&(c.outlets[d].outlet_timing=[]),c.outlets[d].outlet_timing.push(e);var f=moment(e.open_time,"HH:mm:ss"),g=moment(e.duration,"HH:mm:ss"),h=moment(f._d).add(g._d,"hours"),i=!1;a>=f&&h>=a&&(i=!0);var j=moment(f).format("hh:mm A"),k=moment(h).format("hh:mm A");void 0!==c.outlets[d].is_open?c.outlets[d].is_open!==!0&&(c.outlets[d].is_open=i,c.outlets[d].start_time=j,c.outlets[d].end_time=k):(c.outlets[d].is_open=i,c.outlets[d].start_time=j,c.outlets[d].end_time=k),void 0===c.outlets[d].timing_string?c.outlets[d].timing_string=j+" to "+k:c.outlets[d].timing_string=c.outlets[d].timing_string+" or "+j+" to "+k}});//console.log(c.outlets)},c.view_menu=function(){var b=c.outlet_select;//console.log(c.outlet_select),null===localStorage.getItem("outlet")?(localStorage.setItem("outlet",JSON.stringify(angular.copy(b))),localStorage.setItem("city_name",c.outlet_select.city_name),c.city_name=localStorage.getItem("city_name"),localStorage.removeItem("cart"),$("#myModal").modal("hide"),$("body").removeClass("modal-open"),$(".modal-backdrop").remove(),a.url("menu")):JSON.parse(localStorage.getItem("outlet")).id!==b.id&&null!==localStorage.getItem("cart")&&0!==JSON.parse(localStorage.getItem("cart")).length?confirm("Your cart will be emptied on changing the outlet. Continue?")&&(localStorage.setItem("outlet",JSON.stringify(angular.copy(b))),localStorage.setItem("city_name",c.outlet_select.city_name),c.city_name=localStorage.getItem("city_name"),localStorage.removeItem("cart"),$("#myModal").modal("hide"),$("body").removeClass("modal-open"),$(".modal-backdrop").remove(),a.url("menu")):(localStorage.setItem("outlet",JSON.stringify(angular.copy(b))),localStorage.setItem("city_name",c.outlet_select.city_name),c.city_name=localStorage.getItem("city_name"),localStorage.removeItem("cart"),$("#myModal").modal("hide"),$("body").removeClass("modal-open"),$(".modal-backdrop").remove(),a.url("menu"))},c.view_main_menu=function(){e.reload();try{void 0!==c.outlet_select&&(null!==localStorage.getItem("outlet")&&JSON.parse(localStorage.getItem("outlet")).id!==c.outlet_select.id?null!==localStorage.getItem("cart")&&0!==JSON.parse(localStorage.getItem("cart")).length?confirm("Your cart will be emptied on changing the outlet. Continue?")&&(localStorage.setItem("outlet",JSON.stringify(angular.copy(c.outlet_select))),localStorage.setItem("city_name",c.city_select.city_name),c.city_name=localStorage.getItem("city_name"),localStorage.removeItem("cart"),b.root_item_count=0,$("#myModal").modal("hide"),$("body").removeClass("modal-open"),$(".modal-backdrop").remove(),a.url("menu")):(localStorage.setItem("outlet",JSON.stringify(angular.copy(c.outlet_select))),localStorage.setItem("city_name",c.city_select.city_name),c.city_name=localStorage.getItem("city_name"),$("#myModal").modal("hide"),$("body").removeClass("modal-open"),$(".modal-backdrop").remove(),a.url("menu")):($("#myModal").modal("hide"),$("body").removeClass("modal-open"),$(".modal-backdrop").remove(),localStorage.setItem("outlet",JSON.stringify(c.outlet_select)),localStorage.setItem("city_name",c.city_select.city_name),c.city_name=localStorage.getItem("city_name"),a.url("menu")))}catch(d){c.error_text='Your web browser does not support storing settings locally. In Safari, the most common cause of this is using "Private Browsing Mode". Some settings may not save or some features may not work properly for you.',alert(d)}},c.set_outlet_list=function(){if(c.outlet_list=[],c.city_outlet_selection)for(var a=0;a<c.outlets.length;a++)parseInt(c.city_select.city_id)===parseInt(c.outlets[a].city_id)&&c.outlet_list.push(c.outlets[a]);else if(c.only_outlet_selection)c.outlet_list=c.outlets;else if(c.city_area_selection&&0==c.outlet_fetched)c.get_location();else if(c.city_area_selection&&c.outlet_fetched)for(var a=0;a<c.outlets.length;a++)c.outlet_list.push(c.outlets[a])},c.city_outlet_selection?(c.api_get_city(),c.api_outlets_by_company()):c.only_outlet_selection?(c.api_outlets_by_company(),c.set_outlet_list()):c.city_area_selection&&c.api_get_city(),c.page_active="/",b.$on("$routeChangeSuccess",function(){c.page_active=a.path()}),c.verify_social_link=function(){try{void 0!==structure_config.social_config.facebook_link&&(c.verified_facebook_link=structure_config.social_config.facebook_link)}catch(a){}try{void 0!==structure_config.social_config.twitter_link&&(c.verified_twitter_link=structure_config.social_config.twitter_link)}catch(a){}try{void 0!==structure_config.social_config.android_link&&(c.verified_android_link=structure_config.social_config.android_link)}catch(a){}try{void 0!==structure_config.social_config.ios_link&&(c.verified_ios_link=structure_config.social_config.ios_link)}catch(a){}},c.verify_social_link();try{localStorage.setItem("test","1"),localStorage.removeItem("test")}catch(f){alert('Your web browser does not support storing settings locally. In Safari, the most common cause of this is using "Private Browsing Mode". Some settings may not save or some features may not work properly for you.')}}]);