// Global stuff
var animation_delay = 750;
//var demongoon_host = "http://apiserver.done.to";
//var done_host = "http://apiserver.done.to";
var demongoon_host = "http://10.0.1.197:5001";
var done_host = "http://10.0.1.197:5000";
var development_mode = false;
var company_id = structure_config.company_id;
var local_test_mode = false;
var german_server_test_mode = false;
var german_server_prods_mode = false;
if (local_test_mode === true) {
//local server
    done_host = "http://10.0.1.197:5000";
    demongoon_host = "http://10.0.1.197:5001";
}
if (german_server_test_mode === true)
{
    done_host = "http://apiger.done.to";
    demongoon_host = "http://apiger.done.to";
}
if (german_server_prods_mode === true)
{
    done_host = "http://prods.done.to";
    demongoon_host = "http://prods.done.to";
}


var done_host_parameters = {
    timeout: 8000
};
//var done_host_parameters = {
//    timeout: 5000
//};
function assign_css()
{
    less.refresh();
    $(".company-title").html(structure_config.company_name);
    less.modifyVars({
        "@bg_color": structure_config.css_config.bg_color,
        "@header_bg_color": structure_config.css_config.header_bg_color,
        "@header_bottom_color": structure_config.css_config.header_bottom_color,
        "@header_item_bg_color": structure_config.css_config.header_item_bg_color,
        "@header_item_bg_hover_color": structure_config.css_config.header_item_bg_hover_color,
        "@header_item_text_color": structure_config.css_config.header_item_text_color,
        "@header_item_text_hover_color": structure_config.css_config.header_item_text_hover_color,
        "@button_bg_color": structure_config.css_config.button_bg_color,
        "@button_bg_hover_color": structure_config.css_config.button_bg_hover_color,
        "@button_text_color": structure_config.css_config.button_text_color,
        "@button_text_hover_color": structure_config.css_config.button_text_hover_color,
        "@category_bg_color": structure_config.css_config.category_bg_color,
        "@category_bg_hover_color": structure_config.css_config.category_bg_hover_color,
        "@category_text_color": structure_config.css_config.category_text_color,
        "@category_text_hover_color": structure_config.css_config.category_text_hover_color,
        "@product_bg_color": structure_config.css_config.product_bg_color,
        "@product_bg_hover_border_color": structure_config.css_config.product_bg_hover_border_color,
        "@product_text_color": structure_config.css_config.product_text_color,
        "@product_description_color": structure_config.css_config.product_description_color,
        "@outlet_bg_color": structure_config.css_config.outlet_bg_color,
        "@outlet_text_color": structure_config.css_config.outlet_text_color,
        "@mobile_header_bg_color": structure_config.css_config.mobile_header_bg_color,
        "@mobile_header_text_color": structure_config.css_config.mobile_header_text_color,
        "@mobile_footer_bg_color": structure_config.css_config.mobile_footer_bg_color,
        "@mobile_footer_text_color": structure_config.css_config.mobile_footer_text_color,
        "@mobile_button_bg_color": structure_config.css_config.mobile_button_bg_color,
        "@mobile_button_text_color": structure_config.css_config.mobile_button_text_color,
        "@intro_text_color": structure_config.css_config.intro_text_color,
    });

}

assign_css();

function setStickyFooter() {
    var footer = $(".bottom-bar");
    var pos = footer.position();
    var height = $(window).height();
    height = height - pos.top;
    height = height - footer.height();

    if (height > 0) {
        footer.css({
            'margin-top': height + 'px'
        });
    }


}





function setCategoryHeight()
{
    $("#category-container").height($("#test").height());

}

function check_date_time(outlet_timing)
{
    var is_open = false;
    if (outlet_timing !== undefined)
    {
        var today = moment();
        var day_of_week = today.day() + 1;

        for (var i = 0; i < outlet_timing.length; i++)
        {
            var start_time = moment(outlet_timing[i].open_time, "HH:mm:ss");
            var duration = moment(outlet_timing[i].duration, "HH:mm:ss");
            var end_time = moment(start_time._d).add(duration._d, 'hours');
            if ((today >= start_time && today <= end_time) || (today <= start_time))
            {
                is_open = true;
                return is_open;
            }
        }
    }
    else
    {
        is_open = true;
    }

    return is_open;

}







function show_social_link(social_link) {
    switch (social_link)
    {
        case "facebook_link":
            window.open(structure_config.social_config.facebook_link, '_blank');
            break;
        case "twitter_link":
            window.open(structure_config.social_config.twitter_link, '_blank');
            break;
        case "android_link":
            window.open(structure_config.social_config.android_link, '_blank');
            break;
        case "ios_link":
            window.open(structure_config.social_config.ios_link, '_blank');
            break;
    }

}

function create_alert(severity, message, timeout) {
    $("div.alert-box.active").remove();
    timeout *= 1000;
    var alert_div;
    switch (severity) {
        case 1:
            alert_div = $("div.alert-box.alert").clone();
            break;
        case 2:
            alert_div = $("div.alert-box.success").clone();
            break;
    }
    $(alert_div).addClass("active");
    $(alert_div).find(".alert-message").text(message);
    $("#alert-container").append(alert_div);
    $(alert_div).slideDown(animation_delay);
    if (timeout) {
        setTimeout(function () {
            $(alert_div).slideUp(animation_delay, function () {
                $(alert_div).remove();
            });
        }, timeout);
    }
}


// Module
var app = angular.module('done', ['ngAnimate', 'ngRoute', 'ngTouch']);


// Routes
app.config(["$httpProvider", "$routeProvider", function ($httpProvider, $routeProvider) {
        $routeProvider
                // Open the Menu page by default. If no outlet is found in localStorage (i.e. first-time user) user gets redirected to the Outlets page
                .when("/", {
                    redirectTo: "/home"
                })
                .when("/home", {
                    templateUrl: "templates/outlets.html",
                    controller: "outletsCtrl"
                })
                .when("/How it works", {
                    templateUrl: "templates/outlets.html",
                    controller: "outletsCtrl"
                })
//                .when("/Bulk Orders", {
//                    templateUrl: "templates/outlets.html",
//                    controller: "outletsCtrl"
//                })

                .when("/menu", {
                    templateUrl: "templates/menu.html",
                    controller: "menuCtrl"
                })
                .when("/checkout", {
                    templateUrl: "templates/checkout.html",
                    controller: "checkoutCtrl"
                })
                .when("/orderhistory", {
                    templateUrl: "templates/orderhistory.html",
                    controller: "orderhistoryCtrl"
                })
                .when("/contactus", {
                    templateUrl: "templates/contactus.html",
                    controller: "contactusCtrl"
                })
                .when("/aboutus", {
                    templateUrl: "templates/aboutus.html",
                    controller: "aboutusCtrl"
                })
                .when("/contact_mob", {
                    templateUrl: "templates/contact_mob.html",
                    controller: "contact_mobCtrl"
                })
//                .when("/privacy", {
//                    templateUrl: "templates/outlets.html",
//                    controller: "outletsCtrl"
//                })
//                .when("/blog", {
//                    templateUrl: "templates/contactus.html",
//                    controller: "contactusCtrl"
//                })
//                .when("/privacypolicy", {
//                    templateUrl: "templates/privacypolicy.html",
//                    controller: "privacyCtrl"
//                })
                // Any other URL redirects to the Menu page
                .otherwise({
                    redirectTo: "/home"
                });
    }]);
// Controllers
app.controller("mainCtrl", ["$http", "$location", "$rootScope", "$scope", function ($http, $location, $rootScope, $scope) {
        $("#mainContainer").show();
        $scope.verified_facebook_link = "";
        $scope.verified_twitter_link = "";
        $scope.verified_android_link = "";
        $scope.verified_ios_link = "";
        $scope.pages = [
            {
                label: "Home",
                faclass: "fa-home",
                url_on: "img/home-on.png",
                url_off: "img/home-off.png",
                route: "/outlets"
            },
//            {
//                label: "How it works?",
//                faclass: "fa-home",
//                url_on: "img/home-on.png",
//                url_off: "img/home-off.png",
//                route: "/outlets"
//            },
//            {
//                label: "Bulk Orders",
//                faclass: "fa-home",
//                url_on: "img/home-on.png",
//                url_off: "img/home-off.png",
//                route: "/outlets"
//            },
//            {
//                label: "Menu",
//                faclass: "fa-bars",
//                url_on: "img/menu-on.png",
//                url_off: "img/menu-off.png",
//                route: "/menu"
//            },
//            {
//                label: "Checkout",
//                faclass: "fa-shopping-cart",
//                url_on: "img/checkout-on.png",
//                url_off: "img/checkout-off.png",
//                route: "/checkout"
//            },
//            {
//                label: "Order History",
//                faclass: "fa-history",
//                url_on: "img/order-history-on.png",
//                url_off: "img/order-history-off.png",
//                route: "/orderhistory"
//            },
//            {
//                label: "About Us",
//                faclass: "fa-star",
//                url_on: "img/about-on.png",
//                url_off: "img/about-off.png",
//                route: "/aboutus"
//            },
//            {
//                label: "Contact Us",
//                faclass: "fa-phone",
//                url_on: "img/contact-on.png",
//                url_off: "img/contact-off.png",
//                route: "/contactus"
//            },
//            {
//                label: "Privacy Policy",
//                faclass: "fa-phone",
//                url_on: "img/contact-on.png",
//                url_off: "img/contact-off.png",
//                route: "/privacypolicy"
//            }
        ];
        $scope.page_active = "/";
        $rootScope.$on("$routeChangeSuccess", function () {
            $scope.page_active = $location.path();
        });

        $scope.api_outlets_by_company = function () {
            // Unset selected subarea in scope
            $scope.selected_subarea = {
                subarea_id: null,
                subarea_name: null,
                area_id: null,
                area_name: null,
                city_id: null,
                city_name: null
            };
            // Unset selected subarea in localStorage
//            if (localStorage.getItem("subarea") !== null) {
//                localStorage.removeItem("subarea");
//            }
            // Update contents of the search box
            $scope.search_keyword = "";
            // Request object
            var request = {
                "company_id": company_id,
                "outlet_last_updated_timestamp": 0,
                "ot_last_updated_timestamp": 0,
                "oa_last_updated_timestamp": 0,
                "oam_last_updated_timestamp": 0,
                "oag_last_updated_timestamp": 0,
                "offer_last_updated_timestamp": 0,
                "oom_last_updated_timestamp": 0
            };
            //create_alert(2, "Loading...", false);
            // Call web-service
            $http.post(done_host + "/done-outlets-by-company", request, done_host_parameters).success(function (response) {
                //create_alert(2, "Welcome!", 3);
                if (development_mode) {
                    //console.log(response);
                }
                $scope.show_splash_screen = false;
                $scope.outlets = response.data.outlets;
                $scope.outlet_timing = response.data.outlet_timing;
                $scope.set_dummy(response.data.outlets[0]);
                $scope.set_outlet_datetime();
                $("#loader_popup").css("display", "none");

                ////console.log($scope.outlets);
            }).error(function () {
                create_alert(1, "It's taking longer than expected. Retry?", 3);
                $scope.error_text = "It's taking longer than expected. Retry?";
                $scope.show_splash_screen = true;
            });
        };

        $scope.set_dummy = function (outlet)
        {
            if (structure_config.dummy_template === 'true')
            {
                localStorage.setItem("outlet", JSON.stringify(angular.copy(outlet)));
            }
        };

        $scope.set_outlet_datetime = function ()
        {
            var today = moment();
            var day_of_week = today.day() + 1;
            //console.log(today.toString());
            for (var i = 0; i < $scope.outlets.length; i++)
            {
                angular.forEach($scope.outlet_timing, function (outlet_time)
                {
                    if ($scope.outlets[i].id === outlet_time.outlet_id && outlet_time.day_of_week === day_of_week)
                    {
                        if ($scope.outlets[i].outlet_timing === undefined)
                        {
                            $scope.outlets[i].outlet_timing = [];
                        }

                        $scope.outlets[i].outlet_timing.push(outlet_time);
                        var start_time = moment(outlet_time.open_time, "HH:mm:ss");
                        var duration = moment(outlet_time.duration, "HH:mm:ss");
                        var end_time = moment(start_time._d).add(duration._d, 'hours');
                        var is_open = false;
                        if (today >= start_time && today <= end_time)
                        {
                            is_open = true;
                        }

                        var format_start_time = moment(start_time).format("hh:mm A");
                        var format_end_time = moment(end_time).format("hh:mm A");
                        if ($scope.outlets[i].is_open !== undefined)
                        {
                            if ($scope.outlets[i].is_open !== true)
                            {
                                $scope.outlets[i].is_open = is_open;
                                $scope.outlets[i].start_time = format_start_time;
                                $scope.outlets[i].end_time = format_end_time;
                            }
                        }
                        else
                        {
                            $scope.outlets[i].is_open = is_open;
                            $scope.outlets[i].start_time = format_start_time;
                            $scope.outlets[i].end_time = format_end_time;
                        }

                        if ($scope.outlets[i].timing_string === undefined)
                        {
                            $scope.outlets[i].timing_string = format_start_time + " to " + format_end_time;
                        }
                        else
                        {
                            $scope.outlets[i].timing_string = $scope.outlets[i].timing_string + " or " + format_start_time + " to " + format_end_time;
                        }

                    }

                });
            }
        };


        $scope.verify_social_link = function ()
        {
            try {
                if (structure_config.social_config.facebook_link !== undefined)
                {
                    $scope.verified_facebook_link = structure_config.social_config.facebook_link;
                }
            }
            catch (e) {
            }
            try {
                if (structure_config.social_config.twitter_link !== undefined)
                {
                    $scope.verified_twitter_link = structure_config.social_config.twitter_link;
                }
            }
            catch (e) {
            }
            try {
                if (structure_config.social_config.android_link !== undefined)
                {
                    $scope.verified_android_link = structure_config.social_config.android_link;
                }
            }
            catch (e) {
            }

            try {
                if (structure_config.social_config.ios_link !== undefined)
                {
                    $scope.verified_ios_link = structure_config.social_config.ios_link;
                }
            }
            catch (e) {
            }


        };
        $scope.verify_social_link();



        try {
            localStorage.setItem("test", "1");
            localStorage.removeItem("test");

        }
        catch (e)
        {
            alert('Your web browser does not support storing settings locally. In Safari, the most common cause of this is using "Private Browsing Mode". Some settings may not save or some features may not work properly for you.');
        }
        /* Re-initialize Foundation */

//        var date_min = moment("2015-07-15");
//        var date_max= moment("2015-07-16");
//        var now = moment();

//        if (now >= date_min && now<date_max) {
//           $('#emergency').foundation('reveal', 'open'); 
//        } else {
         // $scope.api_outlets_by_company();
//        }
         
//        $scope.api_outlets_by_company();

        var dt = moment(moment(), "YYYY-MM-DD HH:mm:ss");
       // alert(dt.format('dddd'));
        if(dt.format('dddd')==='Saturday'){
         
                 $('#emergency').foundation('reveal', 'open');
        }else if(dt.format('dddd')==='Sunday'){
         
                 $('#sunday_message').foundation('reveal', 'open');
        }else{
            $scope.api_outlets_by_company();
        }

    }]);


  




