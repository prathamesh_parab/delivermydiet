var structure_config = {
    //Company Configuration Part
    "company_id": "6901",
    "done_host":"http://api.done.to",
//    "done_host":"http://10.0.0.32:5000",
    //For menu Web service.
    "demongoon_host":"http://api.done.to",
//    "demongoon_host":"http://apiserver.done.to",
    "company_name": "Maroosh",
    "animation_delay":750,
    "development_mode" :false,
    "local_test_mode" : false,
    
    
    
    //Outlet selection configuration
    "city_outlet_selection":false, //Give a selection of City and Outlet.
    "only_outlet_selection":true, // Show Only Outlet selection.
    "city_area_selection":false, // Give a selection of City and Area.
    
    //Make sure from above three parameters only one should be true else false.
    
    
    
    //Static Pages
    "aboutus_text": " Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br/>gallery of type and scrambled it to make a type specimen book.",
    "intro_text": "<span>Lorem Ipsum</span>",
    "analytics_tracking_id": "",
    
    
    "dummy_template":"true", //Not in Use
    "show_outlet_search":"true", //Not in Use
    /* Added flags for functionality */
    
//    Menu configuration
    "soldout":false,
    "side_category":false,//To display categories on left side. else it will show at top.
    "with_image_template": true,
    "product_grid_size":2,
    
    "enable_otp":false,//for first time user only at the time of checkout only when login functionality is not applicable.
    "enable_login":false,
//    "login":"true",
    "fb_login":"false", //Not in use
    "show_area_subarea":"true", //Not in Use
    "show_preorder_selection":"true",//Not in Use
    
    
    
    //Deliver time slots configuration
    "timeslot_selection":"true",                //to give option to select delivery date and time or not.
    "pre_order_dates":3,                        //days to allow pre order.
    "start_time_slot":"notfixed",                  //time slot should be fixed or it should start with current time.
    "start_time_interval":"45",                 //Start time will start after defined in mins.
    "timeslot_interval":"45",                   //time slots interval in mins.
    
    
    "validate_time_on_outlet":"true",          //Outlet closed validation on Outlet page or default Menu Page
    "validate_min_order":"true",               //Validation of Minimum order.
    
    // Payment options
    "online_payment":"false",                   //If false default will be COD or Online payment --> true
    "city_selection":"false",                   //Either selection for city -->true or Box type outlet --.false
    
    "cash_on_delivery":"true",
    "citrus":false,
    "payu":true,
    "paytm":false,
    "ccavenue":false,
    "direcpay":false,
    "braintree":false,
    "social_config": {
        "facebook_link": "http://www.facebook.com",
        "twitter_link": "http://www.facebook.com",
        "android_link": "http://www.facebook.com",
        "ios_link": "http://www.facebook.com"
    },
    "css_config": {
        "bg_color": "#EDEDEE",
        "header_bg_color": "#FFFFFF",
        "header_bottom_color": "#E7E7E8",
        "header_item_bg_color": "#FFFFFF",
        "header_item_bg_hover_color": "#F2F2F2",
        "header_item_text_color": "#616161",
        "header_item_text_hover_color": "#EE2726",
        "button_bg_color": "#EE2726",
        "button_bg_hover_color": "rgba(238, 39, 38, 0.58)",
        "button_text_color": "#fff",
        "button_text_hover_color": "#000",
        "category_bg_color": "#DEDEDE",
        "category_bg_hover_color": "#EBEBEC",
        "category_text_color": "#616161",
        "category_text_hover_color": "#EE2725",
        "product_bg_color": "#FFF",
        "product_bg_hover_border_color": "#ccc",
        "product_text_color": "#616161",
        "product_description_color": "#878787",
        "outlet_bg_color": "#fff",
        "outlet_text_color": "#3A3A3A",
        "mobile_header_bg_color": "#242424",
        "mobile_header_text_color": "#fff",
        "mobile_footer_bg_color": "#242424",
        "mobile_footer_text_color": "#FFCD01",
        "mobile_button_bg_color": "#F04E23",
        "mobile_button_text_color": "#FFFFFF",
        "intro_text_color": "#EE2726"
    }
};