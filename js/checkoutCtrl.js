app.controller("checkoutCtrl", ["$http", "$location", "$scope", function ($http, $location, $scope) {

        //Config Variables...
        $scope.enable_login = structure_config.enable_login;
        $scope.delivery_pickup_time = structure_config.delivery_pickup_time;
        $scope.stepcomplete = 0;
        $scope.is_personal_info_complete = false;
        $scope.stepcomplete = 0;
        $scope.edit_personal_info = true;
        $scope.edit_address_info = true;
        $scope.btn_disable = true;
        $scope.coupon_amount = null;
        //timeslot variables.
        $scope.timeslot_selection = structure_config.timeslot_selection;
        $scope.pre_order_dates = structure_config.pre_order_dates;
        $scope.start_time_slot = structure_config.start_time_slot;
        $scope.start_time_interval = structure_config.start_time_interval;
        $scope.timeslot_interval = structure_config.timeslot_interval;
        $scope.enable_otp = structure_config.enable_otp;
        $scope.deliver_type = "ordernow";
        //Payment Options
//        $scope.payment_mode = "COD";
        $scope.citrus = structure_config.citrus;
        $scope.payu = structure_config.payu;
        $scope.quikwallet = structure_config.quikwallet;
        $scope.paytm = structure_config.paytm;
        $scope.braintree = structure_config.braintree;
        $scope.ccavenue = structure_config.ccavenue;
        $scope.direcpay = structure_config.direcpay;
        $scope.new_user = 0;

        $scope.show_subarea_selection = structure_config.show_subarea_selection;

        $scope.usermobiledisabled = false;
        if (localStorage.getItem("selected_date")) {
            $scope.selected_date = JSON.parse(localStorage.getItem("selected_date"));
        }
        if ($scope.enable_login == "true" && localStorage.getItem("user") === null) {

            create_alert(1, "Kindly login to get proceed", 5);
            $location.url("login");
        }




        $('#slider').css('visibility', 'hidden');
        if (localStorage.getItem("outlet") === null) {

            create_alert(1, "Please choose an outlet.", 3);
            $location.url("outlets");
        } else if (localStorage.getItem("cart") === null || JSON.parse(localStorage.getItem("cart")).length === 0) {
            create_alert(1, "Your cart is empty!", 3);

            $location.url("menu");
        } else {

            $scope.get_user_details = function () {
                create_alert(2, "Finding you...", false);

                $http.post(done_host + "/done-get-company-user", {primary_phone: $scope.user_mobile, company_id: company_id}, done_host_parameters).success(function (response) {
                    if (development_mode) {
                        //console.log(response);
                    }
                    $scope.find_me_called = true;
                    if (response.responseCode === 0) {
                        create_alert(2, response.data.name + "! Welcome back!", 3);
                        $scope.user = response.data;
                        angular.forEach(response.data.addresses, function (address) {
                            if (address.subarea_id !== null && address.area_name !== null && address.tag !== null)
                            {
                                if ($scope.user_address_subareas !== null || $scope.user_address_subareas !== undefined)
                                {
                                    for (var k = 0; k < $scope.user_address_subareas.length; k++)
                                    {
                                        if ($scope.user_address_subareas[k].id === address.subarea_id)
                                        {

                                            var address_string = address.flat_no + ", " + (address.society !== null ? address.society + ', ' : '') + address.street + "," + address.landmark;
                                            var address_full_string = address_string + ", " + (address.subarea_name !== null ? address.subarea_name + ", " : "") + (address.area_name !== null ? address.area_name + " " : "");

                                            if (address.tag !== null && address.tag !== "")
                                            {
                                                address.address_string = address.tag.toUpperCase();
                                            }
                                            else
                                            {
                                                address.address_string = address_string;
                                            }
                                            address.address_full_string = address_full_string;
                                            $scope.user_addresses.push(address);
                                            $scope.mobile_user_addresses.push(address);
                                        }
                                    }
                                }
                            }
                        });
                        var dummy_address = {
                            id: 0,
                            tag: '',
                            flat_no: '',
                            street: '',
                            landmark: '',
                            address_string: 'Add New Address'

                        };
                        $scope.mobile_user_addresses.push(dummy_address);

                        $scope.selected_address = $scope.user_addresses[0];
                        if ($scope.mobile_showTab === $scope.mobile_userTab) {
                            $scope.selected_address = $scope.mobile_user_addresses[0];
                        }
                        $scope.new_user = 0;

                    } else if (response.responseCode === 250) {

                        create_alert(2, "Hi! you seem to be new here...", 3);
                        $scope.new_user = 1;
//                        $scope.sendOtp("new user");
//                        $scope.isverified = false;

                        $scope.user = {
                            id: null,
                            name: ""
                        };
                        $scope.selected_address = null;
                    }
                    else {
                        create_alert(1, "There seems to be some issue. Retry?", 3);
                    }
                    localStorage.setItem("user", JSON.stringify({"primary_phone": $scope.user_mobile}));
                }).error(function () {
                    create_alert(1, "It's taking longer than expected. Retry?", 3);
                });

            };

            $scope.sendOtp = function (type) {

                $scope.otp = {
                    "primary_phone": $scope.user_mobile,
                    "company_id": company_id
                }
                $http.post(done_host + "/done-registration-otp", $scope.otp, {
                    headers: {
                        client_platform: "web"
                    },
                    timeout: 5000
                }).success(function (response) {
                    if (development_mode) {
                        //console.log(response);
                        create_alert(2, "OTP sent to your Mobile No.");
                    }

                }).error(function (response) {

                });
            };
            $scope.verify_otp = function (type) {

                $scope.req = {
                    "primary_phone": $scope.user_mobile,
                    "company_id": company_id,
                    "otp": parseInt($scope.user_otp)
                }
//                //console.log($scope.user_otp); 	 	
                $http.post(done_host + "/done-verify-mobile-by-otp", $scope.req, {
                    headers: {
                        client_platform: "web"
                    },
                    timeout: 5000
                }).success(function (response) {
                    if (development_mode) {
                        //console.log(response);
                    }
//                        //console.log(response); 	 	
                    if (response.responseCode === 0) {
                        $("#otp-modal").bsModal("hide");
                        if ($scope.payment_mode === 'COD') {
                            $scope.order.payment_method = $scope.payment_mode;
                            $scope.api_save_order();
                        } else {
                            $scope.order.payment_method = $scope.payment_mode;
                            $scope.save_cart($scope.payment_mode);
                        }
//                            create_alert(2,"OTP Verified",5);
                        $scope.isverified = true;

//                            return 1;
                    } else if (response.responseCode === 252) {
                        $scope.isverified = false;
                        create_alert(1, "Please enter 6 digit valid code", 5);
                    } else {
                        $scope.isverified = false;
                        create_alert(1, response.responseMsg, 5);
//                            return 0;
                    }
                }).error(function (response) {
//                        return 0;
                    $scope.isverified = false;
                });
                if ($scope.isverified === true) {
                    return 1;
                } else {

                    return 0;
                }
            };

            $scope.RegenerateOtp = function () {
//                //console.log($scope.user_mobile); 	 	
                if ($scope.user_mobile.length === 10) {
                    if (!new RegExp(/[7-9]{1}\d{9}/).test($scope.user_mobile)) {
                        create_alert(1, "Please enter a valid phone number", 3);
                    } else {
                        $scope.sendOtp('register');
                    }
                } else {
                    create_alert(1, "Kindly Enter valid Number", 3);
                }
            };

            if (localStorage.getItem("user") !== null && $scope.enable_login == "true") {

                $scope.user = JSON.parse(localStorage.getItem("user"));

                $scope.user_mobile = $scope.user.primary_phone;
                $scope.usermobiledisabled = true;
                $scope.get_user_details();

            } else if (localStorage.getItem("user") !== null && $scope.enable_login !== "true") {
                $scope.user_mobile = Math.floor((JSON.parse(localStorage.getItem("user")).primary_phone) / 100).toString();
            }


            $scope.btn_disable = true;
            $scope.subareas_fetched = false;
            $scope.order_checked = false;
            $scope.outlet = JSON.parse(localStorage.getItem("outlet"));

            //console.log("Showing outlet informations:");
            //console.log($scope.outlet);
            $scope.outlet_id = $scope.outlet.id;
            $scope.cart_items = JSON.parse(localStorage.getItem("cart"));

            var del_date = false;
            var selectedDate = "";
            var date = "";
            var end_time = "";
            var current_time = "";
            var now = moment();

            angular.forEach($scope.cart_items, function (item) {

                $scope.cart_total += item.total_price;
                $scope.item_count += item.quantity;
            });
            $scope.item_count = 0;
            $scope.cart_total = 0;
            $scope.find_me_called = false;
            $scope.active_tab_id = 1;
            $scope.user_addresses = [];
            $scope.mobile_user_addresses = [];
            $scope.user_address_subareas = [];

            $scope.mobile_cartTab = 1;
            $scope.mobile_userTab = 2;
            $scope.mobile_verifyTab = 3;
            $scope.mobile_showTab = $scope.mobile_cartTab;
            $scope.user = {};
            if (localStorage.getItem("subarea") !== null) {
                $scope.user_address_subareas = JSON.parse(localStorage.getItem("subarea"));
            }
            $scope.selected_address = null;
//            $scope.order_type=null;
            $scope.order_type = $scope.outlet.delivery_type.split(',')[0];
            $scope.web_order_type = false;

            $scope.showTY = false;
            $scope.showTYbutton = "";
            $scope.order_number = "";
//           //console.log($scope.order_type);

//            var is_open = check_date_time($scope.outlet.outlet_timing);

            $scope.check_date_time = function (outlet_timing)
            {
                var is_open = false;
                if (outlet_timing !== undefined)
                {
                    $.ajax({
                        url: "ws/validatestoretime.php",
                        method: "POST",
                        data: {
                            outlet_timing: outlet_timing
                        },
                        success: function (result) {
                            if (result == 1) {

                            } else {
                                create_alert(1, "Outlet Closed", 3);
                                $location.url("menu");
                            }
                        }});


                }
            }
            $scope.change_active_tab = function (tab_id) {
                if ($scope.active_tab_id === tab_id) {
                    $scope.active_tab_id = 0;
                } else {
                    if (tab_id === 3) {
                        $scope.showstep = 3;
                        $scope.edit_address_info = false;
                        if ($scope.order_type !== "Deliver") {
                            $scope.api_check_order();

                        }
                    }

                    $scope.active_tab_id = tab_id;

                }
            };
//            $scope.open_modal = function (id) {
//                $("#" + id).modal("show");
//            };
            $scope.open_modal = function (id) {
                $("#" + id).bsModal("show");
            };

            $scope.show_toast = function (item) {
                if (item.customization.length !== 0) {

                    if (item.full_view_customization === true) {
                        item.full_view_customization = false;
                    } else {
                        item.full_view_customization = true;
                    }

                }
            };

            $scope.validate_user_mobile = function () {
                if ($scope.user_mobile !== undefined) {
                    if ($scope.user_mobile.length === 10) {
                        if (!new RegExp(/[7-9]{1}\d{9}/).test($scope.user_mobile)) {
                            create_alert(1, "Please enter a valid phone number", 3);
                        } else {
                            $scope.mobile_user_addresses = [];
                            $scope.get_user_details();
                        }
                    } else {

                        $scope.find_me_called = false;
                        $scope.user = null;
                        $scope.user_addresses = [];
                        $scope.mobile_user_addresses = [];

                    }
                }
            };


            $scope.$watch("deliverdate", function (date) {

                if (typeof $scope.deliverdate !== 'undefined' && $scope.timeslot_selection === "true") {

                    $scope.compare_date();
                }
            });
            $scope.compare_date = function () {
                date = moment($scope.deliverdate, "DD-MM-YYYY");

                selectedDate = moment(date).format("DD-MM-YYYY");


//alert(moment(now).format("DD-MM-YYYY"));
//alert(selectedDate);
//                alert(selectedDate);
                if (moment(now).format("DD-MM-YYYY") === selectedDate) {
                    del_date = true;

                } else {
                    del_date = false;

                }

                $scope.Deliverytime_add();
            };

            $scope.Deliverytime_add = function () {
                current_time = moment(now, "HH:mm:ss");
//                alert(current_time);

                $http.post("ws/generatetimslot.php", {outlettiming_detail: $scope.outlet.outlet_timing, selected_date: $scope.deliverdate, timeslot_interval: $scope.timeslot_interval}, done_host_parameters).success(function (response) {
                    if (development_mode) {
                        //console.log(response);
                    }

                    $scope.timeslots = response;

                }).error(function () {
                    create_alert(1, "It's taking longer than expected. Retry?", 3);
                });
            };




            //$scope.validate_user_mobile();
            $scope.select_address = function (address) {
                $scope.selected_address = address;
                $scope.stepcomplete = 2;
                $scope.api_check_order();
                $scope.change_active_tab(3);


                $('html,body').animate({
                    scrollTop: $(".dvCouponPay").offset().top - $(".dvHeader2").height() - 200
                }, 500);
            };

            $scope.menu = function () {
                $location.url("menu");
            };

            $scope.mobile_menu = function () {
                if ($scope.mobile_showTab === $scope.mobile_cartTab)
                {
                    $location.url("menu");
                }
                else if ($scope.mobile_showTab === $scope.mobile_userTab)
                {
                    $scope.mobile_showTab = $scope.mobile_cartTab;
                }
                else if ($scope.mobile_showTab === $scope.mobile_verifyTab)
                {
                    $scope.mobile_showTab = $scope.mobile_userTab;
                }
            };

            $scope.change_order_type = function (type) {

                var nameReg = new RegExp(/^[A-Za-z\s]+$/);
                var emailReg = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
                if (!nameReg.test($scope.user.name)) {
                    create_alert(1, "Kindly enter name", 3);
                } else if (!emailReg.test($scope.user.primary_email)) {
                    create_alert(1, "Kindly enter proper EmailID", 3);
                } else if (!$scope.deliverdate && $scope.deliver_type === "preorder") {
                    create_alert(1, "Kindly select Delivery Date", 3);
                } else if (!$scope.selected_timeslot && $scope.deliver_type === "preorder") {
                    create_alert(1, "Kindly select delivery time slot", 3);
                } else {
                    $scope.order_type = type;
                    $scope.web_order_type = true;
                    if (type === "Deliver") {
                        $scope.showstep = 2;
                        $scope.stepcomplete = 1;

                        $scope.change_active_tab(2);

                        if ($scope.user_addresses.length < 1) {
                            $scope.open_modal('add-address-modal');
                        } else {
                            $('html,body').animate({
                                scrollTop: $(".dvAddressDtls").offset().top - $(".dvHeader2").height() - 200
                            }, 500);
                        }
                    } else {
                        $scope.showstep = 3;
                        $scope.stepcomplete = 2;
                        $scope.change_active_tab(3);
                    }

                    $scope.edit_personal_info = false;
                    $scope.is_personal_info_complete = true;
                }

//                if (nameReg.test($scope.user.name) && emailReg.test($scope.user.primary_email) && $scope.deliverdate && $scope.selected_timeslot != undefined) {
//
//                    
//
//
//                } else {
////                    if($scope.deliverdate==="undefined"){
////                        create_alert(1,"Please select the order processing date",3);
////                    }else{
//                    create_alert(1, "Please fill in valid details!", 3);
////                    }
//                }
            };


            $scope.add_address = function () {
//                //console.log($scope.user_address_area);
                if (!$scope.user_address_subarea && $scope.show_subarea_selection) {
                    create_alert(2, "Please select subarea.", 3);
                } else if (!$scope.user_address_tag) {
                    create_alert(2, "Please tag your address.", 3);
                } else if (!$scope.user_flat_no) {
                    create_alert(2, "Please enter Address.", 3);
                } else if (!$scope.user_landmark) {
                    create_alert(2, "Please enter Street.", 3);
                } else {
                    var new_address = {
                        id: null,
                        tag: $scope.user_address_tag ? $scope.user_address_tag : "",
                        flat_no: $scope.user_flat_no ? $scope.user_flat_no : "",
                        street: $scope.user_street ? $scope.user_street : "",
                        landmark: $scope.user_landmark ? $scope.user_landmark : "",
                        subarea: $scope.user_address_subarea.id ? $scope.user_address_subarea.id : $scope.user_address_subareas[0].id,
                        subarea_name: $scope.user_address_subarea.subarea_name ? $scope.user_address_subarea.subarea_name : $scope.user_address_subareas[0].subarea_name,
                        area_name: $scope.user_address_area.area_name,
                        area_id: $scope.user_address_area.id
                    };
                    if ($scope.user_addresses.length > 0 && $scope.user_addresses[$scope.user_addresses.length - 1].id === null) {
                        $scope.user_addresses[$scope.user_addresses.length - 1] = new_address;
                    }
                    else {
                        $scope.user_addresses.push(new_address);
                    }
                    $scope.select_address(new_address);
                    $("#add-address-modal").bsModal("hide");
//                    $(".modal-backdrop .fade .in").bsModal("hide");
                    $scope.api_check_order();


                    $('html,body').animate({
                        scrollTop: $(".dvAddressDtls").offset().top - 50
                    }, 500);
                }
            };


            $scope.add_user_details = function () {

                if (typeof $scope.user.name === 'undefined' || typeof $scope.user.primary_email === 'undefined' || typeof $scope.user_mobile === 'undefined')
                {
                    var emailReg = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
                    var nameReg = new RegExp(/^[A-Za-z\s]+$/);
                    create_alert(1, "Please enter valid contact information.", 3);
                    if ($scope.user_mobile === undefined) {
                        create_alert(1, "Please enter valid mobile number", 3);
                        $('#mobile-input').focus();
                    }
                    else if ($scope.user_mobile.length !== 10) {
                        create_alert(1, "Please enter valid mobile number", 3);
                        $('#mobile-input').focus();
                    }
                    else if ($scope.user.name === undefined) {
                        create_alert(1, "Please enter name", 3);
                        $('#name-input').focus();
                    }
                    else if (!nameReg.test($scope.user.name)) {
                        create_alert(1, "Please enter valid name", 3);
                        $('#name-input').focus();
                    }
                    else if ($scope.user.primary_email === undefined) {
                        create_alert(1, "Please enter email address", 3);
                        $('#email-input').focus();
                    }
                    else if (!emailReg.test($scope.user.primary_email)) {
                        create_alert(1, "Please enter valid email address", 3);
                        $('#email-input').focus();
                    }
                }
                else if ($scope.order_type.toLowerCase().indexOf("deliver") > -1)
                {
                    if ($scope.selected_address !== null && $scope.selected_address.address_string !== 'Add New Address')
                    {
                        $scope.user_address_tag = $scope.selected_address.tag;
                        $scope.user_flat_no = $scope.selected_address.flat_no;
                        $scope.user_street = $scope.selected_address.street;
                        $scope.user_landmark = $scope.selected_address.landmark;
                        $scope.api_check_order();
                        $scope.mobile_showTab = $scope.mobile_verifyTab;
                    }
                    else {

                        if (typeof $scope.user_flat_no === "undefined" ||
                                typeof $scope.user_landmark === "undefined" ||
                                typeof $scope.user_street === "undefined" ||
                                typeof $scope.user_address_subarea === "undefined")
                        {
                            create_alert(1, "Please enter valid address details.", 3);

                            if ($scope.user_flat_no === undefined)
                            {
                                create_alert(1, "Please enter flat no.", 3);
                                $('#house-input').focus();
                            }
                            else if ($scope.user_street === undefined)
                            {
                                create_alert(1, "Please enter street", 3);
                                $('#street-input').focus();
                            }
                            else if ($scope.user_landmark === undefined)
                            {
                                create_alert(1, "Please enter landmark", 3);
                                $('#landmark-input').focus();
                            }
                            else if ($scope.user_address_subarea === undefined)
                            {
                                create_alert(1, "Please select locality", 3);
                                $('#mobile-locality').focus();
                            }


                        }
                        else {
                            var address_string = $scope.user_flat_no + ", " + $scope.user_street + "," + $scope.user_landmark;
                            var address_full_string = address_string + ", " + ($scope.user_address_subarea.subarea_name !== null ? $scope.user_address_subarea.subarea_name + ", " : "") + ($scope.user_address_area.area_name !== null ? $scope.user_address_area.area_name + " " : "");

                            var new_address = {
                                id: null,
                                tag: $scope.user_address_tag,
                                flat_no: $scope.user_flat_no,
                                street: $scope.user_street,
                                landmark: $scope.user_landmark,
                                subarea: $scope.user_address_subarea.id,
                                subarea_name: $scope.user_address_subarea.subarea_name,
                                area_name: $scope.user_address_area.area_name,
                                area_id: $scope.user_address_area.id,
                                address_full_string: address_full_string
                            };
                            //$scope.user_addresses.push(new_address);
                            $scope.selected_address = new_address;
                            $scope.api_check_order();
                            $scope.mobile_showTab = $scope.mobile_verifyTab;
                        }
                    }
                }
                else {
                    $scope.api_check_order();
                    $scope.mobile_showTab = $scope.mobile_verifyTab;
                }
            };

            $scope.edit_quantity = function (delta, product) {
                if (delta === -1 && product.quantity > 1) {
                    product.quantity--;
                } else if (delta === 1) {
                    product.quantity++;
                }
                product.total_price = product.quantity * product.price;
            };

            $scope.remove_cart_item = function (index) {
                $scope.cart_items.splice(index, 1);
            };


            $scope.create_order_object = function () {
                var products = [];
                angular.forEach($scope.cart_items, function (item) {
                    var customizations = [];
                    if (item.customization.length > 0) {
                        angular.forEach(item.customization, function (c) {
                            var co = {
                                outlet_product_id: c.outlet_product_id,
                                id: c.id
                            };
                            customizations.push(co);
                        });
                    }
                    var product = {
                        outlet_product_id: item.outlet_product_id,
                        id: item.id,
                        comments: item.comments,
                        customization: customizations
                    };
                    for (var i = 0; i < item.quantity; i++)
                        products.push(product);
                });

                $scope.deliver_comments = $scope.deliverdate ? " Delivery Date : " + $scope.deliverdate : "";
                if ($scope.selected_timeslot) {
                    $scope.deliver_comments += " Delivery Time :" + $scope.selected_timeslot.display;
                }
//                $scope.deliver_comments += " " + $scope.selected_timeslot ? (" Delivery Time :"+ $scope.selected_timeslot.display):"";
                $scope.deliver_comments += $scope.order_comment ? $scope.order_comment : "";

                $scope.order = {
                    "payment_method": "COD",
                    "is_preorder": false,
                    "order_date": Math.round(Date.now() / 1000),
                    "coupon_code": "",
                    "total_amount": $scope.cart_total,
                    "delivery_type": $scope.order_type,
                    "orders": [
                        {
                            "company_id": company_id,
                            "outlet_id": $scope.outlet_id,
                            "coupon_code": $scope.coupon_code,
                            "total": $scope.cart_total,
                            "comments": $scope.deliver_comments,
                            "products": products
                        }
                    ]
                };
                if ($scope.order_type === "Deliver") {
                    $scope.order.subarea_id = $scope.selected_address.subarea;
                }

                $scope.order.user = {
                    "id": $scope.user.id,
                    "name": $scope.user.name,
                    "primary_phone": $scope.user_mobile,
                    "primary_email": $scope.user.primary_email
                };
                if ($scope.order_type === "Deliver") {
                    $scope.order.user.address = {
                        "id": $scope.selected_address.id,
                        "flat_no": $scope.selected_address.flat_no,
                        "street": $scope.selected_address.street,
                        "landmark": $scope.selected_address.landmark,
                        "subarea_name": $scope.selected_address.subarea_name,
                        "subarea_id": $scope.selected_address.subarea,
                        "area_name": $scope.selected_address.area_name,
                        "area_id": $scope.selected_address.area_id,
                        "tag": $scope.user_address_tag,
                        "city_id": $scope.outlet.city_id,
                        "state_id": $scope.outlet.state_id
                    };
                }
            };

            $scope.api_check_order = function () {
                create_alert(2, "Checking order...", false);

                $scope.offers = [];
                $scope.create_order_object();
                $http.post(done_host + "/done-check-order", $scope.order).success(function (response) {
                    if (development_mode) {
                        //console.log(response);
                    }
                    switch (response.responseCode) {
                        case 0:
                            create_alert(2, "OK!", 3);
                            $scope.extra_charges = [];
                            $scope.order_checked = true;
                            $scope.extra_charges = response.data.orders[0].applied_extra_charges;
                            //console.log("Showing extra charges");
                            //console.log($scope.extra_charges);
                            $scope.offers = response.data.orders[0].applied_offer;
                            $scope.calculate_payable_amount();
                            break;
                        case 600:
                            create_alert(1, response.responseMsg, 3);
                            $scope.order_checked = true;
//                            $scope.extra_charges = response.data.orders[0].applied_extra_charges;
//                            $scope.offers = response.data.orders[0].applied_offer;
                            $scope.calculate_payable_amount();
                            break;
                        default:
                            $scope.menu();
                            create_alert(1, response.responseMsg, 3);
                    }
                });
            };
           
            $scope.make_payment = function () {

                if ($scope.payment_mode === 'COD') {
                    $scope.order.payment_method = $scope.payment_mode;
                    $scope.api_save_order();
                 
                } else {
                    $scope.order.payment_method = $scope.payment_mode;
                    $scope.save_cart($scope.payment_mode);
                  
                }
            }

            $scope.web_api_save_order = function ()
            {
                $scope.api_save_order('web');
            };
            $scope.mobile_api_save_order = function ()
            {
                $scope.api_save_order('mobile_web');
            };
            $scope.TY_redirect = function ()
            {
                if ($scope.showTYbutton === 'web') {
                    $location.url("orderhistory");
                }
                else if ($scope.showTYbutton === 'mobile_web') {
                    $location.url("menu");
                }
            };


            $scope.api_save_order = function (client_platform) {
                $scope.btn_disable = true;
                create_alert(2, "Contacting outlet...", false);
                $scope.order.user = {
                    "id": $scope.user.id,
                    "name": $scope.user.name,
                    "primary_phone": $scope.user_mobile,
                    "primary_email": $scope.user.primary_email
                };
                if ($scope.order_type === "Deliver") {
                    $scope.order.user.address = {
                        "id": $scope.selected_address.id,
                        "flat_no": $scope.selected_address.flat_no,
                        "street": $scope.selected_address.street,
                        "landmark": $scope.selected_address.landmark,
                        "subarea_name": $scope.selected_address.subarea_name,
                        "subarea_id": $scope.selected_address.subarea,
                        "area_name": $scope.selected_address.area_name,
                        "area_id": $scope.selected_address.area_id,
                        "tag": $scope.user_address_tag,
                        "city_id": $scope.outlet.city_id,
                        "state_id": $scope.outlet.state_id
                    };
                }
//                //console.log($scope.order);
                $http.post(done_host + "/done-save-order", $scope.order, {
                    headers: {
                        client_platform: "web"
                    },
                    timeout: 5000
                }).success(function (response) {
                    if (development_mode) {
                        //console.log(response);
                    }
                    if (response.responseCode !== 0) {
                        $("div.alert-box.active").remove();
                        create_alert(1, response.responseMsg, 3);
                        $scope.btn_disable = false;
                    } else {
                        $("div.alert-box.active").remove();
                        localStorage.removeItem("cart");
                        $scope.showTYbutton = client_platform;
                        $scope.showTY = true;
                        $scope.order_number = response.data.order_number;
//                        if (client_platform === 'web') {                            
////                            create_alert(2, "Your Order no.: " + response.data.order_number, 5);
////                            $location.url("orderhistory");
//                        }
//                        else if (client_platform === 'mobile_web') {                           
////                            create_alert(2, "Your Order no.: " + response.data.order_number, false);
////                            $location.url("menu");
//                        }
                        $scope.showTY = true;
                    }
                    fbq('track', 'Purchase', {value: response.data.total_amount, currency: 'USD'});
                }).error(function (response) {
                    $("div.alert-box.active").remove();
                    create_alert(1, "Server Error!!!", 3);
                    $scope.btn_disable = false;
                });
            };



            $scope.calculate_payable_amount = function () {
                $scope.payable_amount = $scope.cart_total;

                angular.forEach($scope.extra_charges, function (extra_charge) {
                    $scope.payable_amount += extra_charge.extra_charged_amt;

                });
                angular.forEach($scope.offers, function (offer) {
                    $scope.coupon_amount = offer.discount_amt;
                    $scope.payable_amount -= offer.discount_amt;
                });
            };
            $scope.set_area = function (subarea) {
                for (var i = 0; i < $scope.user_address_areas.length; i++) {
                    if (subarea.area_id === $scope.user_address_areas[i].id) {
                        $scope.user_address_area = $scope.user_address_areas[i];
                        $scope.user_address_area_name = $scope.user_address_area.area_name;
                    }
                }
            };

            $scope.validate_cart = function () {

                if ($scope.cart_total === 0)
                {
                    $scope.menu();
                }
                else {
                    if ($scope.order_type === null)
                    {
                        create_alert(1, "Please select order type!", 3);
                    }
                    else {
                        $scope.mobile_showTab = $scope.mobile_userTab;
                    }
                    //check timing also
                }
            };

            $scope.$watch("payment_mode", function (subarea) {
                if ($scope.payment_mode !== undefined) {
                    $scope.stepcomplete = 3;
                }
//                }
            });

            $scope.$watch("stepcomplete", function (subarea) {
                $scope.payment_mode = undefined;
                if ($scope.stepcomplete >= 2) {
                    $scope.payment_mode = "COD";
                    $scope.btn_disable = false;
                } else if ($scope.stepcomplete >= 3) {
                    $scope.btn_disable = false;

                } else {
                    $scope.btn_disable = true;

                }

            });

            $scope.$watch("user_address_subarea", function (subarea) {
                if (typeof $scope.user_address_subarea !== 'undefined' && $scope.subareas_fetched) {
//                    //console.log(subarea);
                    $scope.set_area(subarea);
                } else {
                    $scope.user_address_area = null;
                    $scope.user_address_area_name = "";
                }
            });
            $scope.$watch("cart_items", function () {
                $scope.item_count = 0;
                $scope.cart_total = 0;
                angular.forEach($scope.cart_items, function (item) {
                    $scope.cart_total += item.total_price;
                    $scope.item_count += item.quantity;
                });
                $scope.payable_amount = $scope.cart_total;
                if ($scope.cart_total === 0)
                {
                    create_alert(1, "Your cart is empty!", 3);
                    $scope.menu();
                }
                localStorage.setItem("cart", JSON.stringify($scope.cart_items));
            }, true);


            $http.post(done_host + "/done-outlet-subareas", {outlet_id: $scope.outlet_id}).success(function (response) {
                if (development_mode) {
                    //console.log(response);
                }
                if (localStorage.getItem("subarea") === null) {
                    $scope.user_address_areas = response.data.areas;
                    $scope.user_address_subareas = response.data.subareas;
                    if (!$scope.show_subarea_selection) {
                        $scope.user_address_subarea = $scope.user_address_subareas[0];
                    }

                    $scope.disablecombo = false;

                } else if (localStorage.getItem("subarea") !== null) {
                    $scope.disablecombo = true;
                    $scope.user_address_subareas = [];
                    $scope.user_address_areas = [];
                    $scope.user_address_subareas.push(JSON.parse(localStorage.getItem("subarea")));
//                    //console.log(response.data.subareas);
//                    //console.log(JSON.parse(localStorage.getItem("subarea")));
//                    $scope.user_address_subareas = JSON.parse(localStorage.getItem("subarea"));
//                    $scope.user_address_subarea = JSON.parse(localStorage.getItem("subarea"));
//                    $scope.set_area($scope.user_address_subareas);
                    $scope.user_address_areas.push(JSON.parse(localStorage.getItem("subarea")));
                    $scope.user_address_subarea = $scope.user_address_subareas[0];
                    $scope.user_address_area = JSON.parse(localStorage.getItem("subarea"));
                    $scope.user_address_area.id = JSON.parse(localStorage.getItem("subarea")).area_id;
                    $scope.user_address_area_name = JSON.parse(localStorage.getItem("subarea")).area_name;
                }
                //$scope.get_user_details();

                $scope.subareas_fetched = true;
            });

            $scope.makeeditable = function (id) {
                if (id === "edit_personal_info") {
                    $scope.stepcomplete = 0;
                } else if (id === "edit_address_info") {
                    $scope.stepcomplete = 1;
                }
            }

            $("#deliverdate").datepicker({
                minDate: 0,
                maxDate: $scope.pre_order_dates - 1,
                dateFormat: "dd-mm-yy"
            });





            $scope.make_payment = function () {

                if ($scope.enable_otp == true  && $scope.new_user == 1) {
                    $scope.sendOtp("new user");
                    $("#otp-modal").bsModal("show");
                } else {
                    if ($scope.payment_mode === 'COD') {
                        $scope.order.payment_method = $scope.payment_mode;
                        $scope.api_save_order();
                    } else if ($scope.payment_mode === "braintree") {
                        $scope.serversavecart();
                    } else {
                        $scope.order.payment_method = $scope.payment_mode;
                        $scope.save_cart($scope.payment_mode);
                    }
                }
            };


            $scope.save_cart = function (gateway) {
                $scope.order.payment_method = $scope.payment_mode;
                $http.post(done_host + "/done-save-cart", $scope.order, {
                    headers: {
                        client_platform: "web"
                    },
                    timeout: 5000
                }).success(function (save_cart_response) {
                    if (save_cart_response.responseCode === 0) {
                        if (gateway === 'citrus') {
                            var citrusreq = {
                                txn_id: save_cart_response.data.txn_id,
                                mobile: $scope.user_mobile,
                                first_name: $scope.user.name,
                                email: $scope.user.primary_email,
                                total_amount: save_cart_response.data.total_amount,
                                address: $scope.order.user.address,
                                order_number: save_cart_response.data.order_number,
                                user_id: save_cart_response.data.user.id,
                                order_id: save_cart_response.data.orders[0].order_id,
                                is_new_user: save_cart_response.data.user.is_new_user,
                                coupon_code: save_cart_response.data.coupon_code
                            };

                            $scope.citruspay(citrusreq);

                        } else if (gateway === 'paytm') {
                            $scope.Paytm(save_cart_response);
                        }
                        else if (gateway === 'payumoney') {

                            var payureq = {
                                from_js: 1,
                                pg: "wallet",
                                bankcode: "payuw",
                                txn_id: save_cart_response.data.txn_id,
                                mobile: $scope.user_mobile,
                                first_name: $scope.user.name,
                                email: $scope.user.primary_email,
                                total_amount: save_cart_response.data.total_amount,
                                address: $scope.order.user.address,
                                order_number: save_cart_response.data.order_number,
                                user_id: save_cart_response.data.user.id,
                                order_id: save_cart_response.data.orders[0].order_id,
                                is_new_user: save_cart_response.data.user.is_new_user
                            };

                            $scope.Payu(payureq);
                        }
                        else if (gateway === 'direcpay') {

                            var direcpayreq = {
                                from_js: 1,
                                pg: "wallet",
                                bankcode: "payuw",
                                txn_id: save_cart_response.data.txn_id,
                                client_platform: client_platform,
                                mobile: $scope.user_mobile,
                                first_name: $scope.user.name,
                                email: $scope.user.primary_email,
                                total_amount: save_cart_response.data.total_amount,
                                address: $scope.order.user.address,
                                order_number: save_cart_response.data.order_number,
                                user_id: save_cart_response.data.user.id,
                                order_id: save_cart_response.data.orders[0].order_id,
                                is_new_user: save_cart_response.data.user.is_new_user
                            };
                            $scope.DirecPay(direcpayreq);
                        }
                        else if (gateway === 'quikwallet') {

                            var quikwalletreq = {
                                from_js: 1,
                                pg: "wallet",
                                bankcode: "quickwallet",
                                txn_id: save_cart_response.data.txn_id,
                                client_platform: "web",
                                mobile: $scope.user_mobile,
                                first_name: $scope.user.name,
                                email: $scope.user.primary_email,
                                total_amount: save_cart_response.data.total_amount,
                                address: $scope.order.user.address,
                                order_number: save_cart_response.data.order_number,
                                user_id: save_cart_response.data.user.id,
                                order_id: save_cart_response.data.orders[0].order_id,
                                outlet_id: save_cart_response.data.orders[0].outlet_id,
                                is_new_user: save_cart_response.data.user.is_new_user
                            };
                            $scope.QuikWallet(quikwalletreq);

                        } else if (gateway === "braintree") {
                            $scope.senddata();
                        }
                    } else {
                        create_alert(1, 'Error placing your order', 3);
                    }
                }).error(function () {
                    create_alert(1, 'OOPs ! , Its taking longer time than expected', 2);
                });
                fbq('track', 'AddPaymentInfo');
            };

            $scope.Payu = function (payureq) {
                $.ajax({
                    url: "payu_libs/payureq.php",
                    data: payureq,
                    type: 'POST',
                    success: function (response) {
                        $('#pay').html(response);
                    }
                });
            };
            $scope.DirecPay = function (direcpayreq) {
                $.ajax({
                    url: "direcpay_libs/direcpay.php",
                    data: direcpayreq,
                    type: 'POST',
                    success: function (response) {
                        $('#pay').html(response);
                    }
                });
            };

            $scope.QuikWallet = function (quikwalletreq) {
                $.ajax({
                    url: "quikwallet_libs/quickwallet.php",
                    data: quikwalletreq,
                    type: 'POST',
                    success: function (response) {
                        $('#pay').html(response);
                    }
                });
            };
            $scope.Paytm = function (save_cart_response) {
                var postdata = {
                    txn_id: save_cart_response.data.txn_id,
                    MOBILE_NO: $scope.user.primary_phone,
                    first_name: $scope.user.name,
                    EMAIL: $scope.user.primary_email,
                    total_amount: save_cart_response.data.total_amount,
                    address: $scope.order.user.address,
                    order_number: save_cart_response.data.order_number,
                    user_id: save_cart_response.data.user.id,
                    order_id: save_cart_response.data.orders[0].order_id,
                    is_new_user: save_cart_response.data.user.is_new_user
//                    txn_time:
                };
                $.ajax({
                    url: "PaytmKit/paytmreq.php",
                    data: postdata,
                    type: 'POST',
                    success: function (response) {
                        $('#pay').html(response);
                    }
                });

            };

            $scope.citruspay = function (reqdata) {
                $.ajax({
                    url: "testSSL.php",
                    url: "citruskit/citrusreq.php",
                            data: reqdata,
                    type: 'POST',
                    success: function (response) {
                        $('#pay').html(response);
                    }
                });
            }

            $scope.serversavecart = function () {
                $.ajax({
                    url: "braintree/save_cart_get_client_token.php",
                    method: "POST",
                    data: {
                        cartinfo: $scope.order
                    },
                    success: function (result) {
                        result = JSON.parse(result);
                        if (result.responseCode === 0) {
                            window.location = "braintree/call.php";
                        } else {

                        }
                    }});
            };

            $scope.check_date_time($scope.outlet.outlet_timing);
        }
//        $scope.Deliverytime_add();
    }
]);
app.directive('sglclick', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                var fn = $parse(attr['sglclick']);
                var delay = 300, clicks = 0, timer = null;
                element.on('click', function (event) {
                    clicks++;  //count clicks
                    if (clicks === 1) {
                        timer = setTimeout(function () {
                            scope.$apply(function () {
                                fn(scope, {$event: event});
                            });
                            clicks = 0;             //after action performed, reset counter
                        }, delay);
                    } else {
                        clearTimeout(timer);    //prevent single-click action
                        clicks = 0;             //after action performed, reset counter
                    }
                });
            }
        };
    }])
