<?php

$company_id = $_GET["company_id"];
$keyword = rawurlencode($_GET["keyword"]);
echo callWebService('http://api.done.to:8983/solr/company_subareas/spell?wt=json&q=subarea_name:*' . $keyword . '*&fq=company_id:' . $company_id . '&indent=true&start=0&rows=10');
//echo callWebService('http://api.done.to:8983/solr/company_subareas/spell?wt=json&q=subarea_name:*' . $keyword . '*&indent=true&start=0&rows=100');

function callWebService($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}