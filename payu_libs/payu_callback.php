<?php

/*
 * @version: 1.2
 * 
 * @last-changed-date-lib : 12/04/2016
 * 
 */

include "../dynamicVAR.php";

date_default_timezone_set("Asia/Calcutta");


//$timestamp = $received->data->timestamp;
$compute_timestamp = date('Y-m-d H:i:s', $timestamp);

$server = PAYUMONEY_SERVER;

$payment_mode = 'NA';


function callWebService($url, $methodType, $data) {
    if ($data != null)
        $data = json_encode($data, JSON_NUMERIC_CHECK);
    $headers = array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data)
    );
    $ch = curl_init($url);
    if ($methodType) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}

$status = $_POST['status'];
if (strtolower($status) == "success") {
    $payment_mode = $_POST['mode'];
} else {
    $payment_mode = 'NA';
}
$data = array();
$data['order_id'] = $_POST['udf3'];
$data['txn_status'] = $status;
$data['txn_id'] = $_POST['txnid'];
$data['txn_message'] = $status;
$data['txn_payment_mode'] = $payment_mode;
$data['txn_time'] = $_POST['addedon'];
$data['client-platform'] = "web";
$data['new_user'] = $_POST['udf1'];


$res = callWebService($server . '/done-save-transaction', true, $data);


/************************* MAINTAIN LOG *********************************/

$data_recieved = 'data_recieved.txt';
$data_recieved_current = file_get_contents($data_recieved);
$data_recieved_current .= "'\n' _____ POST ____ '\n'" . json_encode($_POST) . "'\n' _____ Data ____ '\n'" . json_encode($data) . "'\n' ********* '\n'";
echo file_put_contents($data_recieved, $data_recieved_current);

$data_response = 'data_response.txt';
$data_response_current = file_get_contents($data_response);
$data_response_current .="'\n' ____ Done Server Response _____ '\n'" . json_encode($res) . "'\n' ********* '\n'";
echo file_put_contents($data_response, $data_response_current);

/************************* MAINTAIN LOG END *****************************/
?>