<?php

/*
 * @version: 1.2
 * 
 * @last-changed-date-lib : 12/04/2016
 * 
 */
require_once './payu_libs/payu.php';
include "../dynamicVAR.php";

function payment_success() {
    /* Payment success logic goes here. */
    echo "Congratulations !! The Payment is successful.";
}

function payment_failure() {
    /* Payment failure logic goes here. */
    echo "We are sorry. The Payment has failed";
}

// Merchant key here as provided by Payu
$Merchant_Key = PAYUBIZ_MERCHANT_KEY;
// Merchant Salt as provided by Payu
$Salt = PAYUBIZ_SALT;
//Company Products name
$Company_Name = PAYUBIZ_COMPANY_NAME;
// Company URL
$Redirect_URL = PAYUBIZ_REDIRECT_URL;

$server = PAYUBIZ_SERVER;


if (isset($_POST['from_client'])) {
    $TxnId = $_POST['txnid'];
    $total_amount = $_POST['amount'];
    $first_name = $_POST['firstname'];
    $email = $_POST['email'];
    $mobile = $_POST['phone'];
    $product_info = $_POST['productinfo'];
    $is_new_user = $_POST['udf1'];
    $order_id = $_POST['udf2'];
    $user_id = $_POST['udf3'];
    $order_number = $_POST['udf4'];
    pay_page(array('key' => $Merchant_Key, 'txnid' => $TxnId, 'amount' => $total_amount,
        'firstname' => $first_name, 'email' => $email, 'phone' => $mobile,
        'productinfo' => $product_info, 'udf1' => $is_new_user, 'udf2' => $order_id, 'udf3' => $user_id, 'udf4' => $order_number, 'surl' => 'payment_success', 'furl' => 'payment_failure', 'pg' => 'Wallet', 'bankcode' => 'payuw'), $Salt);
} else if (isset($_POST['from_js'])) {
    ?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
        <head>
            <title>Payu Payments</title>
        </head>

        <body>
            <h2> Payu Payments Example </h2> <hr />
            <form method='POST' id="transactionForm" action="payuform.php">
                <table border='0'>
                    <tr> <td> from_client : </td> <td> <input name='from_client' type='hidden' value='1'> </td>
                    <!--<tr> <td> Key : </td> <td> <input name='key' type='text' value='tradus'> </td>-->
                    <tr> <td> Transaction Id : </td> <td> <input name='txnid' type='text' value='<?php echo $_POST['txn_id']; ?>'> </td>			
                    <tr> <td> Amount : </td> <td> <input name='amount' type='text' value='<?php echo $_POST['total_amount']; ?>'> </td>
                    <tr> <td> First name : </td> <td> <input name='firstname' type='text' value='<?php echo $_POST['first_name']; ?>'> </td>
                    <tr> <td> Email : </td> <td> <input name='email' type='text' value='<?php echo $_POST['email']; ?>'> </td>
                    <tr> <td> Phone : </td> <td> <input name='phone' type='text' value='<?php echo $_POST['mobile']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='productinfo' type='text' value='<?php echo $Company_Name; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='udf1' type='text' value='<?php echo $_POST['is_new_user']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='udf2' type='text' value='<?php echo $_POST['user_id']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='udf3' type='text' value='<?php echo $_POST['order_id']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='udf4' type='text' value='<?php echo $_POST['order_number']; ?>'> </td>

                    <tr> <td> Product Info : </td> <td> <input name='pg' type='text' value='<?php //echo $_POST['pg']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='bankcode' type='text' value='<?php //echo $_POST['bankcode']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='surl' type='text' value='<?php echo $Redirect_URL . "/payu_libs/payubizreq.php"; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='furl' type='text' value='<?php echo $Redirect_URL . "/payu_libs/payubizreq.php"; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='key' type='text' value='<?php echo $Merchant_Key; ?>'> </td>
                </table>
                <input type="submit" value="Submit">
            </form>
            <script type="text/javascript">
                document.getElementById("transactionForm").submit();
            </script>
        </body>
    </html>

    <?php
} else {

    function callWebService($url, $methodType, $data) {
        if ($data != null)
            $data = json_encode($data, JSON_NUMERIC_CHECK);
        $headers = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data)
        );
        $ch = curl_init($url);
        if ($methodType) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    $data = array();
    $data['order_id'] = $_POST['udf2'];
    $data['txn_status'] = $_POST['status'];
    $data['txn_id'] = $_POST['txnid'];
    $data['txn_message'] = $_POST['unmappedstatus'];
    $data['txn_payment_mode'] = $_POST['mode'];
    $data['txn_time'] = $_POST['addedon'];
    $data['client-platform'] = $_POST['platform'];
    $data['new_user'] = $_POST['udf1'];
    
    $res = callWebService($server.'/done-save-transaction', true, $data);
    ?>

    <div id="myModal1" class='modal fade' role='dialog'>

    </div>
    <div style="height: 100%;width: 100%; background: url('../img/logo.png');opacity: 0.2;">

    </div>


    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet/less" type="text/css" href="../css/main.less" />
    <link rel="stylesheet" href="../css3-animate-it-master/css/animations.css" type="text/css" />
    <!--[if lte IE 9]>
        <link href='css3-animate-it-master/css/animations-ie-fix.css' rel='stylesheet'/>
    <![endif]-->
    <link rel="stylesheet" href="../css/jquery-ui.css">
    <link rel="stylesheet" href="../font-awesome-4.1.0/css/font-awesome.min.css">

    <script src="../js/less-1.7.5.min.js" type="text/javascript"></script>
    <script src="../js/dynamicCSS.js" type="text/javascript"></script>
    <!-- jquery -->
    <script src="../js/jquery-1.11.3.min.js"></script>
    <script src="../js/jquery-1.11.4-ui.js"></script>
    <!-- Bootstrap -->
    <script src="../js/bootstrap.min.js"></script>
    <!-- custom js> -->
    <script src="../js/angular.min.js"></script>
    <script src="../js/angular-animate.min.js"></script>
    <script src="../js/angular-cookies.min.js"></script>
    <script src="../js/angular-route.min.js"></script>
    <script src="../js/angular-touch.js"></script> 
    <script src="../js/jquery.backstretch.min.js"></script>
    <script src="../js/moment.min.js"></script>

    <script type="text/javascript" src="../fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
    <script src='../css3-animate-it-master/js/css3-animate-it.js'></script>



    <script>
    <?php if (strtolower($status) === "success" || strtolower($status) === "paid") { ?>

                    $("#myModal1").html("<div><div class='modal-dialog'><div class='modal-content'><div class='modal-header' style='background-color: #CCC;'><button type='button' class='close' data-dismiss='modal'>&times;</button><h4 class='modal-title text-center'>Payment Status</h4></div><div class='modal-body text-center'><p>Congratulations ! , Your Order was successful.<br> Please Note your transaction id <strong><?php echo $data['txn_id']; ?></strong>, <br>May help you for future reference<br/>Order Id is : <?php echo $data['order_id']; ?> <br>You May also call on our respective outlet numbers</p></div><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");
                    $("#myModal1").modal('show');
                    $('#myModal1').on('hidden.bs.modal', function () {
                        localStorage.removeItem("cart");
                        window.open("<?php echo $Redirect_URL; ?>", "_self");
                    });

    <?php } else { ?>

                    $("#myModal1").html("<div ><div class='modal-dialog'><div class='modal-content'><div class='modal-header' style='background-color: #CCC;'><button type='button' class='close' data-dismiss='modal'>&times;</button><h4 class='modal-title text-center'>Payment Status</h4></div><div class='modal-body text-center'><p>Sorry ! We could not process the payment.<br> Please Note your transaction id <strong><?php echo $data['txn_id']; ?></strong>, <br>May help you for future reference<br/>Order Id is : <?php echo $data['order_id']; ?> <br>You May also call on our respective outlet numbers</p></div><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");
                    $('#myModal1').modal('show');
                    $('#myModal1').on('hidden.bs.modal', function () {
                        localStorage.removeItem("cart");
                        window.open("<?php echo $Redirect_URL; ?>", "_self");
                    });
    <?php } ?>
    </script>
    <?php
}
/* And we are done. */
?>