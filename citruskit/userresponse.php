<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" ><![endif]-->
<html class="no-js" lang="en" ng-app="done">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">

        <title>Eatsome.</title>
        <link rel="icon" href="img/Eatsome-logo-mini_1.png">

        <!-- Other styles -->
        <link rel="stylesheet" href="css/animate.css">
        
        <!-- Fonts -->
        <link rel="stylesheet" href="font-awesome-4.1.0/css/font-awesome.min.css">
        <link href="http://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet" type="text/css">

        <!-- Foundation styles -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/foundation.min.css">

        <link rel="stylesheet/less" type="text/css" href="css/done.less" />
        <script>
            less = {
                env: "production"
            };
        </script>
        <script src="js/less-1.7.3.min.js" type="text/javascript"></script>

        <script src="js/vendor/modernizr.js"></script>
    </head>
    <body>

        <div class="fixed">
            <nav class="top-bar" data-topbar>
                <ul class="title-area">
                    <li class="name">
                        <h1>
                            <a href="#" onclick="location.reload();">
                                <img alt="Logo" id="logo" src="img/logo.png" style="max-width: 200px;">
                            </a>
                        </h1>
                    </li>
                </ul>
            </nav>
        </div>
        <div id="slider" style="margin: 0 5rem;"></div>

        <div id="save-order-modal" class="reveal-modal small" data-reveal>

        </div>

        <script src="js/vendor/jquery.js"></script>
        <script src="js/moment.min.js"></script>

        <script src="js/angular.min.js"></script>
        <script src="js/angular-animate.min.js"></script>
        <script src="js/angular-cookies.min.js"></script>
        <script src="js/angular-route.min.js"></script>

        <script src="js/done.js"></script>
        <script src="js/controllers/outletsCtrl.js"></script>
        <script src="js/controllers/menuCtrl.js"></script>
        <script src="js/controllers/checkoutCtrl.js"></script>
        <script src="js/controllers/orderhistoryCtrl.js"></script>
        <script src="js/controllers/aboutusCtrl.js"></script>
        <script src="js/controllers/contactusCtrl.js"></script>

        <script src="js/foundation.min.js"></script>
        <script src="js/jquery.backstretch.min.js"></script>
        <script>
                                    $(document).foundation();
                                    $("#slider").backstretch([
                                        "img/1.jpg", "img/2.jpg", "img/3.jpg"], {duration: 6000, fade: 3000});
                                    $("#popup").foundation("reveal", "open");
        </script>
        <script>
            $(document).foundation();
<?php if ($_POST['TxStatus'] === 'SUCCESS') { ?>

                $("#save-order-modal").html("<div class=\"row panel\">Congratulations ! , Your Order was successful </div><div class=\"row panel\">Please Note your transaction id <strong><?php echo $_POST['TxId']; ?></strong>, May help you for future reference<br/><div>Order Id is : <?php echo $_POST['ordernumber']; ?></div></div><div class=\"row panel\">You May also call on our respective outlet numbers</div><a class=\"close-reveal-modal\">&#215;</a><img src=\"http://tracker.viralmint.com/sales/1426575350/<?php echo $_POST['orderAmount']; ?>/<?php echo $_POST['ordernumber']; ?>?coupon_code=<?php echo $_SESSION['coupon_code']; ?>\" height=\"0\" width=\"0\" />");
                $("#save-order-modal").foundation("reveal", "open");


<?php } else { ?>

                $("#save-order-modal").html("<div class=\"row panel\">Uh! oh!. We could not process the payment.</div><div class=\"row panel\">Please Note your transaction id <strong><?php echo $_POST['TxId']; ?></strong>, May help you for future reference</div><div class=\"row panel\">You May also call on our respective outlet numbers</div><a class=\"close-reveal-modal\">&#215;</a>");
                $("#save-order-modal").foundation("reveal", "open");

<?php } ?>

            $(document).ready(function () {
                $(document).on('close.fndtn.reveal', '[data-reveal]', function () {
                    localStorage.removeItem("cart");
                    window.open("http://order.eatsome.in", "_self");
                });
                $(document).on('closed.fndtn.reveal', '[data-reveal]', function () {
                    localStorage.removeItem("cart");
                    window.open("http://order.eatsome.in", "_self");
                });

            });
        </script>
    </body>
</html>
