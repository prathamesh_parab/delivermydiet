<?php

date_default_timezone_set('us/eastern');

$data = json_decode(file_get_contents("php://input"));
//    print_r($data->outlettiming_detail[0]->open_time);
$current_date = date('Y-m-d');
$current_time = date('Y-m-d H:i:s');
$loopdate = $current_time;
$lunchopen_time = "09:00";
$lunchstart_time = date("Y-m-d H:i", strtotime("$current_date $lunchopen_time"));
$lunchend_time = "12:00";
$lunchended_time = date("Y-m-d H:i", strtotime("$current_date $lunchend_time"));

$dinnerend_time = "22:00";
$dinnerended_time = date("Y-m-d H:i", strtotime("$current_date $dinnerend_time"));

$timeslots = [];
$k = 0;

for ($i = 0; $i < 5; $i++) {
    if (date("w", strtotime($loopdate)) == 0) {
        $loopdate = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($loopdate)));
        $timeslots[$k]["date"] = $loopdate;
        
        $timeslots[$k]["displaydate"] = date("l,M d", strtotime($loopdate));
        $timeslots[$k]["displaymoth"] = date("M", strtotime($loopdate));
        $timeslots[$k]["displayno"] = date("d", strtotime($loopdate));
        $timeslots[$k]["displayday"] = date("l", strtotime($loopdate));
        
        $timeslots[$k]["islunch"] = true;
    } else if (date("w", strtotime($loopdate)) == 6) {
        $loopdate = date('Y-m-d H:i:s', strtotime('+2 day', strtotime($loopdate)));
        $timeslots[$k]["date"] = $loopdate;
        
        $timeslots[$k]["displaydate"] = date("l,M d", strtotime($loopdate));
        $timeslots[$k]["displaymoth"] = date("M", strtotime($loopdate));
        $timeslots[$k]["displayno"] = date("d", strtotime($loopdate));
        $timeslots[$k]["displayday"] = date("l", strtotime($loopdate));
        
        $timeslots[$k]["islunch"] = true;
    } else {

        if (date("Y-m-d", strtotime($loopdate)) == date("Y-m-d", strtotime($current_time))) {
            if (strtotime($lunchended_time) < strtotime($current_time)) {
                $timeslots[$k]["islunch"] = true;
            } else {
                $timeslots[$k]["islunch"] = false;
            }

            if (strtotime($current_time) < strtotime($dinnerended_time)) {
                $timeslots[$k]["isdinner"] = true;
            } else {
                $timeslots[$k]["isdinner"] = false;
            }
        } else {
            $timeslots[$k]["islunch"] = true;
            $timeslots[$k]["isdinner"] = true;
        }
        $timeslots[$k]["date"] = $loopdate;
        
        $timeslots[$k]["displaydate"] = date("l,M d", strtotime($loopdate));
        $timeslots[$k]["displaymoth"] = date("M", strtotime($loopdate));
        $timeslots[$k]["displayno"] = date("d", strtotime($loopdate));
        $timeslots[$k]["displayday"] = date("l", strtotime($loopdate));
        
        $loopdate = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($loopdate)));
    }
    $timeslots[$k]["dayofweek"] = date("w", strtotime($loopdate));
    $k++;
}


echo json_encode($timeslots);
?>
