<?php

date_default_timezone_set("Asia/Kolkata");

$data = json_decode(file_get_contents("php://input"));
//    print_r($data->outlettiming_detail[0]->open_time);
$current_date = date('Y-m-d');
$current_time = date('Y-m-d H:i:s');

//print_r($_POST);

$is_open = false;

if ($_POST['outlet_timing'] !== 'undefined') {
    for ($i = 0; $i < count($_POST['outlet_timing']); $i++) {
        $outlet_starttime = $_POST['outlet_timing'][$i]['open_time'];
        $outlet_duration = $_POST['outlet_timing'][$i]['duration'];
        $start_time = date("Y-m-d H:i:s", strtotime("$current_date $outlet_starttime")); //moment(outlet_timing[i].open_time, "HH:mm:ss");
        $duration = date("Y-m-d H:i:s", strtotime("$current_date $outlet_duration")); //moment($_POST['outlet_timing'][$i]['duration'], "HH:mm:ss");
        $end_time = date("Y-m-d H:i:s", strtotime("+ " . date('H', strtotime($duration)) . " hours", strtotime($start_time)));
        if (strtotime($current_time) >= strtotime($start_time) && strtotime($current_time) <= strtotime($end_time)) {
            $is_open = true;
            
        }
    }
}
echo $is_open;