<?php
session_start();

date_default_timezone_set("Asia/Calcutta");
echo 'site is under construction';
if(!isset($_SESSION['client_platform'])){ ?>
<script>
    <?php if (strtolower($data['txn_status']) === "success") { ?>
                    var txnid = '////<?php echo $data['txn_id']; ?>';
                    var type = 1;
                    var msg = "Your Order was successful placed";



    <?php } else { ?>
                    var txnid = '////<?php echo $data['txn_id']; ?>';
                    var type = 0;
                    var msg = "Uh! oh!. We could not process the payment";


    <?php } ?>
                function handleResponse() {
                    Android.handleResponse(txnid, type, msg);
                }
                handleResponse();
           
    
</script>

    <?php exit; } ?>
    

<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" ><![endif]-->

<html class="no-js" lang="en" ng-app="done">
    <head>
        <meta charset="utf-8">

        <meta name="keywords" content="Maroosh">
        <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">

        <title>Maroosh</title>
        <link rel="icon" href="../img/maroosh-logo.png">

        <!-- Other styles -->
        <link rel="stylesheet" href="../css/animate.css">

        <!-- Fonts -->
        <link rel="stylesheet" href="../font-awesome-4.1.0/css/font-awesome.min.css">
        <link href="http://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet" type="text/css">

        <!-- Foundation styles -->
        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/foundation.min.css">
        <link rel="stylesheet" href="../css/jquery.bxslider.css">

        <link rel="stylesheet/less" type="text/css" href="../css/done.less" />
        <script>
            less = {
                env: "production"
            };
        </script>

        <script src="../js/less-1.7.3.min.js" type="text/javascript"></script>
        <script>
            less.refresh();
        </script>
        <script src="../js/vendor/modernizr.js"></script>
    </head>
    <body ng-controller="mainCtrl" ng-cloak>

        <div id='loader_popup'>
            <div id='floatingBarsG'>
                <img src='../img/loading.gif'>
            </div>
        </div>
        <div id="alert-container">
            <div data-alert class="alert-box alert radius" style="display: none;">
                <span class="alert-message">Alert!</span>
                <a class="close">&times;</a>
            </div>
            <div data-alert class="alert-box warning radius" style="display: none;">
                <span class="alert-message">Alert!</span>
                <a class="close">&times;</a>
            </div>
            <div data-alert class="alert-box success radius" style="display: none;">
                <span class="alert-message">Alert!</span>
                <a class="close">&times;</a>
            </div>
        </div>

        <div class="outlet-logo hide-for-small">
            <div class="row">
                <div id="social-links-container" class="columns small-12 large-3">
                    <div onclick="window.open('https://www.facebook.com/MarooshIndia?fref=ts')" id="fb-icon" ></div>
                    <div onclick="window.open('https://twitter.com/marooshindia')" id="tweet-icon"></div>
                    <div onclick="window.open('https://instagram.com/marooshofficial/')" id="instagram-icon"></div>

                    <div id="interest-icon"></div>
                    <div id="youtube-icon"></div>
                </div>
                <div class="columns small-12 large-12 no-padding" style="margin-top: -37px;">
                    <div class="medium-2 columns">
                        <img style="cursor: pointer;" onclick="location.assign('http://www.maroosh.co.in/')" src="img/maroosh-logo.png" alt="Maroosh">
                    </div>
                    <div class="medium-10 columns menubar no-padding" style="height: 34px;margin-top:37px;">
                        <!--margin-top: 35px;margin-left: 205px;-->
                        <nav class="top-bar" data-topbar>

                            <section class="top-bar-section">
                                <ul class="left menulist" style="">
                                    <li>
                                        <a href="http://www.maroosh.co.in" >
                                            Home
                                        </a>
                                    </li>
                                    <li ng-repeat="page in pages" ng-class="{
                                                                active: page.route === page_active
                                                            }">
                                        <a ng-href="#{{page.route}}">
                                            {{page.label}}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://www.maroosh.co.in/about-us.php" >
                                            About Us
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://www.maroosh.co.in/feedback.php" >
                                            FeedBack
                                        </a>
                                    </li>
                                    <li>
                                        <a href=" http://www.maroosh.co.in/locate-us.php" >
                                            Contact Us
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#/terms" >
                                            Terms & Conditions
                                        </a>
                                    </li>
                                </ul>
                                <!--                    <ul class="left termsconditions" style="margin-left: 25px;">
                                                        <li><a href="#/termsconditions" class="terms">Terms & Conditions</a></li>
                                                        <li><a href="#/privacypolicy" class="terms">Privacy Policy</a></li>
                                                        <li><a href="#/disclaimers" class="terms">Disclaimer</a></li>
                                                    </ul>-->
                            </section>
                            <ul class="title-area">
                                <li class="toggle-topbar menu-icon"><a href="#" ng-click="$event.preventDefault();"><span>Site Menu</span></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>


            </div>
        </div>

        <div ng-view></div>
        <div id="app-50-offer" class="reveal-modal tiny" data-reveal>
            <a class="close-reveal-modal" style="color:#452503;">&#215;</a>
            <div class="app50-offer small-12 columns" onclick="window.open('https://play.google.com/store/apps/details?id=to.done.maroosh')">
                <img src="../img/app-50-offer.png" alt="App 50 offer"/>
                <!--                <div class="small-12 columns">
                                    <span id="android-icon-offer">Click here to download the app to avail the offer</span>
                                </div>-->
            </div>
        </div>
        <!--                <div id="payu-modal" class="reveal-modal tiny" data-reveal>
                            <a class="close-reveal-modal" style="color:#452503;">&#215;</a>
                            <a href="https://play.google.com/store/apps/details?id=to.done.maroosh&hl=fr" target="_blank"><img src="img/payubanner.jpg"/></a>
                        </div>-->

        <div id="save-order-modal" class="reveal-modal small" data-reveal style="">

        </div>
        <script src="../js/vendor/jquery.js"></script>


        <script src="../js/angular.min.js"></script>
        <script src="../js/angular-animate.min.js"></script>
        <script src="../js/angular-cookies.min.js"></script>
        <script src="../js/angular-route.min.js"></script>
        <script src="../js/growlNotifications.js"></script>
        <script src="../js/jquery.backstretch.min.js"></script>
        <script src="../js/jquery.bxslider.min.js"></script>
        <script src="../js/done.js"></script>
        <script src="../js/controllers/outletsCtrl.js"></script>
        <script src="../js/controllers/menuCtrl.js"></script>
        <script src="../js/controllers/checkoutCtrl.js"></script>
        <script src="../js/controllers/orderhistoryCtrl.js"></script>
        <script src="../js/controllers/aboutusCtrl.js"></script>
        <script src="../js/controllers/contactusCtrl.js"></script>
        <script src="../js/controllers/combosCtrl.js"></script>

        <script src="../js/foundation.min.js"></script>
        <script>

                        $(document).foundation();
                        $('#popup-modal').foundation('reveal', 'open');
                        //                                    $('#payu-modal').foundation('reveal', 'open');
                        if (/iPhone|iOS|iPad|iPod/i.test(navigator.userAgent)) {

                        }
                        else if (/Android/i.test(navigator.userAgent)) {

                        }
                        else {
                            //                            $('#app-50-offer').foundation('reveal', 'open');
                        }


        </script>       
        <script>

                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                    ga('create', 'UA-57538902-12', 'auto');
                    ga('send', 'pageview');


        </script>
    </body>
</html>

<script>
    $(document).foundation();    
<?php if (strtolower($_GET['status']) === "success" || strtolower($_GET['status']) === "paid") { ?>

        $("#save-order-modal").html("<div class=\"row panel\">Congratulations ! , Your Order was successful </div><div class=\"row panel\">Please Note your transaction id <strong><?php echo $_SESSION['billnumbers']; ?></strong>, May help you for future reference<br/><div>Order Id is : <?php echo $_SESSION['orderid']; ?></div></div><div class=\"row panel\">You May also call on our respective outlet numbers</div><a class=\"close-reveal-modal\">&#215;</a>");
        $("#save-order-modal").foundation("reveal", "open");
        $(document).on('closed.fndtn.reveal', '[data-reveal]', function () {
            localStorage.removeItem("cart");
            window.open("http://order.maroosh.co.in", "_self");
        });

<?php } else { ?>

        $("#save-order-modal").html("<div class=\"row panel\">Uh! oh!. We could not process the payment.</div><div class=\"row panel\">Please Note your transaction id <strong><?php echo $_SESSION['billnumbers']; ?></strong>, May help you for future reference<br/><div>Order Id is : <?php echo $_SESSION['orderid']; ?></div></div><div class=\"row panel\">You May also call on our respective outlet numbers</div><a class=\"close-reveal-modal\">&#215;</a>");
        $("#save-order-modal").foundation("reveal", "open");
        $(document).on('closed.fndtn.reveal', '[data-reveal]', function () {
            window.open("http://order.maroosh.co.in", "_self");
        });
<?php }?>

</script>
<?php
/* And we are done. */
?>
