<?php
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");

include "../dynamicVAR.php";
session_start();



$ORDER_ID = $_POST["order_id"];
$CUST_ID = $_POST["user_id"];
$INDUSTRY_TYPE_ID = PAYTM1_INDUSTRY_TYPE_ID;
$CHANNEL_ID = PAYTM1_CHANNEL_ID;
$TXN_AMOUNT = $_POST["total_amount"];
$isnewuser = $_POST["is_new_user"];
$MOBILE_NO = $_POST["MOBILE_NO"];
$EMAIL = $_POST["EMAIL"];
$_SESSION['total_amount'] = $_POST["total_amount"];
$_SESSION['IS_NEW_USER'] = $_POST["is_new_user"];
$_SESSION['TXN_ID'] = $_POST["txn_id"];
$_SESSION['coupon_code'] = $_POST["coupon_code"];
//print_r($_SESSION);exit;
// Create an array having all required parameters for creating checksum.
//$paramList["MID"] = PAYTM_MERCHANT_MID;
//$paramList["ORDER_ID"] = $ORDER_ID;
//$paramList["CUST_ID"] = $CUST_ID;
//$paramList["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;
//$paramList["CHANNEL_ID"] = $CHANNEL_ID;
//$paramList["TXN_AMOUNT"] = $TXN_AMOUNT;
//$paramList["WEBSITE"] = PAYTM_MERCHANT_WEBSITE;
//$paramList["REQUEST_TYPE"] = "DEFAULT";
//$paramList["isnewuser"] = $isnewuser;
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Merchant Check Out Page</title>
        <meta name="GENERATOR" content="Evrsoft First Page">
    </head>
    <body>
        <h1>Merchant Check Out Page</h1>

        <form method="post" action="PaytmKit/pgRedirect.php" name="f2">
            <table border="1">
                <tbody>
                    <tr>
                        <th>S.No</th>
                        <th>Label</th>
                        <th>Value</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><label>ORDER_ID::*</label></td>
                        <td><input id="ORDER_ID" tabindex="1" maxlength="20" size="20"
                                   name="ORDER_ID" autocomplete="off"
                                   value="<?= $ORDER_ID ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><label>CUSTID ::*</label></td>
                        <td><input id="CUST_ID" tabindex="2" maxlength="12" size="12" name="CUST_ID" autocomplete="off" value="<?= $CUST_ID ?>"></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><label>INDUSTRY_TYPE_ID ::*</label></td>
                        <td><input id="INDUSTRY_TYPE_ID" tabindex="4" maxlength="12" size="12" name="INDUSTRY_TYPE_ID" autocomplete="off" value="<?= $INDUSTRY_TYPE_ID ?>"></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><label>Channel ::*</label></td>
                        <td><input id="CHANNEL_ID" tabindex="4" maxlength="12"
                                   size="12" name="CHANNEL_ID" autocomplete="off" value="<?= $CHANNEL_ID ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td><label>txnAmount*</label></td>
                        <td><input title="TXN_AMOUNT" tabindex="10"
                                   type="text" name="TXN_AMOUNT"
                                   value="<?= $TXN_AMOUNT ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td><label>mobile No.*</label></td>
                        <td><input title="MOBILE_NO" tabindex="10"
                                   type="text" name="MOBILE_NO"
                                   value="<?= $MOBILE_NO ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td><label>emailid*</label></td>
                        <td><input title="EMAIL" tabindex="10"
                                   type="text" name="EMAIL"
                                   value="<?= $EMAIL ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td><label>IsnewUser*</label></td>
                        <td><input title="TXN_AMOUNT" tabindex="10"
                                   type="text" name="IS_NEW_USER"
                                   value="<?= $isnewuser ?>">
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><input value="CheckOut" type="submit"	onclick=""></td>
                    </tr>
                </tbody>
            </table>
            <script type="text/javascript">
                document.f2.submit();
            </script>
            * - Mandatory Fields
        </form>
    </body>
</html>