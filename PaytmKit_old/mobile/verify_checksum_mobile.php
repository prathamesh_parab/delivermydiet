<?php
// following files need to be included
require_once("../PaytmKit/lib/config_paytm.php");
require_once("../PaytmKit/lib/encdec_paytm.php");

$paytmChecksum = "";
$paramList = array();
$isValidChecksum = "FALSE";

$paramList = $_POST;
$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg

$isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.



if($isValidChecksum){
	$paramList['IS_CHECKSUM_VALID'] = 'Y';
}else{
	$paramList['IS_CHECKSUM_VALID'] = 'N';
}

echo json_encode($paramList);


?>
