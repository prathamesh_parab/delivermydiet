<?php
require_once './payu_libs/payu.php';

function payment_success() {
    /* Payment success logic goes here. */
    echo "Congratulations !! The Payment is successful.";
}

function payment_failure() {
    /* Payment failure logic goes here. */
    echo "We are sorry. The Payment has failed";
}

if (isset($_POST['from_client'])) {
    $TxnId = $_POST['txnid'];
    $total_amount = $_POST['amount'];
    $first_name = $_POST['firstname'];
    $email = $_POST['email'];
    $mobile = $_POST['phone'];
    $product_info = $_POST['productinfo'];
    $is_new_user = $_POST['udf1'];
    $order_id = $_POST['udf2'];
    $user_id = $_POST['udf3'];
    $order_number = $_POST['udf4'];
    pay_page(array('key' => 'ebb7pv', 'txnid' => $TxnId, 'amount' => $total_amount,
        'firstname' => $first_name, 'email' => $email, 'phone' => $mobile,
        'productinfo' => $product_info, 'udf1' => $is_new_user, 'udf2' => $order_id, 'udf3' => $user_id, 'udf4' => $order_number, 'surl' => 'payment_success', 'furl' => 'payment_failure', 'pg' => 'Wallet', 'bankcode' => 'payuw'), 'IPacXWUF');
} else if (isset($_POST['from_js'])) {
    ?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
        <head>
            <title>Payu Payments</title>
        </head>

        <body>
            <h2> Payu Payments Example </h2> <hr />
            <form method='POST' id="transactionForm" action="payuformseam.php">
                <table border='0'>
                    <tr> <td> from_client : </td> <td> <input name='from_client' type='hidden' value='1'> </td>
                    <!--<tr> <td> Key : </td> <td> <input name='key' type='text' value='tradus'> </td>-->
                    <tr> <td> Transaction Id : </td> <td> <input name='txnid' type='text' value='<?php echo $_POST['txn_id']; ?>'> </td>			
                    <tr> <td> Amount : </td> <td> <input name='amount' type='text' value='<?php echo $_POST['total_amount']; ?>'> </td>
                    <tr> <td> First name : </td> <td> <input name='firstname' type='text' value='<?php echo $_POST['first_name']; ?>'> </td>
                    <tr> <td> Email : </td> <td> <input name='email' type='text' value='<?php echo $_POST['email']; ?>'> </td>
                    <tr> <td> Phone : </td> <td> <input name='phone' type='text' value='<?php echo $_POST['mobile']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='productinfo' type='text' value='Just another test Product'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='udf1' type='text' value='<?php echo $_POST['is_new_user']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='udf2' type='text' value='<?php echo $_POST['order_id']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='udf3' type='text' value='<?php echo $_POST['user_id']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='udf4' type='text' value='<?php echo $_POST['order_number']; ?>'> </td>

                    <tr> <td> Product Info : </td> <td> <input name='pg' type='text' value='<?php echo $_POST['pg']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='bankcode' type='text' value='<?php echo $_POST['bankcode']; ?>'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='surl' type='text' value='http://order.eatsome.in/example_2.php'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='furl' type='text' value='http://order.eatsome.in/example_2.php'> </td>
                    <tr> <td> Product Info : </td> <td> <input name='key' type='text' value='ebb7pv'> </td>
                </table>
                <input type="submit" value="Submit">
            </form>
            <script type="text/javascript">
                document.getElementById("transactionForm").submit();
            </script>
        </body>
    </html>

    <?php
} else {

    function callWebService($url, $methodType, $data) {
        if ($data != null)
            $data = json_encode($data, JSON_NUMERIC_CHECK);
        $headers = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data)
        );
        $ch = curl_init($url);
        if ($methodType) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    $data = array();
    $data['order_id'] = $_POST['udf2'];
    $data['txn_status'] = $_POST['status'];
    $data['txn_id'] = $_POST['txnid'];
    $data['txn_message'] = $_POST['unmappedstatus'];
    $data['txn_payment_mode'] = $_POST['mode'];
    $data['txn_time'] = $_POST['addedon'];
    $data['client-platform'] = $_POST['platform'];
    $data['new_user'] = $_POST['udf1'];
//$res = callWebService('http://10.0.0.26:5000/done-save-transaction', true, $data);
    $res = callWebService('http://api.done.to:5000/done-save-transaction', true, $data);
    ?>
    <!DOCTYPE html>
    <!--[if IE 9]><html class="lt-ie10" lang="en" ><![endif]-->
    <html class="no-js" lang="en" ng-app="done">
        <head>
            <meta charset="utf-8">

            <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">

            <title>Eatsome.</title>
            <link rel="icon" href="img/Eatsome-logo-mini_1.png">

            <!-- Other styles -->
            <link rel="stylesheet" href="css/animate.css">

            <!-- Fonts -->
            <link rel="stylesheet" href="font-awesome-4.1.0/css/font-awesome.min.css">
            <link href="http://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet" type="text/css">

            <!-- Foundation styles -->
            <!-- Foundation styles -->
            <link rel="stylesheet" href="css/normalize.css">
            <link rel="stylesheet" href="css/foundation.min.css">

            <link rel="stylesheet/less" type="text/css" href="css/done.less" />
            <script>
                less = {
                    env: "production"
                };
            </script>
            <script src="js/less-1.7.3.min.js" type="text/javascript"></script>

            <script src="js/vendor/modernizr.js"></script>
        </head>
        <body ng-controller="mainCtrl" ng-cloak>

            <div id="alert-container">
                <div data-alert class="alert-box alert radius" style="display: none;">
                    <span class="alert-message">Alert!</span>
                    <a class="close">x</a>
                </div>
                <div data-alert class="alert-box warning radius" style="display: none;">
                    <span class="alert-message">Alert!</span>
                    <a class="close">x</a>
                </div>
                <div data-alert class="alert-box success radius" style="display: none;">
                    <span class="alert-message">Alert!</span>
                    <a class="close">x</a>
                </div>
            </div>
            <div id="slider" style="margin: 0 5rem;"></div>
            <div class="fixed">
                <nav class="top-bar" data-topbar>
                    <ul class="title-area">
                        <li class="name"></li>
                        <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                        <li class="toggle-topbar menu-icon"><a href="#" ng-click="$event.preventDefault();"><span>Site Menu</span></a></li>
                    </ul>
                    <section class="top-bar-section">
                        <ul class="left">
                            <li ng-repeat="page in pages" ng-class="{active: page.route === page_active}"><a ng-href="#{{page.route}}">{{page.label}}</a></li>      
                            <li ><a href="http://eatsome.in/about-us.html" target="_blank">About Us</a></li>      
                        </ul>
                        <div class="left terms-condition">
                            <a ng-href="#/terms">Terms & Conditions</a>
                            <a ng-href="#/faq">FAQ</a>
                            <a ng-href="#/privacypolicy">Privacy policy</a>
                            <a ng-href="#/disclaimers">Disclaimers</a>
                        </div>
                        <ul id="social-links-container">
                            <li onclick="window.open('https://play.google.com/store/apps/details?id=done.mobile.eatsome')">
                                <img src="img/android.png" alt="android">
                            </li>
                            <li onclick="window.open('https://itunes.apple.com/us/app/eatsome/id929610729?mt=8')">
                                <img src="img/apple.png" alt="apple">
                            </li>
                            <li onclick="window.open('https://www.facebook.com/EATS0ME')">
                                <img src="img/facebook.png" alt="Facebook">
                            </li>
                            <li onclick="window.open('https://twitter.com/eatsome')">
                                <img src="img/twitter.png" alt="Twitter">
                            </li>
                        </ul>   

                    </section>
                </nav>
                <!--            <div id="popup" class="reveal-modal medium" data-reveal style="padding: 0px;">
                
                                <img src="img/ES_GudipadwaOffer webbanner3.png">
                                <img src="img/Eatsome worldcup popup.jpg">
                                <img src="img/Eatsome_valentine_offer_popup.jpg">
                                <img src="img/Deali-cious-offer.png">
                                <img src="img/Offer-5-final.jpg">
                                <div>
                                    <a class="close-reveal-modal" id="clsbogo">&#215;</a>
                                </div>
                            </div>-->
                <div id="popup_offer" class="reveal-modal medium" data-reveal style="padding: 0px;">

                    <!--<img src="img/No Diet Day offer pop up_Revision1.jpg">-->
                    <img src="img/Eat What You Want Day_pop up_Revision3.jpg">
                    <!--<img src="img/Eatsome worldcup popup.jpg">-->
                    <!--<img src="img/Eatsome_valentine_offer_popup.jpg">-->
                    <!--<img src="img/Deali-cious-offer.png">-->
                    <!--<img src="img/Offer-5-final.jpg">-->
                    <div>
                        <a class="close-reveal-modal" id="clsbogo">&#215;</a>
                    </div>
                </div>
            </div>
            <div id="save-order-modal" class="reveal-modal small" data-reveal style="font-family: arial;">

            </div>

            <script src="js/vendor/jquery.js"></script>
            <script src="js/moment.min.js"></script>

            <script src="js/angular.min.js"></script>
            <script src="js/angular-animate.min.js"></script>
            <script src="js/angular-cookies.min.js"></script>
            <script src="js/angular-route.min.js"></script>

            <script src="js/done.js"></script>
            <script src="js/controllers/outletsCtrl.js"></script>
            <script src="js/controllers/menuCtrl.js"></script>
            <script src="js/controllers/checkoutCtrl.js"></script>
            <script src="js/controllers/orderhistoryCtrl.js"></script>
            <script src="js/controllers/aboutusCtrl.js"></script>
            <script src="js/controllers/contactusCtrl.js"></script>
            <script src="js/controllers/orderconfirmCtrl.js"></script>
            <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
            <script src="js/foundation.min.js"></script>

            <script src="js/jquery.backstretch.min.js"></script>
            <script>
                                        $(document).foundation();
                                        $("#slider").backstretch([
                                        "img/1.jpg", "img/2.jpg", "img/3.jpg"], {duration: 6000, fade: 3000});
    //                                    $("#popup_offer").foundation("reveal", "open");
            </script>



            <script>
                        $(document).foundation();
    <?php if (strtolower($_POST['status']) === "success") { ?>

                    $("#save-order-modal").html("<div class=\"row panel\">Congratulations ! , Your Order was successful </div><div class=\"row panel\">Please Note your transaction id <strong><?php echo $_POST['txnid']; ?></strong>, May help you for future reference<br/><div>Order Id is : <?php echo $_POST['udf4']; ?></div></div><div class=\"row panel\">You May also call on our respective outlet numbers</div><a class=\"close-reveal-modal\">&#215;</a>");
                            $("#save-order-modal").foundation("reveal", "open");
                            $(document).on('closed.fndtn.reveal', '[data-reveal]', function () {
                    localStorage.removeItem("cart");
                            window.open("http://order.eatsome.in", "_self");
                    });
    <?php } else { ?>

                    $("#save-order-modal").html("<div class=\"row panel\">Uh! oh!. We could not process the payment.</div><div class=\"row panel\">Please Note your transaction id <strong><?php echo $_POST['txnid']; ?></strong>, May help you for future reference</div><div class=\"row panel\">You May also call on our respective outlet numbers</div><a class=\"close-reveal-modal\">&#215;</a>");
                            $("#save-order-modal").foundation("reveal", "open");
                            $(document).on('closed.fndtn.reveal', '[data-reveal]', function () {
                    window.open("http://order.eatsome.in", "_self");
                    });
    <?php } ?>

            </script>
    <?php
}

/* And we are done. */
?>
